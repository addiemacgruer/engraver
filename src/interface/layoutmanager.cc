#include "layoutmanager.h"

#include "barlayout.h"
#include "util/logging.h"
#include "util/maths.h"

#include <algorithm>

namespace interface {

constexpr double LayoutManager::epsilon = 0.00001;

NotationArea::NotationArea(
    const notation::Notation &notation, const geom::Rect<> &area)
    : mpNotation{&notation}, mArea{area} {}

bool NotationArea::operator<(const NotationArea &other) const {
  return mArea.area() < other.area().area();
}

const notation::Notation &NotationArea::notation() const {
  return *mpNotation;
}

geom::Rect<> NotationArea::area() const {
  return mArea;
}

LayoutManager::LayoutManager(notation::Movement &movement)
    : mpMovement(&movement), mNotationList{}, mBarLayouts{}, mPartOffsets{} {}

double LayoutManager::offset_for(size_t part) const {
  return mPartOffsets.at(part)->offset;
}

double LayoutManager::height_for(size_t part) const {
  return mPartOffsets.at(part)->bottom - mPartOffsets.at(part)->top;
}

namespace {
template <class T>
bool float_equal(T a, T b, T epsilon) {
  return std::fabs(a - b) < epsilon;
}
} // namespace

void LayoutManager::update_part_height(size_t part, double top, double bottom) {
  {
    auto _ = mPartOffsetsMutex.lock();
    auto existingTop = mPartOffsets.at(part)->top;
    auto existingBottom = mPartOffsets.at(part)->bottom;
    if (float_equal(existingTop, top, epsilon) &&
        float_equal(existingBottom, bottom, epsilon))
      return;
  }
  {
    auto _ = mPartOffsetsMutex.lock();
    mNeedsRedraw = true;
    mPartOffsets.at(part)->top = top;
    mPartOffsets.at(part)->bottom = bottom;
  }
}

void LayoutManager::check_offsets_count() {
  { // make sure that there are the correct number of parts
    auto _ = mPartOffsetsMutex.lock();
    auto partsCount = mpMovement->parts().size();
    if (mPartOffsets.size() != partsCount) {
      util::debug("Reinitialising part offsets size to ", partsCount);
      mNeedsRedraw = true;
      mPartOffsets.clear();
      for (size_t i = 0; i < partsCount; ++i)
        mPartOffsets.push_back(std::make_unique<Offset>());
    }
  }
  auto mostbars = mpMovement->get_total_bars();
  { // making sure that the number of bars is correct in each part
    auto _ = mBarLengthMutex.lock();
    if (mBarLayouts.size() != mostbars) {
      mBarLayouts.clear();
      util::debug("Making space for ", mostbars, " bars");
      for (size_t i = 0; i < mostbars; ++i) {
        mBarLayouts.push_back(std::make_unique<BarLayout>(*this));
        auto &last = mBarLayouts.back();
        util::debug("  bar layout ", i, " at ", last.get());
      }
      mNeedsRedraw = true;
    }
  }
}

bool LayoutManager::needs_redraw() const {
  return mNeedsRedraw;
}

void LayoutManager::make_clean() {
  mNeedsRedraw = false;
}

void LayoutManager::finalise() {
  auto lastBottom = double{0};
  for (size_t i = 0, j = mPartOffsets.size(); i < j; ++i) {
    auto &part = mPartOffsets.at(i);
    part->offset = lastBottom - part->top;
    lastBottom += (part->bottom - part->top);
    util::debug("Part:", i, " top:", part->top, " offset:", part->offset,
        " bottom:", part->bottom);
    util::debug(" lb:", lastBottom);
  }
  mNotationList.clear();
}

interface::BarLayout *LayoutManager::get_bar_layout(size_t bar) const {
  auto _ = mBarLengthMutex.lock();
  auto &barlayout = mBarLayouts.at(bar);
  auto position = barlayout.get();
  if (position == nullptr) {
    util::error("Couldn't get the BarLayout at position ", bar);
    throw std::runtime_error{"Got a bad position?"};
  }
  return position;
}

//  Notation list methods follow
void LayoutManager::add_to_notation_list(
    const notation::Notation &notation, const geom::Rect<> &screenPosition) {
  auto pair = std::make_unique<NotationArea>(notation, screenPosition);
  {
    auto _ = mNotationMutex.lock();
    mNotationList.push_back(std::move(pair));
  }
  mIsNotationListSorted = false;
}

const std::vector<const notation::Notation *>
LayoutManager::get_notation_at_point(const geom::Point<> &point) const {
  auto _ = mNotationMutex.lock();
  if (!mIsNotationListSorted) {
    auto begin = mNotationList.begin();
    auto end = mNotationList.end();
    std::sort(begin, end);
    mIsNotationListSorted = true;
  }
  auto rval = std::vector<const notation::Notation *>{};
  for (const auto &pair : mNotationList) {
    if (geom::is_point_in_rect(point, pair->area()))
      rval.push_back(&pair->notation());
  }
  return rval;
}

std::size_t LayoutManager::get_notation_list_count() const {
  auto _ = mNotationMutex.lock();
  return mNotationList.size();
}

geom::Rect<> LayoutManager::get_rect_for_notation(
    const notation::Notation &notation) const {
  auto _ = mNotationMutex.lock();
  auto result = std::find_if(mNotationList.begin(), mNotationList.end(),
      [&notation](const auto &pair) { return &pair->notation() == &notation; });
  if (result != mNotationList.end())
    return (*result)->area();
  util::warning("Notation not found:", &notation);
  return {};
}

void LayoutManager::make_dirty() {
  mNeedsRedraw = true;
}

void LayoutManager::reset() {
  {
    auto _ = mNotationMutex.lock();
    mNotationList.clear();
  }
  {
    auto _ = mBarLengthMutex.lock();
    mBarLayouts.clear();
  }
}

} // namespace interface
