#include "engraverwindow.h"

#include "barlayout.h"
#include "control.h"
#include "global.h"
#include "gui/filechooser.h"
#include "gui/genericdialogue.h"
#include "gui/label.h"
#include "gui/menu.h"
#include "gui/scrollbar.h"
#include "gui/window.h"
#include "interface/layoutmanager.h"
#include "lilypond/lilypond.h"
#include "notation/book.h"
#include "notation/bookkey.h"
#include "notation/movement.h"
#include "notation/movementkey.h" // for MovementKey
#include "util/assert.h"
#include "util/logging.h"
#include "util/string.h"

namespace interface {

gui::Scrollbar &EngraverWindow::get_vertical_scrollbar() const {
  return *mVerticalScrollbar;
}

gui::Window &EngraverWindow::get_window_widget() const {
  return *mWindow;
}

gui::Scrollbar &EngraverWindow::get_horizontal_scrollbar() const {
  return *mHorizontalScrollbar;
}

geom::Rect<int> EngraverWindow::get_size() const {
  return mWindow->get_size();
}

EngraverWindow::~EngraverWindow() {
  //  util::log_alloc(util::Alloc::Destruct, "interface", "EngraverWindow");
}

ManuscriptCanvas &EngraverWindow::get_drawing_canvas() const {
  util::debug("Requested canvas.");
  return *mCanvas.get();
}

gui::Label &EngraverWindow::get_status_textbox() const {
  return *mStatusText.get();
}

void EngraverWindow::set_title(const std::string &title) {
  mWindow->set_text(title);
}

void EngraverWindow::create_menus() {
  util::log(util::LogLevel::Debug, "Creating menubars");
  mpMenu = std::make_unique<gui::Menu>();
  add_file_menu();
  add_edit_menu();
  add_view_menu();
  add_movement_menu();
  add_parts_menu();
  add_bar_menu();
  add_notation_menu();
  add_help_menu();
  mpMenu->end_sub_menu();
  mpMenu->init();
  mWindow->add_widget(mpMenu.get());
}

void EngraverWindow::create_scroll_bars() {
  mVerticalScrollbar = std::make_unique<gui::Scrollbar>();
  mHorizontalScrollbar = std::make_unique<gui::Scrollbar>();
  mHorizontalScrollbar->set_orientation(
      gui::Scrollbar::Orientation::Horizontal);
  mHorizontalScrollbar->set_callback([this]() { mCanvas->redraw(); });
  mVerticalScrollbar->set_callback([this]() { mCanvas->redraw(); });
  mWindow->add_widget(mVerticalScrollbar.get());
  mWindow->add_widget(mHorizontalScrollbar.get());
}

void EngraverWindow::close() {
  assert::is_not_null(
      mWindow.get(), "interface/engraverwindow::close lost pointer");
  mWindow->close();
}

EngraverWindow::EngraverWindow(Control &controller) : mpControl{&controller} {
  //  util::log_alloc(util::Alloc::Construct, "interface", "EngraverWindow");
  mWindow = std::make_unique<gui::Window>();
  mWindow->set_text("Engraver");
  mWindow->on_resize([this](const geom::Rect<int> &size) { resize(size); });
  mStatusText = std::make_unique<gui::Label>();
  mStatusText->set_text("Welcome to Engraver!");
  mWindow->add_widget(mStatusText.get());

  create_menus();
  mCanvas = std::make_unique<ManuscriptCanvas>(*mpControl);
  create_scroll_bars();
  mWindow->end();

  mWindow->set_size({{100, 100}, {1124, 868}});
  mWindow->on_close([this]() { mpControl->try_to_close(); });
  mWindow->show();
  util::debug("Finished init");
}

namespace { // TODO copied from control.cc
auto select_file_type(
    const std::string &description, const std::string &extension) {
  auto rval = gui::FileChooser::create_saveas_chooser()
                  ->add_filter(description, "*" + extension)
                  .get_filename();
  return util::append_if_needed(rval, extension);
}

auto select_engraver_file() {
  return select_file_type("Engraver file", ".grave");
}
auto select_denemo_file() {
  return select_file_type("Denemo file", ".denemo");
}
auto select_musicxml_file() {
  return select_file_type("MusicXml file", ".musicxml");
}
auto select_lilypond_file() {
  return select_file_type("Lilypond file", ".ly");
}
} // namespace

void EngraverWindow::on_open_file() try {
  mStatusText->set_text("Opening file...");
  auto filename = select_engraver_file();
  Control::open_engraver(filename);
} catch (std::exception &e) {
  util::log(util::LogLevel::Error, "Failed to open file:", e.what());
  mStatusText->set_text(util::concat("Failed to open file: ", e.what()));
}

void EngraverWindow::on_import_denemo() try {
  mStatusText->set_text("Importing Denemo...");
  auto filename = select_denemo_file();
  Control::open_denemo(filename);
} catch (std::exception &e) {
  mStatusText->set_text(util::concat("Failed to import Denemo: ", e.what()));
}

void EngraverWindow::on_import_music_xml() try {
  mStatusText->set_text("Importing MusicXml...");
  auto filename = select_musicxml_file();
  Control::open_music_xml(filename);
} catch (std::exception &e) {
  mStatusText->set_text(util::concat("Failed to import Denemo: ", e.what()));
}

void EngraverWindow::on_export_lilypond_part() try {
  util::info("OnExportLilypondPart");
  mStatusText->set_text("Exporting Lilypond Part...");
  auto filename = select_lilypond_file();
  auto format = lilypond::Format{filename};
  format.write_part(mpControl->get_current_part());
  mStatusText->set_text("Exported Lilypond Part OK");
} catch (std::exception &e) {
  mStatusText->set_text(
      util::concat("Failed to export Lilypond part: ", e.what()));
}

namespace {
auto choose_directory() {
  auto chooser = gui::FileChooser::create_directory_chooser();
  return chooser->get_filename();
}
} // namespace

void EngraverWindow::on_export_lilypond_parts() try {
  util::info("OnExportLilypondParts");
  mStatusText->set_text("Exporting Lilypond Parts...");
  auto foldername = choose_directory();
  auto format = lilypond::Format{foldername};
  format.write_all_scores(mpControl->get_book());
  mStatusText->set_text("Exported Lilypond Parts OK");
} catch (std::exception &e) {
  mStatusText->set_text(
      util::concat("Failed to export Lilypond parts: ", e.what()));
}

void EngraverWindow::on_export_lilypond_score() try {
  util::info("OnExportLilypondScore");
  mStatusText->set_text("Exporting Lilypond Score...");
  auto filename = select_lilypond_file();
  auto format = lilypond::Format{filename};
  format.write_score(mpControl->get_book());
  mStatusText->set_text("Exported Lilypond Part OK");
} catch (std::exception &e) {
  mStatusText->set_text(
      util::concat("Failed to export Lilypond parts: ", e.what()));
}

void EngraverWindow::on_book_properties() {
  util::info("OnBookProperties");
  auto gd = gui::GenericDialogue{"Book properties"};
  for (const auto key : enums::values<notation::BookKey>()) {
    gd.add_text(enums::to_string(key), //
        [this, key]() { return mpControl->get_book().get(key); },
        [this, key](
            const std::string &text) { mpControl->get_book().set(key, text); });
  }
  gd.show();
}

void EngraverWindow::on_movement_properties() {
  util::info("OnMovementProperties");
  auto gd = gui::GenericDialogue{"Movement properties"};

  for (const auto key : enums::values<notation::MovementKey>()) {
    gd.add_text(enums::to_string(key), //
        [this, key]() { return mpControl->get_current_movement().get(key); },
        [this, key](const std::string &text) {
          mpControl->get_current_movement().set(key, text);
        });
  }

  gd.show();
}

void EngraverWindow::on_left_click() {
  util::log(util::LogLevel::Info, "Left click");
}

void EngraverWindow::resize(const geom::Rect<int> &size) {
  auto menuHeight{30};
  auto width = size.width();
  auto height = size.height();

  mpMenu->set_size({{0, 0}, {width, menuHeight}});

  auto boxheight = mStatusText->get_label_size().y() + 8;
  mStatusText->set_size({{0, height - boxheight}, {width, height}});
  mCanvas->set_size(geom::Rect<int>{{0, 0 + menuHeight},
      width - gui::Scrollbar::scrollbarWidth,
      height - boxheight - menuHeight - gui::Scrollbar::scrollbarWidth});
  mVerticalScrollbar->set_size(
      geom::Rect<int>{{width - gui::Scrollbar::scrollbarWidth, menuHeight},
          gui::Scrollbar::scrollbarWidth,
          height - menuHeight - boxheight - gui::Scrollbar::scrollbarWidth});
  mHorizontalScrollbar->set_size(geom::Rect<int>{
      {0, height - boxheight - gui::Scrollbar::scrollbarWidth},
      width - gui::Scrollbar::scrollbarWidth, gui::Scrollbar::scrollbarWidth});
  mpMenu->redraw();
  mStatusText->redraw();
  mCanvas->redraw();
}

} // namespace interface
