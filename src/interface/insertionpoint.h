#pragma once

#include <cstddef>
#include <iosfwd>
#include <memory>

namespace notation {
class Bar;
class Book;
class Movement;
class Part;
} // namespace notation

namespace interface {

class InsertionPoint {
public:
  // creators
  explicit InsertionPoint(notation::Book &pBook);

  // getters
  notation::Book &get_book() const;
  notation::Movement &get_movement() const;
  notation::Part &get_part() const;
  notation::Bar &get_bar() const;

  std::size_t get_movement_number() const;
  std::size_t get_part_number() const;
  std::size_t get_notation_number() const;

  bool at_end_of_bar() const;
  bool at_end_of_part() const;
  const InsertionPoint get_bar_after_end() const;
  std::size_t get_bar_number() const;
  int get_height() const;
  const InsertionPoint get_initial_bar() const;

  // setters
  void add_to_height(int);
  void change_movement(int);
  void change_part(int);
  void move_insertion_point_by_bars(int);
  void move_insertion_point_horizontally(int);
  void move_to_end_of_bar();
  void select_part(size_t);
  void set_height(int);

private:
  notation::Book *mpBook;
  std::size_t mMovementCount{};
  std::size_t mPartCount{};
  std::size_t mBarCount{};
  std::size_t mNotationCount{};
  int mHeightOffset{};
};

std::ostream &operator<<(std::ostream &, InsertionPoint &);

} // namespace interface
