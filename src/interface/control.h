#pragma once

#include "insertionpoint.h"

#include <vector>

namespace command {
class Command;
}

namespace notation {
class Articulations;
class Book;
class Notation;
} // namespace notation

namespace interface {

class EngraverWindow;
class ManuscriptCanvas;

class Control {
public:
  using SelectionList = std::vector<notation::Notation *>;
  using CommandList = std::vector<std::unique_ptr<command::Command>>;
  // creators
  Control(const Control &) = delete;
  Control &operator=(const Control &) = delete;
  ~Control();

  /** open a document, using the file extension to determine the type */
  static void new_document();
  static void open_document(const std::string &filename);
  static void open_denemo(const std::string &filename);
  static void open_engraver(const std::string &filename);
  static void open_music_xml(const std::string &filename);

  // info
  notation::Book &get_book() const;
  notation::Bar &get_current_bar() const;
  notation::Movement &get_current_movement() const;
  notation::Part &get_current_part() const;
  const InsertionPoint get_insertion_point() const;
  EngraverWindow &get_window() const;
  SelectionList get_selection() const;
  /** recentres the canvas if necessary - returns true if it was */
  bool recentre() const;

  // document actions
  /** display the document in a frame */
  void init();
  void set_window_title(const std::string &title);
  void save_document();
  void save_document_as();

  // commands
  void execute_command(std::unique_ptr<command::Command> &&command);
  void refresh_dirty_widgets();

  // actions
  void add_selected(notation::Notation *selected);
  void change_insertion_point_height(int amount);
  void change_movement(int amount);
  void change_part(int amount);
  void clear_selected();
  void delete_bar_globally();
  void delete_bar_in_part();
  void delete_notation();
  void move_insertion_point_by_bars(int amount);
  void move_insertion_point_horizontally(int amount);
  void set_insertion_point(const InsertionPoint &ip);
  void set_status_text(const std::string &newstatustext) const;
  void try_to_close();
  void zoom(int zoomChange);
  void redraw();

private:
  // private methods
  explicit Control(std::unique_ptr<notation ::Book> &&pBook);

  // instance variables
  CommandList mUndoList{};
  CommandList mRedoList{};
  std::unique_ptr<notation::Book> mpBook;
  SelectionList mpSelected{};
  std::string mFilename{};
  ManuscriptCanvas *mCanvas{};
  std::unique_ptr<EngraverWindow> mWindow{};
  InsertionPoint mInsertionPoint;

  bool mIsClean{true};
};

} // namespace interface
