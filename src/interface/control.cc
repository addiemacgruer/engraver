#include "control.h"

#include "command/command.h"
#include "denemo/denemo.h"
#include "engraverwindow.h"
#include "global.h"
#include "gui/filechooser.h"
#include "gui/label.h"
#include "gui/scrollbar.h"
#include "gui/toolkit.h"
#include "manuscriptcanvas.h"
#include "musicxml/musicxml.h"
#include "notation/bar.h"
#include "notation/book.h"
#include "notation/duration.h"
#include "notation/movement.h"
#include "notation/note.h"
#include "notation/part.h"
#include "notation/rest.h"
#include "util/assert.h"
#include "util/logging.h"
#include "util/maths.h"
#include "xml/node.h"

namespace interface {

Control::~Control() = default;

EngraverWindow &Control::get_window() const {
  return *mWindow;
}

void Control::new_document() {
  auto book = std::make_unique<notation::Book>();
  auto &movement = book->add_movement();
  auto &part = movement.add_part_at_bottom();
  part.add_bar_at_end();

  auto rval = std::unique_ptr<Control>(new Control(std::move(book)));

  rval->init();
  Global::get_instance().add_control(std::move(rval));
}

void Control::init() {
  util::debug("Creating frame");
  mWindow = std::make_unique<EngraverWindow>(*this);
  mCanvas = &mWindow->get_drawing_canvas();
}

void Control::open_engraver(const std::string &filename) {
  if (filename.empty())
    throw std::runtime_error{"Empty filename"};

  auto root = xml::Node::from_document(filename);

  auto book = notation::Book::class_for_node(*root.get());
  auto rval = std::unique_ptr<Control>{new Control(std::move(book))};
  rval->init();
  rval->mFilename = filename;
  rval->mWindow->set_title(util::concat("Engraver: ", filename));
  Global::get_instance().add_control(std::move(rval));
}

void Control::set_window_title(const std::string &title) {
  mWindow->set_title(title);
}

namespace {
template <class F>
auto import_file(const std::string &filename, F rvalfunction) try {
  if (filename.empty())
    throw std::runtime_error{"Tried to open an empty filename"};
  auto rval = rvalfunction(filename);
  rval->init();
  rval->set_window_title(util::concat("Engraver: ", filename));
  Global::get_instance().add_control(std::move(rval));
} catch (std::exception &e) {
  util::critical("Parse failed due to ", e.what());
  throw;
}
} // namespace

void Control::open_denemo(const std::string &filename) {
  import_file(filename, [](const std::string &fname) {
    auto book = denemo::parse_denemo(fname);
    return std::unique_ptr<Control>(new Control(std::move(book)));
  });
}

void Control::open_music_xml(const std::string &filename) {
  import_file(filename, [](const std::string &fname) {
    auto book = std::make_unique<notation::Book>();
    musicxml::parse_music_xml(fname, *book);
    return std::unique_ptr<Control>(new Control(std::move(book)));
  });
}

namespace {
auto select_file_type(
    const std::string &description, const std::string &extension) {
  auto rval = gui::FileChooser::create_saveas_chooser()
                  ->add_filter(description, "*" + extension)
                  .get_filename();
  return util::append_if_needed(rval, extension);
}

auto select_engraver_file() {
  return select_file_type("Engraver file", ".grave");
}
} // namespace

void Control::save_document_as() try {
  mFilename = select_engraver_file();
  save_document();
} catch (std::exception &e) {
  set_status_text(util::concat("Failed to save as:", e.what()));
  mFilename = "";
}

void Control::save_document() try {
  set_status_text("Saving...");
  if (mFilename.empty()) // if not chosen, try once`
    mFilename = select_engraver_file();
  if (mFilename.empty())
    throw std::runtime_error{"Invalid filename"};

  auto xmldocument = xml::Node{};
  get_book().add_node_for_class(xmldocument);
  xmldocument.save_document(mFilename);
  set_status_text("Saved OK");
  mIsClean = true;
  mWindow->set_title(util::concat("Engraver: ", mFilename));
} catch (std::exception &e) {
  set_status_text(util::concat("Failed to save: ", e.what()));
}

bool Control::recentre() const {
  auto &hbar = mWindow->get_horizontal_scrollbar();
  auto ippPos = mCanvas->get_insertion_point_position();
  auto insertionX = ippPos.x();
  if (insertionX < hbar.get_left() || insertionX > hbar.get_right()) {
    hbar.set_centre(insertionX);
    mCanvas->redraw();
    return true;
  }
  return false;
}

void Control::execute_command(std::unique_ptr<command::Command> &&command) try {
  mIsClean = false;
  command->execute(*this);
  if (command->is_undoable()) {
    mUndoList.push_back(std::move(command));
  } else {
    mUndoList.clear();
  }
  mCanvas->require_recentre();
} catch (std::exception &e) {
  set_status_text(util::concat("Command failed: ", e.what()));
}

namespace {
template <class T>
auto test_dirty_and_draw(T &t) {
  if (t.is_dirty())
    t.redraw();
}
} // namespace

void Control::refresh_dirty_widgets() {
  test_dirty_and_draw(mWindow->get_horizontal_scrollbar());
  test_dirty_and_draw(mWindow->get_vertical_scrollbar());
  test_dirty_and_draw(mWindow->get_status_textbox());
}

// info
notation::Book &Control::get_book() const {
  return mInsertionPoint.get_book();
}

notation::Movement &Control::get_current_movement() const {
  return mInsertionPoint.get_movement();
}

notation::Part &Control::get_current_part() const {
  return mInsertionPoint.get_part();
}

notation::Bar &Control::get_current_bar() const {
  return mInsertionPoint.get_bar();
}

// actions
void Control::set_status_text(const std::string &newstatustext) const {
  mWindow->get_status_textbox().set_text(newstatustext);
}

void Control::clear_selected() {
  mpSelected.clear();
}

void Control::add_selected(notation::Notation *selected) {
  mpSelected.push_back(selected);
}

auto Control::get_selection() const -> SelectionList {
  if (!mpSelected.empty())
    return mpSelected;
  auto rval = SelectionList{};
  // if at start of a bar, select last notation in previous
  auto notationno = mInsertionPoint.get_notation_number();
  auto barno = mInsertionPoint.get_bar_number();
  if (notationno == 0 && barno >= 1) {
    auto &bar = get_current_part().at(barno - 1);
    if (bar.empty()) {
      set_status_text("Nothing to select");
      return rval;
    }
    auto &lastnotation = bar.at(bar.size() - 1);
    rval.push_back(&lastnotation.as_notation());
  } else if (notationno > 0) {
    auto &bar = get_current_bar();
    auto &previousbarfinal = bar.at(notationno - 1);
    rval.push_back(&previousbarfinal.as_notation());
  }
  return rval;
}

const InsertionPoint Control::get_insertion_point() const {
  return mInsertionPoint;
}

void Control::set_insertion_point(const InsertionPoint &ip) {
  mInsertionPoint = ip;
}

void Control::change_part(int amount) {
  mInsertionPoint.change_part(amount);
  mCanvas->redraw();
}

void Control::change_movement(int amount) {
  mInsertionPoint.change_movement(amount);
  mCanvas->invalidate_layout();
  mCanvas->redraw();
}

void Control::change_insertion_point_height(int amount) {
  util::debug("Changing insertion point height by ", amount);
  mInsertionPoint.add_to_height(amount);
  mCanvas->redraw();
}

void Control::move_insertion_point_horizontally(int amount) {
  mInsertionPoint.move_insertion_point_horizontally(amount);
  mCanvas->require_recentre();
}

void Control::move_insertion_point_by_bars(int amount) {
  mInsertionPoint.move_insertion_point_by_bars(amount);
  mCanvas->require_recentre();
}

namespace {
auto is_start_of_bar(const InsertionPoint &ip) {
  return ip.get_notation_number() == 0;
}
auto is_start_of_part(const InsertionPoint &ip) {
  return ip.get_notation_number() == 0 && ip.get_bar_number() == 0;
}
} // namespace

void Control::delete_notation() {
  if (is_start_of_part(mInsertionPoint))
    return;
  if (is_start_of_bar(mInsertionPoint)) {
    mInsertionPoint.move_insertion_point_by_bars(-1);
    mInsertionPoint.move_to_end_of_bar();
  }
  auto &bar = get_current_bar();
  bar.delete_notation(mInsertionPoint.get_notation_number() - 1);
  mInsertionPoint.move_insertion_point_horizontally(-1);
  mCanvas->require_recentre();
  mIsClean = false;
}

void Control::zoom(int zoomChange) {
  mCanvas->zoom(zoomChange);
  mCanvas->redraw();
}

void Control::redraw() {
  mCanvas->redraw();
}

void Control::delete_bar_globally() {
  auto &movement = get_current_movement();
  for (auto &part : movement.parts()) {
    part->delete_bar(mInsertionPoint.get_bar_number());
  }
  movement.delete_bar(mInsertionPoint.get_bar_number());
  mIsClean = false;
  mCanvas->redraw();
}

void Control::delete_bar_in_part() {
  auto &part = get_current_part();
  part.delete_bar(mInsertionPoint.get_bar_number());
  // did we delete the last bar?
  if (mInsertionPoint.get_bar_number() >= part.size())
    mInsertionPoint.move_insertion_point_by_bars(-1);
  mCanvas->redraw();
}

Control::Control(std::unique_ptr<notation ::Book> &&pBook)
    : mpBook{std::move(pBook)}, mInsertionPoint{*mpBook.get()} {}

void Control::try_to_close() {
  if (!mIsClean) {
    auto rval = gui::ask_dialogue(
        "Document has unsaved changes.  Really close?", {"Close", "Cancel"});
    if (rval != 0)
      return;
  }
  mWindow->close();
  Global::get_instance().remove_control(this);
}

} // namespace interface
