#pragma once

#include "util/mutex.h"
#include <atomic>
#include <map>

namespace geom {
class Ratio;
}

namespace interface {

class LayoutManager;

/** this class is for the horizontal layout of each bar */
class BarLayout {
public:
  explicit BarLayout(LayoutManager &lm);
  /** set the minimum position for the time */
  void set_position_for_time(const geom::Ratio &time, double width);
  /** get the position for the time */
  double get_position_for_time(const geom::Ratio &time) const;

  /** inform the renderer about the minimum length of each bar */
  void set_minimum_bar_length(double length);
  /** get the length of the longest bar in the movement */
  double get_bar_length() const;

private:
  mutable util::Mutex mMutex{};
  std::map<geom::Ratio, double> mNotationPositions{};
  std::atomic<double> mBarLength{0};
  LayoutManager *mpLm;
};

} // namespace interface
