#include "../engraverwindow.h"
#include "command/adddot.h"
#include "command/addnote.h"
#include "command/addrest.h"
#include "command/articulation.h"
#include "command/courtesy.h"
#include "command/dynamic.h"
#include "command/flatten.h"
#include "command/removedot.h"
#include "command/sharpen.h"
#include "command/span.h"
#include "command/tied.h"
#include "command/tuplet.h"
#include "gui/menu.h"
#include "interface/control.h"
#include "notation/articulations.h"
#include "notation/dynamictype.h"
#include "notation/notenames.h"
#include "notation/span.h"

namespace {
auto shortcut_for_articulation(notation::Articulation art) {
  switch (art) {
  case notation::Articulation::Accent:
    return gui::Shortcut{'>'};
  case notation::Articulation::Staccatissimo:
    return gui::Shortcut{'!'};
  case notation::Articulation::Marcato:
    return gui::Shortcut{'^'};
  case notation::Articulation::StoppedNote:
    return gui::Shortcut{'+'};
  default:
    break;
  }
  return gui::Shortcut{};
}

auto shortcut_for_span_start(notation::Span span) {
  switch (span) {
  case notation::Span::Slur:
    return gui::Shortcut{'['};
  default:
    return gui::Shortcut{};
  }
}

auto shortcut_for_span_end(notation::Span span) {
  switch (span) {
  case notation::Span::Slur:
    return gui::Shortcut{']'};
  default:
    return gui::Shortcut{};
  }
}
} // namespace
void interface::EngraverWindow::add_notation_menu() {
  mpMenu->add_sub_menu("&Notation");
  mpMenu->add_sub_menu("Add note");
  int shortcut = 0;

  for (auto note : enums::values<notation::NoteName>()) {
    auto notename = enums::to_string(note);
    mpMenu->add_menu(notename,
        [this, note, notename]() {
          auto addnote = std::make_unique<command::AddNote>(
              *mpControl, static_cast<int>(note));
          mpControl->execute_command(std::move(addnote));
        },
        gui::Shortcut{'0' + shortcut});
    ++shortcut;
  }

  mpMenu->end_sub_menu();
  mpMenu->add_sub_menu("Add rest");
  mpMenu->add_divider();
  shortcut = 0;
  for (auto note : enums::values<notation::NoteName>()) {
    auto notename = enums::to_string(note);
    mpMenu->add_menu(notename,
        [this, note, notename]() {
          auto addrest = std::make_unique<command::AddRest>(
              *mpControl, static_cast<int>(note));
          mpControl->execute_command(std::move(addrest));
        },
        gui::Shortcut{'0' + shortcut, gui::Shortcut::Flags::Alt});
    ++shortcut;
  }
  mpMenu->end_sub_menu();
  mpMenu->add_divider();
  // triplets
  mpMenu->add_divider();
  mpMenu->add_sub_menu("Insert tuplets");
  mpMenu->add_menu("Start triplets", [this]() {
    auto triplet = std::make_unique<command::Tuplet>(
        mpControl->get_insertion_point(), geom::Ratio(2, 3));
    mpControl->execute_command(std::move(triplet));
  });
  mpMenu->add_menu("Start duplets", [this]() {
    auto triplet = std::make_unique<command::Tuplet>(
        mpControl->get_insertion_point(), geom::Ratio(3, 2));
    mpControl->execute_command(std::move(triplet));
  });
  mpMenu->add_menu("Start other tuplets", [this]() {
    auto query = std::make_unique<command::Tuplet>(
        mpControl->get_insertion_point(), command::TupletType::Ask);
    mpControl->execute_command(std::move(query));
  });
  mpMenu->add_menu("End tuplets", [this]() {
    auto end = std::make_unique<command::Tuplet>(
        mpControl->get_insertion_point(), command::TupletType::End);
    mpControl->execute_command(std::move(end));
  });
  mpMenu->end_sub_menu();
  // articulations
  mpMenu->add_sub_menu("Toggle articulations");
  for (auto artic : enums::values<notation::Articulation>()) {
    auto name = enums::to_string(artic);
    mpMenu->add_menu(name,
        [this, artic]() {
          auto command =
              std::make_unique<command::ToggleArticulation>(*mpControl, artic);
          mpControl->execute_command(std::move(command));
        },
        shortcut_for_articulation(artic));
  }
  mpMenu->end_sub_menu();
  mpMenu->add_menu("Add dot",
      [this]() {
        auto adddotcommand = std::make_unique<command::AddDot>(*mpControl);
        mpControl->execute_command(std::move(adddotcommand));
      },
      gui::Shortcut{'.'});
  mpMenu->add_menu("Remove dot",
      [this]() {
        auto removedotcommand =
            std::make_unique<command::RemoveDot>(*mpControl);
        mpControl->execute_command(std::move(removedotcommand));
      },
      gui::Shortcut{','});
  mpMenu->add_menu("Sharpen",
      [this]() {
        auto sharpen = std::make_unique<command::Sharpen>(*mpControl);
        mpControl->execute_command(std::move(sharpen));
      },
      gui::Shortcut{'='});
  mpMenu->add_menu("Flatten",
      [this]() {
        auto flatten = std::make_unique<command::Flatten>(*mpControl);
        mpControl->execute_command(std::move(flatten));
      },
      gui::Shortcut{'-'});
  mpMenu->add_menu("Toggle courtesy accidental",
      [this]() {
        auto courtesy = std::make_unique<command::Courtesy>(*mpControl);
        mpControl->execute_command(std::move(courtesy));
      },
      gui::Shortcut{'/', gui::Shortcut::Flags::Shift});
  mpMenu->add_menu("Toggle tie",
      [this]() {
        auto tied = std::make_unique<command::Tied>(*mpControl);
        mpControl->execute_command(std::move(tied));
      },
      gui::Shortcut{'~'});
  mpMenu->add_menu("Up one note SHIFT+=");
  mpMenu->add_menu("Down one note SHIFT+-");
  mpMenu->add_sub_menu("Toggle span start");
  for (auto span : enums::values<notation::Span>())
    mpMenu->add_menu(enums::to_string(span),
        [this, span]() {
          auto spancommand = std::make_unique<command::ToggleSpan>(
              *mpControl, span, command::ToggleSpan::Position::Start);
          mpControl->execute_command(std::move(spancommand));
        },
        shortcut_for_span_start(span));
  mpMenu->end_sub_menu();
  mpMenu->add_sub_menu("Toggle span end");
  for (auto span : enums::values<notation::Span>())
    mpMenu->add_menu(enums::to_string(span),
        [this, span]() {
          auto spancommand = std::make_unique<command::ToggleSpan>(
              *mpControl, span, command::ToggleSpan::Position::End);
          mpControl->execute_command(std::move(spancommand));
        },
        shortcut_for_span_end(span));
  mpMenu->end_sub_menu();
  // dynamics
  mpMenu->add_sub_menu("Dynamics");
  for (auto dynamic : enums::values<notation::DynamicType>()) {
    mpMenu->add_menu(enums::to_string(dynamic), //
        [this, dynamic]() {
          auto dyncom = std::make_unique<command::Dynamic>(*mpControl, dynamic);
          mpControl->execute_command(std::move(dyncom));
        });
  }
  mpMenu->add_menu("Clear dynamic", [this]() {
    auto dyncom = std::make_unique<command::Dynamic>(*mpControl);
    mpControl->execute_command(std::move(dyncom));
  });
  mpMenu->end_sub_menu();
  mpMenu->end_sub_menu();
} // namespace interface
