#include "interface/engraverwindow.h"

#include "command/barstyle.h"
#include "command/keysignature.h"
#include "command/timesignature.h"
#include "command/volta.h"
#include "gui/menu.h"
#include "interface/control.h"
#include "notation/barstyle.h"
#include "notation/book.h"
#include "notation/movement.h"
#include "notation/part.h"

void interface::EngraverWindow::add_movement_menu() {
  mpMenu->add_sub_menu("&Movement");
  mpMenu->add_menu(
      "Previous movement", [this]() { mpControl->change_movement(-1); });
  mpMenu->add_menu(
      "Next movement", [this]() { mpControl->change_movement(1); });
  mpMenu->add_divider();
  mpMenu->add_menu("Add movement (after)", [this]() {
    // TODO should be a command we can undo
    auto ip = mpControl->get_insertion_point();
    auto &newmovement = ip.get_book().add_movement();
    auto &part = newmovement.append_part();
    part.add_bar_at_end();
    mpControl->change_movement(1);
  });
  mpMenu->add_menu("Add movement (before)");
  mpMenu->add_menu("Move movement forward");
  mpMenu->add_menu("Move movement back");
  mpMenu->add_divider();
  mpMenu->add_menu("Change starting key", [this]() {
    auto initialInsertionPoint =
        mpControl->get_insertion_point().get_initial_bar();
    auto keysignature =
        std::make_unique<command::ChangeKeySignature>(initialInsertionPoint);
    mpControl->execute_command(std::move(keysignature));
  });

  mpMenu->add_menu("Change starting time signature", [this]() {
    auto initialInsertionPoint =
        mpControl->get_insertion_point().get_initial_bar();
    auto timesignature =
        std::make_unique<command::ChangeTimeSignature>(initialInsertionPoint);
    mpControl->execute_command(std::move(timesignature));
  });
  mpMenu->add_divider();
  mpMenu->add_menu("Change key signature here", [this]() {
    auto initialInsertionPoint = mpControl->get_insertion_point();
    auto keysignature =
        std::make_unique<command::ChangeKeySignature>(initialInsertionPoint);
    mpControl->execute_command(std::move(keysignature));
  });
  mpMenu->add_menu("Change time signature here", [this]() {
    auto initialInsertionPoint = mpControl->get_insertion_point();
    auto timesignature =
        std::make_unique<command::ChangeTimeSignature>(initialInsertionPoint);
    mpControl->execute_command(std::move(timesignature));
  });
  // bar change styles
  mpMenu->add_divider();
  mpMenu->add_sub_menu("Change bar starting style");
  for (const auto barstyle : enums::values<notation::BarStyle>()) {
    mpMenu->add_menu(enums::to_string(barstyle), [this, barstyle]() {
      auto ip = mpControl->get_insertion_point();
      auto bs = std::make_unique<command::BarStyle>(
          command::Position::Start, barstyle, ip);
      mpControl->execute_command(std::move(bs));
    });
  }
  mpMenu->end_sub_menu();
  mpMenu->add_sub_menu("Change bar ending style");
  for (const auto barstyle : enums::values<notation::BarStyle>()) {
    mpMenu->add_menu(enums::to_string(barstyle), [this, barstyle]() {
      auto ip = mpControl->get_insertion_point();
      auto bs = std::make_unique<command::BarStyle>(
          command::Position::End, barstyle, ip);
      mpControl->execute_command(std::move(bs));
    });
  }
  // Volta
  mpMenu->end_sub_menu();
  mpMenu->add_divider();
  mpMenu->add_menu("Set volta number", //
      [this]() {
        auto volta = std::make_unique<command::SetVolta>(
            mpControl->get_insertion_point());
        mpControl->execute_command(std::move(volta));
      });
  // other movement properties
  mpMenu->add_divider();
  mpMenu->add_menu("Set tacet parts in this movement");
  mpMenu->add_divider();
  mpMenu->add_menu(
      "Movement properties", [this]() { on_movement_properties(); });
  mpMenu->end_sub_menu();
}
