#include "../engraverwindow.h"
#include "gui/menu.h"

void interface::EngraverWindow::add_help_menu() {
  mpMenu->add_sub_menu("&Help");
  mpMenu->add_menu("Index");
  mpMenu->add_menu("About");
  mpMenu->end_sub_menu();
}
