#include "interface/engraverwindow.h"

#include "command/addpart.h"
#include "command/clef.h"
#include "gui/genericdialogue.h"
#include "gui/menu.h"
#include "interface/control.h"
#include "notation/instrument.h"
#include "notation/movement.h"
#include "notation/part.h"

void interface::EngraverWindow::add_parts_menu() {
  mpMenu->add_sub_menu("&Parts");
  mpMenu->add_menu("Add part (before)", [this]() {
    auto ip = mpControl->get_insertion_point();
    auto addpart = std::make_unique<command::AddPart>(
        ip.get_movement_number(), ip.get_part_number());
    mpControl->execute_command(std::move(addpart));
  });
  mpMenu->add_menu("Add part (after)", [this]() {
    auto ip = mpControl->get_insertion_point();
    auto addpart = std::make_unique<command::AddPart>(
        ip.get_movement_number(), ip.get_part_number() + 1);
    mpControl->execute_command(std::move(addpart));
  });
  mpMenu->add_menu("Move part up", [this]() {
    auto partno = mpControl->get_insertion_point().get_part_number();
    if (partno == 0)
      throw std::runtime_error{"At first part"};
    auto &movement = mpControl->get_current_movement();
    if (movement.size() <= 2)
      throw std::runtime_error{"Not enough parts to swap"};
    movement.swap_parts(partno, partno - 1);
    mpControl->redraw();
  });
  mpMenu->add_menu("Move part down");
  mpMenu->add_menu("Delete part", [this]() {
    // TODO prompt!
    auto &movement = mpControl->get_current_movement();
    auto partno = mpControl->get_insertion_point().get_part_number();
    movement.delete_part_at(partno);
  });
  mpMenu->add_divider();
  mpMenu->add_menu("Change instrument", [this]() {
    auto &instrument = mpControl->get_current_part().get_instrument();
    auto gd = gui::GenericDialogue("Change instrument information");
    gd.add_text("Instrument name", [&]() { return instrument.get_name(); },
        [&](const std::string &text) { instrument.set_name(text); });
    gd.add_text("Short name", [&]() { return instrument.get_short_name(); },
        [&](const std::string &text) { instrument.set_short_name(text); });
    gd.show();
  });
  mpMenu->add_divider();
  mpMenu->add_menu("Change starting clef", [this]() {
    auto initialInsertionPoint =
        mpControl->get_insertion_point().get_initial_bar();
    auto clefchange =
        std::make_unique<command::ChangeClef>(initialInsertionPoint);
    mpControl->execute_command(std::move(clefchange));
  });
  mpMenu->add_menu("Change clef here", [this]() {
    auto initialInsertionPoint = mpControl->get_insertion_point();
    auto clefchange =
        std::make_unique<command::ChangeClef>(initialInsertionPoint);
    mpControl->execute_command(std::move(clefchange));
  });
  mpMenu->add_divider();
  mpMenu->add_menu("Part properties");
  mpMenu->end_sub_menu();
}
