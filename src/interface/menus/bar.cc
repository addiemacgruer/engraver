#include "command/addbar.h"
#include "gui/menu.h"
#include "interface/control.h"
#include "interface/engraverwindow.h"

void interface::EngraverWindow::add_bar_menu() {
  mpMenu->add_sub_menu("&Bar");
  mpMenu->add_menu("Bar start style");
  mpMenu->add_menu("Bar end style");
  mpMenu->add_divider();
  mpMenu->add_menu("Change clef here");
  mpMenu->add_menu("Insert bar before (movement)", [this]() {
    auto addbar = std::make_unique<command::AddBar>(
        command::Insertion::Global, mpControl->get_insertion_point());
    mpControl->execute_command(std::move(addbar));
  });
  mpMenu->add_menu("Insert bar before (just this part)", [this]() {
    auto addbar = std::make_unique<command::AddBar>(
        command::Insertion::Local, mpControl->get_insertion_point());
    mpControl->execute_command(std::move(addbar));
  });
  mpMenu->add_divider();
  mpMenu->add_menu(
      "Delete bar (movement)", [this]() { mpControl->delete_bar_globally(); });
  mpMenu->add_menu("Delete bar (just this part)",
      [this]() { mpControl->delete_bar_in_part(); });
  mpMenu->end_sub_menu();
}
