#include "interface/engraverwindow.h"

#include "gui/menu.h"
#include "gui/shortcut.h"
#include "interface/control.h"

void interface::EngraverWindow::add_edit_menu() {
  mpMenu->add_sub_menu("&Edit");
  mpMenu->add_menu("Undo", gui::Shortcut{'z', gui::Shortcut::Flags::Command});
  mpMenu->add_divider();
  mpMenu->add_menu("Cut", gui::Shortcut{'x', gui::Shortcut::Flags::Command});
  mpMenu->add_menu("Copy", gui::Shortcut{'c', gui::Shortcut::Flags::Command});
  mpMenu->add_menu("Paste", gui::Shortcut{'v', gui::Shortcut::Flags::Command});
  mpMenu->add_menu(
      "Select All", gui::Shortcut{'a', gui::Shortcut::Flags::Command});
  mpMenu->add_divider();
  mpMenu->add_sub_menu("Move cursor");
  mpMenu->add_menu("Left",
      [this]() { mpControl->move_insertion_point_horizontally(-1); },
      gui::Shortcut{gui::Shortcut::Special::Left});
  mpMenu->add_menu("Right",
      [this]() { mpControl->move_insertion_point_horizontally(1); },
      gui::Shortcut{gui::Shortcut::Special::Right});
  mpMenu->add_menu("Up",
      [this]() { mpControl->change_insertion_point_height(1); },
      gui::Shortcut{gui::Shortcut::Special::Up});
  mpMenu->add_menu("Down",
      [this]() { mpControl->change_insertion_point_height(-1); },
      gui::Shortcut{gui::Shortcut::Special::Down});
  mpMenu->add_divider();
  mpMenu->add_menu("Previous part", [this]() { mpControl->change_part(-1); },
      gui::Shortcut{gui::Shortcut::Special::Up, gui::Shortcut::Flags::Command});
  mpMenu->add_menu("Next part", [this]() { mpControl->change_part(1); },
      gui::Shortcut{
          gui::Shortcut::Special::Down, gui::Shortcut::Flags::Command});
  mpMenu->end_sub_menu();
  mpMenu->add_menu("Delete", [this]() { mpControl->delete_notation(); },
      gui::Shortcut{gui::Shortcut::Special::Backspace});
  mpMenu->end_sub_menu();
}
