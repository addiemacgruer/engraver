#include "../engraverwindow.h"
#include "gui/menu.h"
#include "gui/window.h"
#include "interface/control.h"

void interface::EngraverWindow::add_file_menu() {
  mpMenu->add_sub_menu("&File");
  mpMenu->add_menu("&New", []() { interface::Control::new_document(); },
      gui::Shortcut{'n', gui::Shortcut::Flags::Command});
  mpMenu->add_menu("&Open", [this]() { this->on_open_file(); },
      gui::Shortcut{'o', gui::Shortcut::Flags::Command});
  mpMenu->add_menu("&Save", [this]() { mpControl->save_document(); },
      gui::Shortcut{'s', gui::Shortcut::Flags::Command});
  mpMenu->add_menu("Save &as...", [this]() { mpControl->save_document_as(); },
      gui::Shortcut{
          'v', gui::Shortcut::Flags::Command | gui::Shortcut::Flags::Shift});
  mpMenu->add_divider();
  mpMenu->add_sub_menu("&Import");
  mpMenu->add_menu("Denemo...", [this]() { this->on_import_denemo(); },
      gui::Shortcut{'i', gui::Shortcut::Flags::Command});
  mpMenu->add_menu("MusicXml...", [this]() { this->on_import_music_xml(); });
  mpMenu->end_sub_menu();
  mpMenu->add_sub_menu("&Export");
  mpMenu->end_sub_menu();
  mpMenu->add_sub_menu("Export S&core");
  mpMenu->add_divider();
  mpMenu->add_menu(
      "Current part", [this]() { this->on_export_lilypond_part(); });
  mpMenu->add_menu("Current movement");
  mpMenu->add_menu("All parts", [this]() { this->on_export_lilypond_parts(); });
  mpMenu->add_menu(
      "Conductor's score", [this]() { this->on_export_lilypond_score(); });
  mpMenu->end_sub_menu();
  mpMenu->add_divider();
  mpMenu->add_menu("&Quit", [this]() { mWindow->close(); },
      gui::Shortcut{'q', gui::Shortcut::Flags::Command});
  mpMenu->end_sub_menu();
}
