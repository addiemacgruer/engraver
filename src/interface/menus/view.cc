#include "../engraverwindow.h"
#include "gui/menu.h"
#include "interface/control.h"

void interface::EngraverWindow::add_view_menu() { // TODO this is book and view
  mpMenu->add_sub_menu("&View");
  mpMenu->add_menu("Zoom in", [this]() { mpControl->zoom(1); },
      gui::Shortcut{'=', gui::Shortcut::Flags::Command});
  mpMenu->add_menu("Zoom out", [this]() { mpControl->zoom(-1); },
      gui::Shortcut{'-', gui::Shortcut::Flags::Command});
  mpMenu->end_sub_menu();
  mpMenu->add_sub_menu("Boo&k");
  mpMenu->add_menu("Book properties", [this]() { on_book_properties(); });
  mpMenu->end_sub_menu();
}
