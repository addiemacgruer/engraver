#include "manuscriptcanvas.h"

#include "barlayout.h"
#include "control.h"
#include "engraverwindow.h"
#include "global.h"
#include "gui/renderer.h"
#include "gui/scrollbar.h"
#include "layoutmanager.h"
#include "modern/barview.h"      // TODO why so many views?
#include "modern/movementview.h" // TODO why so many views?
#include "notation/bar.h"
#include "notation/movement.h"
#include "notation/part.h"
#include "prefs.h"
#include "render/renderer.h" // TODO why so many views?
#include "util/assert.h"
#include "util/logging.h"
#include "util/maths.h"
#include "util/string.h"
#include <cmath>

namespace interface {

namespace {
// returns the position of the insertionpoint cursor
geom::Point<> draw_cursor(render::Renderer *mpRenderer,
    interface::LayoutManager *mLm, const InsertionPoint &currentip,
    const std::vector<notation::Part *> &partslist) try {
  auto bluePen =
      mpRenderer->push_pen({{128, 128, 255}, 0.01 * mpRenderer->scale()});
  auto currentpart = currentip.get_part_number();
  auto &selectedBar =
      partslist.at(currentpart)->get_bar(currentip.get_bar_number());
  auto x = double{0};

  if (currentip.get_notation_number() == 0) {
    auto &selection = selectedBar.as_notation();
    x = mLm->get_rect_for_notation(selection).lower_left().x() + 1;
  } else {
    auto &selectedNotation =
        selectedBar.at(currentip.get_notation_number() - 1);
    x = mLm->get_rect_for_notation(selectedNotation.as_notation())
            .upper_right()
            .x();
  }

  auto y = mLm->offset_for(currentpart);

  auto ellipseheight =
      static_cast<double>(1.0 * y - currentip.get_height() / 2.0);
  auto highest = std::min<double>(y - 3.0, ellipseheight);
  auto lowest = std::max<double>(y + 3.0, ellipseheight);
  mpRenderer->draw_line({x, highest}, {x, lowest});
  auto centre = geom::Point<>{x, ellipseheight};
  mpRenderer->draw_ellipse(centre, 1.5, 1);
  return centre;
} catch (std::exception &e) {
  util::error("Error while drawing cursor:", e.what());
  return {};
}

geom::Point<> get_origin(Control *mpControl) {
  auto &window = mpControl->get_window();
  auto &horizontal = window.get_horizontal_scrollbar();
  auto &vertical = window.get_vertical_scrollbar();
  auto xorigin = horizontal.get_left();
  auto yorigin = vertical.get_left();
  return geom::Point<>{xorigin, yorigin};
};

void update_scrollbars(Control *mpControl, const geom::Rect<> &windowSize,
    const geom::Rect<> &drawSize, double mScale) {
  auto &window = mpControl->get_window();
  auto &horizontal = window.get_horizontal_scrollbar();
  auto &vertical = window.get_vertical_scrollbar();
  auto xvalue = horizontal.get_left();
  auto yvalue = vertical.get_left();
  util::debug("Drawing size:", drawSize);
  horizontal.set_scale(
      xvalue, windowSize.width() / mScale, drawSize.width() + 1);
  vertical.set_scale(
      yvalue, windowSize.height() / mScale, drawSize.height() + 1);
  horizontal.redraw();
  vertical.redraw();
};
}; // namespace

void ManuscriptCanvas::on_scroll() {
  if (!mCtrlDown)
    return;
  if (mScale < 1)
    mScale = 1;
}

void ManuscriptCanvas::invalidate_layout() {
  mLm = std::make_unique<interface::LayoutManager>(
      mpControl->get_current_movement());
}

ManuscriptCanvas::ManuscriptCanvas(Control &control) : mpControl{&control} {
  invalidate_layout();
  util::debug("Initialised BasicDrawPane");
  mScale = Prefs::get_instance().default_zoom().get();
}

geom::Point<> ManuscriptCanvas::get_insertion_point_position() const {
  return mInsertionPointPosition;
}

double ManuscriptCanvas::get_scale() const {
  return mScale;
}

void ManuscriptCanvas::render(int x, int y, int w, int h) try {
  auto windowSize =
      geom::Rect<>{{x, y}, static_cast<double>(w), static_cast<double>(h)};

  auto origin = get_origin(mpControl); // / mScale;
  util::debug("Origin: ", origin);

  mCanvasSize = windowSize / mScale + origin;
  auto mpRenderer =
      std::make_unique<gui::Renderer>(mCanvasSize, windowSize, mScale);
  //  auto mpRenderer = Global::get_instance().get_gui_factory().make_renderer(
  //      mCanvasSize, windowSize, mScale);
  mpRenderer->clear_canvas();
  mpRenderer->push_pen({{}, 0.1});

  auto mv = modern::MovementView{mpControl->get_current_movement()};
  auto info = modern::Info{mpRenderer.get(), mLm.get()};
  auto drawSize = mv.draw(info);

  for (auto &error : info.get_errors()) {
    auto eRect = error.get_location();
    util::error("Drawing error: ", error.get_message(), " @", eRect);
    auto red = mpRenderer->push_pen({{255, 0, 0}, 5 * mpRenderer->scale()});
    mpRenderer->draw_rect(eRect.lower_left(), eRect.upper_right(), true);
  }
  util::debug("Drew content: ", drawSize.width() * mScale, " x ",
      drawSize.height() * mScale);

  mpControl->set_status_text(
      util::concat("Cursor position: ", mpControl->get_insertion_point()));

  auto selected = mpControl->get_selection();

  if (selected.size() > 1) { // multiple things are selected
    auto rect = mLm->get_rect_for_notation(*selected.at(0));
    for (auto it = begin(selected) + 1; it != end(selected); ++it) {
      rect &= mLm->get_rect_for_notation(**it);
    }
    mpRenderer->draw_rect(rect.lower_left(), rect.upper_right(), false);
  } else { // draw insertion point
    auto currentip = mpControl->get_insertion_point();
    auto partslist = mpControl->get_current_movement().parts();
    if (partslist.empty()) {
      util::warning("No parts!");
    } else {
      mInsertionPointPosition =
          draw_cursor(mpRenderer.get(), mLm.get(), currentip, partslist);
    }
  }

  update_scrollbars(mpControl, windowSize, drawSize, mScale);
  if (mRequiresRecentre) {
    mRequiresRecentre = false;
    auto movedCanvas = mpControl->recentre();
    if (movedCanvas) {
      // inefficient, but hopefully not frequent.
      ManuscriptCanvas::render(x, y, w, h);
    }
  }
} catch (std::exception &e) {
  util::error("Died due to: ", e.what());
}

void ManuscriptCanvas::on_click() try {
  /*
    util::log(util::LogLevel::Info, "Left click BDP", wme.GetX(), ',',
              wme.GetY());

    geom::Point<> pointOnScore = {
        renderer_->viewingRect().upperLeft().x() +
            wme.GetX() / renderer_->scale(),
        renderer_->viewingRect().lowerRight().y() +
            wme.GetY() / renderer_->scale()};

    util::log(util::LogLevel::Info, "Point equivalent:", pointOnScore);

    auto clickedlist = renderer_->notationAtPoint(pointOnScore);

    util::log(util::LogLevel::Info, "Clicked ", clickedlist.size(), "
things");

    for (auto clicked : clickedlist) {
      util::log(util::LogLevel::Info, "clicked! ", clicked);
    }

    control_->clearSelected();
    if (!clickedlist.empty()) {
      control_->addSelected(clickedlist.at(0));
    }
    this->Refresh();
    */
} catch (std::exception &e) {
  util::error("Failed due to ", e.what());
}

void ManuscriptCanvas::on_key_up() {}

void ManuscriptCanvas::on_key_down() {}

void ManuscriptCanvas::zoom(double zoomChange) {
  mScale += zoomChange;
  if (mScale < 1.0)
    mScale = 1.0;
  redraw();
  util::debug("Zoom changed by ", zoomChange, " to ", mScale);
}

void ManuscriptCanvas::require_recentre() {
  mRequiresRecentre = true;
  redraw();
}

} // namespace interface
