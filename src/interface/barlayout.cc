#include "barlayout.h"

#include "geom/ratio.h"
#include "layoutmanager.h"
#include "util/assert.h"
#include "util/logging.h"

namespace {
constexpr double minimumPositionIncrease = 1;
}

namespace interface {

BarLayout::BarLayout(LayoutManager &lm) : mpLm{&lm} {
  mNotationPositions[geom::Ratio{}] = 1;
}

void BarLayout::set_position_for_time(
    const geom::Ratio &time, double width) try {
  auto _ = mMutex.lock();
  auto needToOrganise{false};
  auto positionIterator = mNotationPositions.find(time);
  if (positionIterator == mNotationPositions.end()) { //  new position
    mNotationPositions[time] = width;
    needToOrganise = true;
  } else { //  it exists.  Compare to existing
    auto oldWidth = positionIterator->second;
    if (width > oldWidth) {
      needToOrganise = true;
      mNotationPositions.at(time) = width;
    }
  }
  if (!needToOrganise)
    return;

  mpLm->make_dirty();
  auto previousWidth = double{0};
  for (auto &np : mNotationPositions) {
    if (np.second < previousWidth)
      np.second = previousWidth;
    previousWidth = np.second + minimumPositionIncrease;
  }
} catch (std::exception &e) {
  util::error("BarLayout::set_position_for_time failed with ", time.numerator(),
      "/", time.denominator(), "->", width);
  util::error("what:", e.what());
  throw;
}

double BarLayout::get_position_for_time(const geom::Ratio &time) const try {
  auto _ = mMutex.lock();
  return mNotationPositions.at(time);
} catch (std::exception &e) {
  util::error("BarLayout::get_position_for_time failed with ", time.numerator(),
      "/", time.denominator(), " (", mNotationPositions.size(),
      " positions known)");
  util::error("what:", e.what());
  return 0;
}

void BarLayout::set_minimum_bar_length(double length) {
  if (mBarLength < length) {
    mBarLength = length;
    mpLm->make_dirty();
  }
}

double BarLayout::get_bar_length() const {
  return mBarLength;
}

} // namespace interface
