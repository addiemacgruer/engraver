#include "insertionpoint.h"

#include "notation/bar.h"
#include "notation/book.h"
#include "notation/movement.h"
#include "notation/part.h"
#include "util/assert.h"
#include <iostream>

namespace interface {

InsertionPoint::InsertionPoint(notation::Book &pBook) : mpBook{&pBook} {}

notation::Book &InsertionPoint::get_book() const {
  return *mpBook;
}

notation::Movement &InsertionPoint::get_movement() const try {
  return get_book().at(mMovementCount);
} catch (const std::exception &) {
  util::error("Tried to get movement ", mMovementCount, " but there are only ",
      get_book().size());
  throw std::runtime_error("gui/insertionpoint::get_movement");
}

notation::Part &InsertionPoint::get_part() const try {
  return get_movement().get_part(mPartCount);
} catch (std::exception &e) {
  util::error("Tried to go beyond current part:", mPartCount, " vs. ",
      get_movement().part_count());
  util::error(e.what());
  throw std::runtime_error{"gui/insertionpoint::get_part"};
}

notation::Bar &InsertionPoint::get_bar() const {
  auto &part = get_part();
  if (mBarCount >= part.size()) {
    util::error(
        "Tried to go beyond current bar:", mBarCount, " vs. ", part.size());
    return part.get_bar(part.size() - 1);
  }
  return part.get_bar(mBarCount);
}

void InsertionPoint::move_insertion_point_by_bars(int amount) {
  if (amount < 0 && static_cast<size_t>(-amount) >= mBarCount) {
    mBarCount = 0;
  } else {
    mBarCount += amount;
  }
  auto barcount = get_movement().get_total_bars();
  if (mBarCount >= barcount - 1)
    mBarCount = barcount - 1;
  mNotationCount = 0;
  util::debug("Moved by bars to ", mBarCount, ':', mNotationCount);
}

void InsertionPoint::move_insertion_point_horizontally(int amount) {
  auto totalBars = get_movement().get_total_bars();
  while (amount > 0) {
    mNotationCount++;
    auto notationInBar = get_bar().size();
    // if last note, move to next bar
    if (mNotationCount > notationInBar && mBarCount < totalBars) {
      move_insertion_point_by_bars(1);
    }
    // if we didn't move (ie. at last bar), make sure we've not overrun
    if (mNotationCount > notationInBar)
      mNotationCount = notationInBar;
    --amount;
  }
  while (amount < 0) {
    // if first note, go to previous bar
    if (mNotationCount == 0 && mBarCount >= 1) {
      move_insertion_point_by_bars(-1);
      auto notationInBar = get_bar().size();
      if (notationInBar > 0)
        mNotationCount = notationInBar + 1;
      else
        mNotationCount = 0;
    }
    // no previous bar, clamp to zero
    if (mNotationCount > 0)
      mNotationCount--;

    ++amount;
  }
  util::debug("Moved by notation to ", mBarCount, ':', mNotationCount);
}

void InsertionPoint::move_to_end_of_bar() {
  mNotationCount = get_bar().size();
}

std::size_t InsertionPoint::get_bar_number() const {
  return mBarCount;
}

std::size_t InsertionPoint::get_notation_number() const {
  return mNotationCount;
}

int InsertionPoint::get_height() const {
  return mHeightOffset;
}

void InsertionPoint::set_height(int height) {
  mHeightOffset = height;
}

void InsertionPoint::add_to_height(int amount) {
  mHeightOffset += amount;
}

void InsertionPoint::change_part(int change) {
  if (change < 0 && static_cast<size_t>(std::abs(change)) > mPartCount) {
    util::info("Trying to move before first part");
    mPartCount = 0;
  } else {
    mPartCount += change;
  }
  auto partCount = get_movement().part_count();
  if (mPartCount >= partCount) {
    util::info("Trying to move past last part");
    mPartCount = partCount - 1;
  }
  mNotationCount = 0; // move to start of bar
  // TODO it might be better to move to a time-equivalent position
}

void InsertionPoint::change_movement(int change) {
  if (change < 0 && static_cast<size_t>(std::abs(change)) > mMovementCount) {
    util::info("Trying to move before first movement");
    mMovementCount = 0;
  } else {
    mMovementCount += change;
  }
  auto movementCount = get_book().size();
  if (mMovementCount >= movementCount) {
    util::info("Trying to move past last movement");
    mMovementCount = movementCount - 1;
  }
}

bool InsertionPoint::at_end_of_bar() const {
  auto barStatus = get_bar().get_bar_status();
  if (barStatus == notation::BarStatus::TooEmpty)
    return false;
  auto barNotationCount = get_bar().get_notation_count();
  util::info("End of bar? ", mNotationCount, ">=", barNotationCount, "?");
  return (mNotationCount >= barNotationCount);
}

bool InsertionPoint::at_end_of_part() const {
  if (mBarCount + 1 < get_part().get_bar_count())
    return false;
  return at_end_of_bar();
}

const InsertionPoint InsertionPoint::get_bar_after_end() const {
  auto rval = *this;
  rval.mBarCount = get_part().get_bar_count();
  rval.mNotationCount = 0;
  return rval;
}

const InsertionPoint InsertionPoint::get_initial_bar() const {
  auto rval = *this;
  rval.mBarCount = 0;
  rval.mNotationCount = 0;
  return rval;
}

std::size_t InsertionPoint::get_movement_number() const {
  return mMovementCount;
}
std::size_t InsertionPoint::get_part_number() const {
  return mPartCount;
}
void InsertionPoint::select_part(size_t part) {
  auto maximum = get_movement().parts().size();
  mPartCount = part < maximum ? part : maximum - 1;
}

std::ostream &operator<<(std::ostream &out, InsertionPoint &ip) {
  out << "Part: " << (ip.get_part_number() + 1)
      << " Bar: " << (ip.get_bar_number() + 1)
      << " Notation: " << (ip.get_notation_number());
  return out;
}

} // namespace interface
