#pragma once

#include "geom/point.h"
#include "geom/rect.h"
#include "gui/canvas.h"
#include <memory>

namespace render {
class Renderer;
}

namespace interface {

class LayoutManager;
class EngraverWindow;
class Control;

class ManuscriptCanvas : public gui::Canvas {
public:
  // creator
  explicit ManuscriptCanvas(Control &control);
  ManuscriptCanvas(const ManuscriptCanvas &) = delete;

  // getters
  geom::Point<> get_insertion_point_position() const;
  double get_scale() const;

  // setters
  void invalidate_layout();
  void zoom(double zoomChange);
  void require_recentre();

  // Canvas
  void render(int x, int y, int w, int h) override;

private:
  void on_click();
  void on_key_down();
  void on_key_up();
  void on_scroll();
  //  void render();

  double mScale{10};
  Control *mpControl;
  bool mCtrlDown{false};
  //  bool mShiftDown{false};
  //  bool mAltDown{false};
  bool mRequiresRecentre{false};
  std::unique_ptr<interface::LayoutManager> mLm;
  geom::Rect<> mCanvasSize{};
  geom::Point<> mInsertionPointPosition{};
};
} // namespace interface
