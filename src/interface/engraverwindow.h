#pragma once

#include "manuscriptcanvas.h"
#include <functional>
#include <memory>

namespace gui {
class Window;
class Menu;
class Label;
class ManuscriptCanvas;
class Scrollbar;
} // namespace gui

/** \namespace interface \brief user interface elements specific to this app -
 * windows, menus,
 * ... */
namespace interface {

class Control;

class EngraverWindow {
public:
  // creators
  explicit EngraverWindow(Control &controller);
  EngraverWindow(const EngraverWindow &) = delete;
  EngraverWindow &operator=(const EngraverWindow &) = delete;
  ~EngraverWindow();

  // getters
  ManuscriptCanvas &get_drawing_canvas() const;
  gui::Scrollbar &get_horizontal_scrollbar() const;
  geom::Rect<int> get_size() const;
  gui::Label &get_status_textbox() const;
  gui::Scrollbar &get_vertical_scrollbar() const;
  gui::Window &get_window_widget() const;

  // setters
  void close();
  void on_book_properties();
  void on_export_lilypond_part();
  void on_export_lilypond_parts();
  void on_export_lilypond_score();
  void on_import_denemo();
  void on_import_music_xml();
  void on_left_click();
  void on_movement_properties();
  void on_open_file();
  void resize(const geom::Rect<int> &size);
  void set_title(const std::string &title);

private:
  std::unique_ptr<gui::Window> mWindow;
  Control *mpControl;
  std::unique_ptr<gui::Menu> mpMenu;
  std::unique_ptr<ManuscriptCanvas> mCanvas;
  std::unique_ptr<gui::Label> mStatusText;
  std::unique_ptr<gui::Scrollbar> mVerticalScrollbar;
  std::unique_ptr<gui::Scrollbar> mHorizontalScrollbar;

  void add_bar_menu();
  void add_edit_menu();
  void add_file_menu();
  void add_help_menu();
  void add_movement_menu();
  void add_notation_menu();
  void add_parts_menu();
  void add_view_menu();
  void create_menus();
  void create_scroll_bars();
};

} // namespace interface
