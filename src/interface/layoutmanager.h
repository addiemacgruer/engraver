#pragma once

#include "geom/point.h"
#include "geom/ratio.h"
#include "geom/rect.h"
#include "notation/movement.h"
#include "notation/notation.h"
#include "util/mutex.h"

#include <atomic>
#include <map>
#include <memory>
#include <vector>

namespace interface {

class BarLayout;

/** this is the on-canvas position of each bit of notation,  so we can select it
 * by clicking and draw a cursor near to it */
class NotationArea {
public:
  // creator
  NotationArea(const notation::Notation &notation, const geom::Rect<> &area);
  // getters
  geom::Rect<> area() const;
  const notation::Notation &notation() const;
  bool operator<(const NotationArea &other) const;

private:
  const notation::Notation *mpNotation;
  geom::Rect<> mArea;
};

/** the class is for the vertical layout of the parts */
class Offset {
public:
  double offset;
  double top;
  double bottom;
};

/** overall manager for positioning,  passed to all draw methods to allow
 * feedback.  Okay for multithreaded access */
class LayoutManager {
public:
  // creator
  LayoutManager(const LayoutManager &) = delete;
  explicit LayoutManager(notation::Movement &movement);
  // getter
  /** get the BarLayout for a specific bar */
  BarLayout *get_bar_layout(size_t bar) const;
  const std::vector<const notation::Notation *> get_notation_at_point(
      const geom::Point<> &point) const;
  std::size_t get_notation_list_count() const;
  geom::Rect<> get_rect_for_notation(const notation::Notation &notation) const;
  double height_for(size_t part) const;
  bool needs_redraw() const;
  double offset_for(size_t part) const;
  // setters
  void add_to_notation_list(
      const notation::Notation &notation, const geom::Rect<> &screenPosition);
  void check_offsets_count();
  /** do final fixup once all of the layoutrenderer threads have finished */
  void finalise();
  void make_dirty();
  void make_clean();
  /** called to delete all of the existing layout and start again */
  void reset();
  void update_part_height(size_t part, double top, double bottom);

private:
  notation::Movement *mpMovement;
  mutable bool mIsNotationListSorted{false};
  mutable util::Mutex mBarLengthMutex{};
  mutable util::Mutex mNotationMutex{};
  mutable util::Mutex mPartOffsetsMutex{};
  mutable std::vector<std::unique_ptr<NotationArea>> mNotationList;
  static const double epsilon;
  std::atomic<bool> mNeedsRedraw{false};
  std::vector<std::unique_ptr<BarLayout>> mBarLayouts;
  std::vector<std::unique_ptr<Offset>> mPartOffsets;
};

} // namespace interface
