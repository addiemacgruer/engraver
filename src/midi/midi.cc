#include "midi.h"

#include "notation/bar.h"
#include "notation/clef.h"
#include "notation/movement.h"
#include "notation/note.h"
#include "notation/part.h"
#include "util/logging.h"
#include "util/maths.h"
#include "util/string.h"

#include <arpa/inet.h>
#include <cstring>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <sys/stat.h>

namespace {

enum class MidiEvent {
  // Channel voice messages
  NoteOff = 0b1000,
  NoteOn = 0b1001,
  Aftertouch = 0b1010,
  ControlChange = 0b1011,
  ProgramChange = 0b1100,
  ChannelPressure = 0b1101,
  PitchWheel = 0b1110,
  // Channel mode messages
  ChannelModeMessages = 0b1011,
  // SystemCommonMessages
  SystemExclusive = 0b1111
};

enum class SystemExclusive {
  SystemExclusive = 0b0000,
  SongPositionPointer = 0b0010,
  SongSelect = 0b0011,
  TuneRequest = 0b0110,
  ExclusiveEnd = 0b0111,
  TimingClock = 0b1000,
  Start = 0b1010,
  Continue = 0b1011,
  Stop = 0b1100,
  ActiveSensing = 0b1110,
  MetaEvent = 0b1111
};

enum class MetaEvent {
  SequenceNumber = 0x00,
  TextEvent = 0x01,
  Copyright = 0x02,
  SequenceName = 0x03,
  InstrumentName = 0x04,
  Lyric = 0x05,
  Marker = 0x06,
  CuePoint = 0x07,
  MidiChannelPrefix = 0x20,
  EndOfTrack = 0x2f,
  SetTempo = 0x51,
  SmpteOffset = 0x54,
  TimeSignature = 0x58,
  KeySignature = 0x59,
  SequencerSpecific = 0x7f
};

union {
  uint16_t s;
  unsigned char c[2];
} constexpr static d{1};

bool is_little_endian() {
  return d.c[0] == 1;
}

// this is a conversion function, so Clang is unhappy about the precision loss
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
template <class T>
T readfile(std::fstream &handle) {
  T rval;
  handle.read(reinterpret_cast<char *>(&rval), sizeof(rval));

  if (!is_little_endian())
    return rval;

  switch (sizeof(rval)) {
  case 1:
    return rval;
  case 2:
    return ntohs(rval);
  case 4:
    return ntohl(rval);
  }
}
#pragma clang diagnostic pop

uint8_t getc(std::fstream &handle) {
  return readfile<uint8_t>(handle);
}

unsigned long read_var_length(std::fstream &handle) {
  unsigned long value{getc(handle)};
  if (value & 0x80) {
    value &= 0x7f;
    uint8_t c;
    do {
      c = getc(handle);
      value = (value << 7) + (c & 0x7f);
    } while (c & 0x80);
  }

  return (value);
}

std::string readfilestring(std::fstream &handle, unsigned long length) {
  auto charbuf = std::make_unique<char[]>(length + 1);
  handle.read(charbuf.get(), static_cast<std::streamsize>(length));
  charbuf[length] = '\0';
  auto rval = std::string{charbuf.get()};
  return rval;
}

struct Chunk {
  std::string identifier{};
  uint32_t length{0};
};

Chunk readChunk(std::fstream &handle) {
  auto identifier = readfilestring(handle, 4);
  auto length = readfile<uint32_t>(handle);
  util::info("Read header:", identifier, " length:", length);
  return {identifier, length};
}

void midi_header(std::fstream &handle, notation::Movement *) {
  auto format = readfile<uint16_t>(handle);
  auto nTrackChunks = readfile<uint16_t>(handle);
  auto division = readfile<int16_t>(handle);

  util::info(" midi:", format, ',', nTrackChunks, ',', division);
}

long get_file_size(std::string filename) {
  struct stat stat_buf;
  int rc = stat(filename.c_str(), &stat_buf);
  return rc == 0 ? stat_buf.st_size : -1;
}

void meta_event(std::fstream &handle, notation::Movement *) {
  auto meta_event_type = static_cast<MetaEvent>(readfile<uint8_t>(handle));
  auto event_length = read_var_length(handle);

  switch (meta_event_type) {
  case MetaEvent::SequenceNumber: {
    if (event_length != 2)
      util::log(util::LogLevel::Warning,
          "SequenceNumber length not 2:", event_length);
    auto sequenceNumber = readfile<uint16_t>(handle);
    util::info("-SequenceNumber", sequenceNumber);
    break;
  }
  case MetaEvent::Copyright: {
    auto text = readfilestring(handle, event_length);
    util::info("-Copyright:'", text, '\'');
    break;
  }
  case MetaEvent::TextEvent: {
    auto text = readfilestring(handle, event_length);
    util::info("-TextEvent:'", text, '\'');
    break;
  }
  case MetaEvent::InstrumentName: {
    auto text = readfilestring(handle, event_length);
    util::info("-InstrumentName:'", text, '\'');
    break;
  }
  case MetaEvent::SequenceName: {
    auto text = readfilestring(handle, event_length);
    util::info("-SequenceName:'", text, '\'');
    break;
  }
  case MetaEvent::Lyric: {
    auto text = readfilestring(handle, event_length);
    util::info("-Lyric:'", text, '\'');
    break;
  }
  case MetaEvent::CuePoint: {
    auto text = readfilestring(handle, event_length);
    util::info("-CuePoint:'", text, '\'');
    break;
  }
  case MetaEvent::EndOfTrack:
    util::info("~End of Track");
    break;
  case MetaEvent::SetTempo: {
    if (event_length != 3)
      util::log(
          util::LogLevel::Warning, "SetTempo length not 3:", event_length);

    long tempo = readfile<uint8_t>(handle);
    tempo <<= 8;
    tempo += readfile<uint8_t>(handle);
    tempo <<= 8;
    tempo += readfile<uint8_t>(handle);

    util::info("-Time per crotchet: ", tempo, "uS");
    break;
  }
  case MetaEvent::TimeSignature: {
    if (event_length != 4)
      util::log(
          util::LogLevel::Warning, "TimeSignature length not 3:", event_length);

    int nn = readfile<uint8_t>(handle);
    int dd = readfile<uint8_t>(handle);
    int cc = readfile<uint8_t>(handle);
    int bb = readfile<uint8_t>(handle);

    util::info("-Time Signature:", nn, ':', util::is_power_of_two(dd),
        " - midi clocks per crotchet?:", cc,
        " - # of notated 32nd notes per crotchet:", bb);
    break;
  }
  default:
    auto i_meta_event_type = static_cast<int>(meta_event_type);
    util::info(
        "Got a meta-event ", i_meta_event_type, " of length ", event_length);
    for (size_t i = 0; i < event_length; ++i) {
      readfile<uint8_t>(handle);
    }
  }
}

void midi_track(
    std::fstream &handle, long trackend, notation::Movement *movement) {
  while (handle.tellg() != trackend) {
    auto deltatime = read_var_length(handle);
    util::info("   dT ", deltatime);
    auto eventcode = readfile<uint8_t>(handle);
    // midi event is split into two nibbles
    auto midievent = static_cast<MidiEvent>((eventcode & 0xF0) >> 4);
    auto channel = static_cast<int>(eventcode & 0x0F);

    switch (midievent) {
    case MidiEvent::ControlChange: {
      auto c = readfile<uint8_t>(handle);
      auto v = readfile<uint8_t>(handle);
      util::info(
          "Control change: channel ", channel, " controller #", c, "->", v);
      break;
    }
    case MidiEvent::SystemExclusive: {
      auto systemExclusive = static_cast<SystemExclusive>(channel);
      switch (systemExclusive) {
      case SystemExclusive::SystemExclusive: {
        auto length = read_var_length(handle);
        util::info("SystemExclusive length:", length);
        for (size_t i = 0; i < length; ++i)
          readfile<uint8_t>(handle);
        break;
      }
      case SystemExclusive::MetaEvent:
        meta_event(handle, movement);
        break;
      default:
        auto midieventname =
            static_cast<int>(midievent); // TODO get name of midievent
        util::error("Couldn't handle system SystemExclusive:", midieventname,
            ',', channel);
        throw std::runtime_error("Stuck");
      }
      break;
    }
    default:
      auto midieventname =
          static_cast<int>(midievent); // TODO get name of midievent
      util::error(
          "Couldn't handle midi event: channel #", channel, " ", midieventname);
      throw std::runtime_error("Stuck");
    }
  }
}

} // namespace
namespace midi {

#if 0
std::unique_ptr<notation::Movement> parse_midi(const std::string &file) {
  auto rval = std::make_unique<notation::Movement>();

  auto filesize = get_file_size(file);
  auto handle = std::fstream{file, std::ios::in | std::ios::binary};

  while (handle.good()) {
    auto header = readChunk(handle);
    if (header.identifier == "MThd")
      midi_header(handle, rval.get());
    else if (header.identifier == "MTrk") {
      auto tellg = static_cast<long>(handle.tellg());
      midi_track(handle, tellg + header.length, rval.get());
    } else {
      util::warning("Unknown midi header:", header.identifier);
      auto currentpos = static_cast<long>(handle.tellg());
      auto newpos = currentpos + header.length;
      handle.seekg(newpos);
    }
    if (handle.tellg() == filesize)
      break;
  }

  throw std::runtime_error{"Not implemented"};
}
#endif
} // namespace midi
