#pragma once

#include <memory>

namespace notation {
class Movement;
}

/** \namespace midi \brief Functions for working with MIDI files
 *<a href="http://">MIDI</a> */
namespace midi {
std::unique_ptr<notation::Movement> parse_midi(const std::string &file);
}
