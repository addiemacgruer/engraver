#pragma once

#include <string>

namespace xml {

class Node;

template <class T>
class Serialise {
public:
  /* big five */
  Serialise() = default;
  Serialise(const Serialise &) = default;
  Serialise(Serialise &&) noexcept = default;
  Serialise &operator=(const Serialise &) = default;
  Serialise &operator=(Serialise &&) = default;
  virtual ~Serialise<T>() noexcept = default;

  /* adds the requested item, and all subchildren, as a node to the xml tree */
  virtual void add_node_for_class(Node &parent) = 0;
  virtual std::string get_xml_name() = 0;
};

} // namespace xml
