#pragma once

#include "enum_templates.h" // for enums::to_string, enums::from_string
#include "util/logging.h"   // for util::error
#include "util/string.h"    // for util::concat

#include <functional>    // for function
#include <list>          // for list
#include <memory>        // for unique_ptr
#include <string>        // for string
#include <unordered_map> // for unordered_map

namespace pugi {
class xml_node;
}

/** \namespace xml \brief Classes for reading and writing XML */
namespace xml {

class Node {
public:
  using NodeFunction = std::function<void(const Node &)>;
  // creators
  Node();
  explicit Node(const std::string &name);
  Node(const Node &) = delete;
  Node(Node &&) = default;
  ~Node();

  const std::string &get_name() const;
  void set_name(const std::string &name);
  size_t get_hash() const;
  Node *append_child(const std::string &name = {});

  // functional iterators
  /** apply a function which should only apply to one child, and generate
   * warnings if not */
  void once(const std::string &name, const NodeFunction &f) const;
  /** apply a function to every child with this name */
  size_t many(const std::string &name, const NodeFunction &f) const;
  /** apply a funtion to every child - needed if eg.\ order is important */
  size_t every(const NodeFunction &f) const;
  /** mark that we don't care about this node, or its children */
  void ignore(const std::initializer_list<std::string> &list) const;

  // getters
  template <class T>
  T get_attribute(const std::string &name, T defaultvalue = {}) const {
    if (mAttributes.count(name) == 0)
      return defaultvalue;
    auto attributeValue = get_attribute_value(name);
    return type_for_string<T>(attributeValue);
  }

  template <class T>
  T get_value() const {
    auto childValue = get_string_value();
    if constexpr (std::is_enum<T>::value) {
      return enums::from_string<T>(childValue);
    } else {
      return type_for_string<T>(childValue);
    }
  }

  // setters
  template <class T>
  void add_node(const std::string &name, T value) {
    auto child = append_child(name);
    child->set_value<T>(value);
  }

  template <class T>
  void set_value(const T &value) {
    if constexpr (std::is_enum<T>::value) {
      auto string = enums::to_string(value);
      set_string_value(string);
    } else {
      auto string = util::concat(value);
      set_string_value(string);
    }
  }

  template <>
  inline void set_value<std::string>(const std::string &value) {
    set_string_value(value);
  }

  template <>
  inline void set_value<bool>(const bool &value) {
    set_string_value(value ? "true" : "false");
  }

  template <class T>
  inline void set_attribute(const std::string &name, const T &value) {
    std::string string = util::concat(value);
    set_attrib_value(name, string);
  }

  void save_document(const std::string &filename);

  // static
  static std::unique_ptr<Node> from_document(const std::string &filename);
  static constexpr size_t hash(const std::string &string);

  void calculate_unused() const;
  void clear_unused() const;
  std::string get_path() const;

private:
  // methods
  template <class T>
  T type_for_string(const std::string &string) const {
    if constexpr (std::is_enum<T>::value) {
      return enums::from_string<T>(string);
    } else if constexpr (std::is_integral<T>::value) {
      return static_cast<T>(std::atoi(string.c_str()));
    } else {
      return static_cast<T>(string); // hope there's a pre-defined conversion
    }
  }

  template <>
  bool type_for_string<bool>(const std::string &string) const {
    if (string == "true")
      return true;
    if (string == "false")
      return false;
    util::error("Bad boolean value in '", get_path(), "': ", string);
    return false;
  }

  void convert_node_to_pugi(pugi::xml_node &pugi);
  std::string get_string_value() const;
  std::string get_attribute_value(const std::string &name) const;
  void set_string_value(const std::string &value);
  void set_attrib_value(const std::string &name, const std::string &value);

  // instance variables
  Node *parent{};
  std::string mName{};
  std::string mValue{};
  size_t mHash{};
  std::list<Node> mChildren{};
  std::unordered_map<std::string, std::string> mAttributes{};
  mutable std::unordered_map<std::string, int> mChildMap{};
};

/** adds a node with a given name if the test is true */
template <class T>
void add_node_if_true(
    xml::Node *node, const std::string &name, T value, bool test) {
  if (test)
    node->add_node(name, value);
}

/** tries to get the named value by node.once, or returns the defval if missing
 */
template <class T>
T get_or_default(const xml::Node &node, const std::string &name, T defval) {
  T rval{defval};
  node.once(name, [&](const xml::Node &n) { rval = n.get_value<T>(); });
  return rval;
}

/** tries to get the named value by node.once, or returns a default-constructed
 * value */
template <class T>
T get_or_default(const xml::Node &node, const std::string &name) {
  T rval{};
  node.once(name, [&](const xml::Node &n) { rval = n.get_value<T>(); });
  return rval;
}

} // namespace xml
