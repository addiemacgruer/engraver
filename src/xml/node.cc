#include "node.h"

#include "util/hash.h"
#include "util/logging.h"

#include <pugixml.hpp>
#include <sstream>

namespace xml {

Node::Node() = default;

Node::Node(const std::string &name) : mName{name}, mHash{hash(name)} {}

Node::~Node() {
  for (const auto &[key, value] : mChildMap) {
    if (value > 0) {
      auto path = get_path();
      util::error("Node: '", path, "' ignored child '", key, "' with ", value,
          " copies");
    }
  }
}

const std::string &Node::get_name() const {
  return mName;
}

void Node::set_name(const std::string &name) {
  mName = name;
  mHash = hash(name);
}

Node *Node::append_child(const std::string &name) {
  mChildren.emplace_back(name);
  mChildren.back().parent = this;
  return &mChildren.back();
}

size_t Node::get_hash() const {
  return mHash;
}

size_t Node::many(const std::string &name, const NodeFunction &f) const {
  size_t count{0};
  auto namehash = hash(name);
  for (auto &node : mChildren) {
    if (namehash != node.get_hash())
      continue;
    if (node.get_name() != name) // in case of hash collisions
      continue;
    f(node);
    count++;
    if (!mChildMap.empty()) {
      --mChildMap[name];
    }
  }
  //   util::info("Node::many matched '",name,"' ",count," times in
  //   '",get_name(),"'");
  return count;
}

void Node::once(const std::string &name, const NodeFunction &f) const {
  auto count = many(name, f);
  if (count > 1)
    util::warning("Node::once '", name, "' matched ", count, " nodes in '",
        get_name(), "'");
}

size_t Node::every(const NodeFunction &f) const {
  for (const auto &node : mChildren) {
    f(node);
    if (!mChildMap.empty()) {
      --mChildMap[node.get_name()];
    }
  }
  return mChildren.size();
}

void Node::ignore(const std::initializer_list<std::string> &list) const {
  for (const auto &string : list) {
    many(string, [](const Node &n) { n.clear_unused(); });
    mChildMap[string] = 0;
  }
}

void Node::convert_node_to_pugi(pugi::xml_node &pugi) {
  pugi.set_name(mName.c_str());
  pugi.text().set(mValue.c_str());
  // TODO attributes
  for (auto &subnode : mChildren) {
    auto pugichild = pugi.append_child();
    subnode.convert_node_to_pugi(pugichild);
  }
}

void Node::save_document(const std::string &filename) {
  pugi::xml_document document{};
  convert_node_to_pugi(document);
  constexpr auto flags = pugi::format_no_empty_element_tags | pugi::format_raw;
  auto result = document.save_file(filename.c_str(), "", flags);
  if (!result) { // error
    util::error("Xml Save Error:", result);
    throw std::runtime_error("xml failed to save");
  }
}

namespace {
void convert_pugi_to_nodes(
    Node *parent, pugi::xml_node &node) { // TODO could be multithreaded
  parent->set_name(node.name());
  parent->set_value(node.child_value());
  for (auto &attrib : node.attributes()) {
    parent->set_attribute(attrib.name(), attrib.value());
  }
  for (auto &pugichild : node) {
    if (pugichild.name()[0] == '\0')
      continue;
    auto child = parent->append_child();
    convert_pugi_to_nodes(child, pugichild);
  }
  parent->calculate_unused();
}
} // namespace

std::unique_ptr<Node> Node::from_document(const std::string &filename) {
  util::debug("Started parsing file: '", filename, "'");
  pugi::xml_document root{};
  auto result = root.load_file(filename.c_str());
  if (!result) {
    util::error("Failed to load pugi file: ", result.description());
    throw std::runtime_error{result.description()};
  }
  auto rval = std::make_unique<Node>();
  auto firstChild = root.first_child();
  convert_pugi_to_nodes(rval.get(), firstChild);
  util::debug("Finished parsing file: '", filename, "'");
  return rval;
}

constexpr size_t Node::hash(const std::string &string) {
  return util::hash(string);
}

std::string Node::get_string_value() const {
  return mValue;
}

void Node::set_string_value(const std::string &value) {
  mValue = value;
}

std::string Node::get_attribute_value(const std::string &name) const try {
  return mAttributes.at(name);
} catch (std::exception &) {
  util::error("No such xml attribute: ", name, " in ", get_name());
  return "";
}

void Node::set_attrib_value(const std::string &name, const std::string &value) {
  mAttributes[name] = value;
}

void Node::calculate_unused() const {
  mChildMap.clear();
  for (const auto &child : mChildren) {
    auto name = child.get_name();
    if (mChildMap.count(name) == 0) {
      mChildMap[name] = 1;
    } else {
      ++mChildMap[name];
    }
  }
}

void Node::clear_unused() const {
  for (const auto &child : mChildren) {
    child.clear_unused();
  }
  mChildMap.clear();
}

std::string Node::get_path() const {
  std::string rval{};
  if (parent) {
    rval = parent->get_path();
    rval += "->";
  }
  rval += get_name();
  return rval;
}

} // namespace xml
