#include "duration.h"

#include <iostream>

namespace lilypond {

Duration::Duration(const notation::Duration &duration, std::ostream &out)
    : LilyType(out) {
  out << duration.get_base_duration();
  for (int i = 0, dots = duration.get_dots(); i < dots; ++i)
    out << '.';
}

Duration::~Duration() = default;

template <>
std::unique_ptr<LilyType> make_lily(
    const notation::Duration &duration, std::ostream &out) {
  return std::make_unique<Duration>(duration, out);
}

} // namespace lilypond
