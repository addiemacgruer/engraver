#pragma once

#include "lilytype.h"

#include "notation/duration.h"

namespace lilypond {
class Duration : public LilyType {

public:
  Duration(const notation::Duration &, std::ostream &);
  ~Duration() override;
};

template <>
std::unique_ptr<LilyType> make_lily(const notation::Duration &, std::ostream &);

} // namespace lilypond
