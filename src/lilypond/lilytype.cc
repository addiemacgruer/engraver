#include "lilytype.h"

namespace lilypond {

LilyType::LilyType(std::ostream &out) : mOut{out} {}

LilyType::~LilyType() {
  write_end(mOut);
}

void LilyType::write_end(std::ostream &) noexcept { //  default does nothing
}

} // namespace lilypond
