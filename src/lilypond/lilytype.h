#pragma once

#include <iosfwd> //  for std::ostream
#include <memory> //  for std::unique_ptr

namespace lilypond {

class LilyType {
public:
  explicit LilyType(std::ostream &out);
  virtual ~LilyType();

protected:
  virtual void write_end(std::ostream &out) noexcept;

private:
  std::ostream &mOut;
};

template <class T>
std::unique_ptr<LilyType> make_lily(const T &type, std::ostream &out);

} // namespace lilypond
