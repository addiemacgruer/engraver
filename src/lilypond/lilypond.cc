#include "lilypond.h"

#include "accidental.h"
#include "articulation.h"
#include "duration.h"
#include "notation/articulations.h"
#include "notation/bar.h"
#include "notation/barstyle.h"
#include "notation/book.h"
#include "notation/clef.h"
#include "notation/dynamic.h"
#include "notation/key.h"
#include "notation/markup.h"
#include "notation/movement.h"
#include "notation/movementkey.h" // for MovementKey
#include "notation/note.h"
#include "notation/part.h"
#include "notation/rehearsalmark.h"
#include "notation/rest.h"
#include "notation/span.h"
#include "notation/tempo.h"
#include "notation/timesignature.h"
#include "notation/tuplet.h"
#include "notation/volta.h"
#include "pitch.h"
#include "util/logging.h"
#include "util/string.h"

#include <fstream>
#include <set>

namespace lilypond {

using csPart = const notation::Part *;

namespace {
void write_version(std::ofstream &out) {
  out << "\\version \"2.18.2\"\n";
}

void write_clef(std::ofstream &out, const notation::Clef &clef) {
  auto clefNameLower = util::lower_case_string(clef.get_name());
  out << "\\clef " << clefNameLower;
}

void write_key(std::ofstream &out, const notation::Key &key) {
  out << "\\key ";
  auto pitch = key.get_relative_major();
  auto lpitch = make_lily(pitch, out);
  out << " \\major ";
}

void write_time_signature(
    std::ofstream &out, const notation::TimeSignature &ts) {
  out << "\\time " << ts.get_beats() << '/' << ts.get_beat_lengths() << ' ';
}

void write_octave(std::ofstream &out, int octave) {
  if (octave == 3)
    return;
  for (int i = 3; i < octave; ++i) {
    out << '\'';
  }
  for (int i = 3; i > octave; --i) {
    out << ',';
  }
}

void write_span_end(std::ofstream &out, const notation::Span &span) {
  switch (span) {
  case notation::Span::Slur:
    out << ')';
    break;
  case notation::Span::Crescendo:
  case notation::Span::Diminuendo:
    out << "\\!";
    break;
  default:
    util::warning("Didn't write end span:", span);
    break;
  }
}

void write_span_start(std::ofstream &out, const notation::Span &span) {
  switch (span) {
  case notation::Span::Slur:
    out << '(';
    break;
  case notation::Span::Crescendo:
    out << "\\<";
    break;
  case notation::Span::Diminuendo:
    out << "\\>";
    break;
  default:
    util::warning("Didn't write start span:", span);
    break;
  }
}

void write_grace_note(std::ofstream &out, notation::Grace &grace) {
  switch (grace) {
  case notation::Grace::Grace:
    out << "\\grace { ";
    break;
  case notation::Grace::Acciaccatura:
    out << "\\acciaccatura { ";
    break;
  case notation::Grace::Appoggiatura:
    out << "\\appoggiatura { ";
    break;
  case notation::Grace::None:
    break;
  }
}

void write_dynamic(std::ofstream &out, notation::Dynamic &dynamic) {
  if (dynamic.get_type() != notation::DynamicType::custom) {
    out << '\\' << dynamic.get_type();
  } else {
    util::error(
        "Failed to deal with a custom / unknown dynamic: ", dynamic.get_type());
  }
}

void write_note(std::ofstream &out, notation::Note &note) {
  auto grace = note.get_duration().get_grace();
  write_grace_note(out, grace);
  auto pitch = note.get_pitch();
  make_lily(pitch, out);
  write_octave(out, pitch.get_octave());
  if (note.has_courtesy_accidental())
    out << '!';
  auto duration = make_lily(note.get_duration(), out);
  if (note.is_tied())
    out << "~";
  for (const auto &articulation : note.get_articulations())
    make_lily(articulation, out);
  for (const auto &span : note.get_end_spans())
    write_span_end(out, span);

  for (const auto &span : note.get_start_spans())
    write_span_start(out, span);
  if (auto dynamic = note.get_dynamic(); dynamic)
    write_dynamic(out, dynamic.value());
  if (grace != notation::Grace::None)
    out << " }";
  out << ' ';
}

void write_rest(std::ofstream &out, const notation::Rest &rest) {
  auto grace = rest.get_duration().get_grace();
  if (grace != notation::Grace::None)
    return;
  out << 'r';
  auto duration = make_lily(rest.get_duration(), out);
  for (const auto &articulation : rest.get_articulations()) {
    // should only be a fermata on a rest
    make_lily(articulation, out);
  }
  out << ' ';
}

bool does_multibar_end(notation::Bar &bar, size_t barNo) {
  auto &part = bar.get_part();
  auto &movement = part.get_movement();
  if (movement.does_time_signature_change_at_bar(barNo))
    return true;
  if (movement.does_key_change_at_bar(barNo))
    return true;
  if (movement.get_bar_start_style(barNo) != notation::BarStyle::Single)
    return true;
  if (barNo == movement.get_total_bars())
    return true;
  if (movement.get_markup_at_bar(barNo))
    return true;
  if (movement.get_rehearsal_mark_at_bar(barNo))
    return true;
  if (movement.get_volta(barNo).get_value() != 0)
    return true;
  return false;
}

void write_bar_start(std::ofstream &out, const notation::BarStyle &style) {
  switch (style) {
  case notation::BarStyle::Single:
    return;
  case notation::BarStyle::Repeat:
    out << "\\bar \".|:\" ";
    break;
  case notation::BarStyle::Double:
    out << "\\bar \"||\" ";
    break;
  default:
    out << "| ";
    util::warning("Failed to write bar end:", style);
    break;
  }
}

void write_bar_end(std::ofstream &out, const notation::BarStyle &style) {
  switch (style) {
  case notation::BarStyle::Single:
    out << "| ";
    break;
  case notation::BarStyle::Repeat:
    out << "\\bar \":|.\" ";
    break;
  case notation::BarStyle::Double:
    out << "\\bar \"||\" ";
    break;
  case notation::BarStyle::Fine:
    out << "\\bar \"|.\" ";
    break;
  default:
    out << "| ";
    util::warning("Failed to write bar end:", style);
    break;
  }
}

void write_tuplet(std::ofstream &out, notation::Tuplet &tuplet) {
  if (tuplet.is_start()) {
    auto ratio = tuplet.get_ratio();
    out << "\\tuplet " << ratio.denominator() << "/" << ratio.numerator()
        << " { ";
  } else {
    out << " } ";
  }
}

void write_bar(std::ofstream &out, notation::Bar &bar, size_t barNo) {
  if (auto &movement = bar.get_part().get_movement();
      movement.does_tempo_change_at_bar(barNo)) {
    auto tempo = movement.get_bar_tempo(barNo);
    out << "\\tempo " << tempo.get_text() << " ";
  }
  write_bar_start(out, bar.get_bar_start_style());
  if (auto volta = bar.get_volta().get_value(); volta != 0) {
    out << "\\set Score.repeatCommands = #'((volta \"" << volta << "\")) ";
  }
  for (auto &notation : bar) {
    switch (notation.get_notation_type()) {
    case notation::NotationType::Note: {
      auto &note = dynamic_cast<notation::Note &>(notation);
      write_note(out, note);
    } break;
    case notation::NotationType::Rest: {
      auto &rest = dynamic_cast<notation::Rest &>(notation);
      write_rest(out, rest);
    } break;
    case notation::NotationType::Tuplet: {
      auto &tuplet = dynamic_cast<notation::Tuplet &>(notation);
      write_tuplet(out, tuplet);
    } break;
    case notation::NotationType::Clef: {
      auto &clef = dynamic_cast<notation::Clef &>(notation);
      write_clef(out, clef);
      out << ' ';
    } break;
    default:
      util::warning(
          "Failed to write lilypond notation:", notation.get_notation_type());
    }
  }
  if (auto volta = bar.get_volta().get_value(); volta != 0) {
    out << "\\set Score.repeatCommands = #'((volta #f)) ";
  }
  write_bar_end(out, bar.get_bar_end_style());
  out << "% " << (barNo + 1) << "\n";
}

void write_lily_pair(std::ofstream &out, const std::string &key,
    const std::string &value, const std::string indent = "  ") {
  out << indent << key << " = "
      << (value.empty() ? "##f" : util::quote_if_necessary(value)) << "\n";
}

void write_score_header(
    std::ofstream &out, const notation::Movement &movement) {
  out << "\\header {\n";
  write_lily_pair(out, "piece", movement.get(notation::MovementKey::title));
  write_lily_pair(out, "opus", movement.get(notation::MovementKey::opus));
  out << "} % header\n";
}

void write_book_header(std::ofstream &out, const notation::Book &book,
    const std::string &instrumentName) {
  out << "\\book {\n";
  out << "\\paper {\n  print-all-headers = ##f\n} %paper\n";
  out << "\\header {\n";
  for (const auto key : enums::values<notation::BookKey>())
    write_lily_pair(out, enums::to_string(key), book.get(key));
  write_lily_pair(out, "instrument", instrumentName);
  out << "} % header\n";
}

/*
 \new Staff \with {
  instrumentName = #"Violin "
  shortInstrumentName = #"Vln. "
}
*/
void write_instrument(std::ofstream &out, notation::Instrument &instrument) {
  out << "\\new Staff \\with {\n";
  // TODO if compress full bar rests
  if (auto name = instrument.get_name(); !name.empty()) {
    out << "  instrumentName = #\"" << name << "\"\n";
  }
  if (auto shortname = instrument.get_short_name(); !shortname.empty()) {
    out << "  shortInstrumentName = #\"" << shortname << "\"\n";
  }
  out << "\n}\n";
}

void write_staff(std::ofstream &out, notation::Part &part) {
  auto &instrument = part.get_instrument();
  write_instrument(out, instrument);

  out << "{\n  ";
  out << "\\compressFullBarRests\n  ";
  write_clef(out, part.get_clef_at_bar(0));
  out << "\n  ";
  write_key(out, part.get_key_at_bar(0));
  out << "\n  ";
  write_time_signature(out, part.get_movement().get_bar_time_signature(0));
  out << "\n\n";

  auto barCount = size_t{};
  auto multibarRun = size_t{};
  auto partial{false};
  auto &movement = part.get_movement();
  auto totalBars = movement.get_total_bars();

  for (auto &bar : part) {
    // calculate anacrusis if necessary
    if (barCount == 0 &&
        bar.get_bar_status() == notation::BarStatus::TooEmpty) {
      auto length = bar.get_notation_length();
      out << "\\partial " << length.denominator() << '*' << length.numerator()
          << ' ';
      partial = true;
    } else {
      partial = false;
    }

    if (movement.does_time_signature_change_at_bar(barCount)) {
      auto &timesignature = movement.get_bar_time_signature(barCount);
      write_time_signature(out, timesignature);
    }

    if (movement.does_key_change_at_bar(barCount)) {
      auto &keysignature = movement.get_bar_key(barCount);
      write_key(out, keysignature);
    }

    if (!partial && bar.is_rest_bar() && !does_multibar_end(bar, barCount) &&
        (barCount + 1) < totalBars) {
      ++multibarRun;
      ++barCount;
      continue;
    }

    if (multibarRun) {
      // timesig must be the same at the start and the end; otherwise we'd stop
      // a multibar run
      auto &timesig = movement.get_bar_time_signature(barCount);
      out << "  R1*" << multibarRun << '*' << (timesig.get_beats()) << '/'
          << (timesig.get_beat_lengths()) << " | % bars "
          << (barCount - multibarRun + 1) << " to " << (barCount) << '\n';
      if (barCount + 1 == totalBars) { // then we have finished
        out << " \\bar \"|.\" ";       // TODO is this the right symbol?
      }
      multibarRun = 0;
    }

    // we can ignore which rehearsal mark this is, because lilypond keeps count
    // of that
    if (movement.get_rehearsal_mark_at_bar(barCount))
      out << "\\mark \\default ";

    if (partial || !bar.is_rest_bar()) {
      out << "  ";
      write_bar(out, bar, barCount);
    } else {
      ++multibarRun;
    }

    // computers count from zero, but musicians count from 1, so there is an
    // offset here.  TODO we're counting straight through volta bars, but they
    // should reset, need to restart from volta 1 whenever the volta changes
    ++barCount;
    if ((barCount + 1) % 8 == 0) // lilypond groups output in 8s
      out << "\n% bar " << (barCount + 1) << "\n";
  }
  out << "} % staff\n";
}

void write_movement(std::ofstream &out, const notation::Movement &movement,
    const std::string &instrumentName) {
  out << "\n\\score {\n";
  write_score_header(out, movement);
  auto isTacet{true};
  for (auto &part : movement) {
    if (part.get_part_name() != instrumentName)
      continue;
    write_staff(out, part);
    isTacet = false;
  }
  if (isTacet) {
    auto numberOfBars = movement.get_total_bars();
    out << "\\new Staff { % tacet movement\n  \\compressFullBarRests\n  \\omit "
           "Staff.Clef\n  "
           "\\omit Staff.TimeSignature\n  "
           "\\bar \"|\" R1*"
        << numberOfBars << " \\bar \"|.\"\n}\n";
  }
  out << "} % score \n";
}

void write_lilypond_part_for_instrument(const std::string &filename,
    const std::string &instrumentName, const notation::Book &book) {
  auto out = std::ofstream{filename.c_str(), std::ios::out};
  write_version(out);
  write_book_header(out, book, instrumentName);
  for (auto &movement : book) {
    write_movement(out, movement, instrumentName);
  }
  out << "} % book \n";
}

} // namespace

void Format::write_part(const notation::Part &part) {
  auto instrumentName = part.get_part_name();
  auto &book = part.get_movement().get_book();
  write_lilypond_part_for_instrument(mPath, instrumentName, book);
}

// TODO copy-and-paste code for the next two functions
void Format::write_movement(const notation::Movement &movement) {
  auto out = std::ofstream{mPath, std::ios::out};
  write_version(out);
  auto &book = movement.get_book();
  // 'French' score (remove empty staves)
  // could also be \\RemoveEmptyAllStaves in lily 2.19
  out << "\\layout {\n\\context {\n \\Staff\n  \\RemoveEmptyStaves\n }\n}\n";
  write_book_header(out, book, "Conductor's Score");
  out << "\n\\score {\n"; // TODO movement name
  write_score_header(out, movement);
  out << "<<\n";
  for (auto &part : movement) {
    write_staff(out, part);
  }
  out << ">>\n} % score \n";
  out << "} % book \n";
}

void Format::write_score(const notation::Book &book) {
  auto out = std::ofstream{mPath, std::ios::out};
  write_version(out);
  // 'French' score (remove empty staves)
  // could also be \\RemoveEmptyAllStaves in lily 2.19
  out << "\\layout {\n\\context {\n \\Staff\n  \\RemoveEmptyStaves\n }\n}\n";
  write_book_header(out, book, "Conductor's Score");
  for (auto &movement : book) {
    out << "\n\\score {\n"; // TODO movement name
    write_score_header(out, movement);
    out << "<<\n";
    for (auto &part : movement) {
      write_staff(out, part);
    }
    out << ">>\n} % score \n";
  }
  out << "} % book \n";
}

void Format::write_all_scores(const notation::Book &book) {
  auto instruments = std::set<std::string>{};
  for (auto &movement : book) {
    for (auto &part : movement) {
      auto instrumentName = part.get_part_name();
      instruments.insert(instrumentName);
    }
  }

  for (const auto &instrumentName : instruments) {
    util::info("Lilypond : ", instrumentName);
    auto filename = mPath / (instrumentName + ".ly");
    write_lilypond_part_for_instrument(filename, instrumentName, book);
  }
}

} // namespace lilypond
