#include "articulation.h"

#include "util/logging.h"

#include <iostream>

namespace lilypond {

Articulation::Articulation(
    const notation::Articulation &articulation, std::ostream &out)
    : LilyType{out} {
  switch (articulation) {
  case notation::Articulation::Accent:
    out << "->";
    break;
  case notation::Articulation::DownBow:
    out << "\\downbow";
    break;
  case notation::Articulation::Fermata:
    out << "\\fermata";
    break;
  case notation::Articulation::LeftHandPizzicato:
    out << "-+";
    break;
  case notation::Articulation::Marcato:
    out << "-^";
    break;
  case notation::Articulation::SnapPizzicato:
    out << "\\snappizzicato";
    break;
  case notation::Articulation::Staccatissimo:
    out << "-!";
    break;
  case notation::Articulation::Staccato:
    out << "-.";
    break;
  case notation::Articulation::Tenuto:
    out << "--";
    break;
  case notation::Articulation::Turn:
    out << "\\turn";
    break;
  case notation::Articulation::UpBow:
    out << "\\upbow";
    break;
  case notation::Articulation::UpperMordent:
    out << "\\mordent";
    break;
  default:
    util::warning("Didn't write articulation:", articulation);
    break;
  }
}

Articulation::~Articulation() = default;

template <>
std::unique_ptr<LilyType> make_lily(
    const notation::Articulation &articulation, std::ostream &out) {
  return std::make_unique<Articulation>(articulation, out);
}

} // namespace lilypond
