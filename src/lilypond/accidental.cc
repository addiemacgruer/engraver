#include "accidental.h"

#include <iostream>

namespace lilypond {
Accidental::Accidental(
    const notation::Accidental &accidental, std::ostream &out)
    : LilyType(out) {
  switch (accidental) {
  case notation::Accidental::Sharp:
    out << "is";
    break;
  case notation::Accidental::Flat:
    out << "es";
    break;
  case notation::Accidental::Natural:
    break;
  case notation::Accidental::DoubleSharp:
    out << "isis";
    break;
  case notation::Accidental::FlatFlat:
    out << "eses";
  }
}

Accidental::~Accidental() = default;

template <>
std::unique_ptr<LilyType> make_lily(
    const notation::Accidental &accidental, std::ostream &out) {
  return std::make_unique<Accidental>(accidental, out);
}
} // namespace lilypond
