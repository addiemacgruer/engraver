#pragma once
#include "lilytype.h"

#include "notation/articulations.h"

namespace lilypond {
class Articulation : public LilyType {

public:
  Articulation(const notation::Articulation &articulation, std::ostream &out);
  ~Articulation() override;
};

template <>
std::unique_ptr<LilyType> make_lily(
    const notation::Articulation &, std::ostream &);

} // namespace lilypond
