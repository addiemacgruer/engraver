#pragma once

#include "lilytype.h"

#include "notation/pitch.h"

namespace lilypond {

class Pitch : public LilyType {
public:
  Pitch(const notation::Pitch &, std::ostream &);
  ~Pitch() override;
};

template <>
std::unique_ptr<LilyType> make_lily(const notation::Pitch &, std::ostream &);

} // namespace lilypond
