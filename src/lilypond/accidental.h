#pragma once
#include "lilytype.h"

#include "notation/accidental.h"

namespace lilypond {
class Accidental : public LilyType {

public:
  Accidental(const notation::Accidental &accidental, std::ostream &out);
  ~Accidental() override;
};

template <>
std::unique_ptr<LilyType> make_lily(
    const notation::Accidental &, std::ostream &);

} // namespace lilypond
