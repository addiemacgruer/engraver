#include "pitch.h"

#include "accidental.h"  //  for make_lily(Accidental)
#include "util/string.h" //  for lower_case_string

namespace lilypond {

Pitch::Pitch(const notation::Pitch &pitch, std::ostream &out) : LilyType{out} {
  auto sound = pitch.get_sound();
  auto uppercase = enums::to_string(sound);
  out << util::lower_case_string(uppercase);
  make_lily(pitch.get_accidental(), out);
}

Pitch::~Pitch() = default;

template <>
std::unique_ptr<LilyType> make_lily(
    const notation::Pitch &pitch, std::ostream &out) {
  return std::make_unique<Pitch>(pitch, out);
}

} // namespace lilypond
