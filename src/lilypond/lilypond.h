#pragma once

#include "lilypond/papersize.h"
#include <experimental/filesystem>
#include <memory>
#include <string>

namespace notation {
class Book;
class Movement;
class Part;
} // namespace notation

/** \namespace lilypond \brief Functions for working with Lilypond files
 * <a href="http://">Lilypond</a> */
namespace lilypond {

struct Format {
public:
  std::experimental::filesystem::path mPath;
  PaperSize mPS{PaperSize::a4};

  void write_part(const notation::Part &);
  void write_movement(const notation::Movement &);
  void write_score(const notation::Book &);
  void write_all_scores(const notation::Book &);
};

} // namespace lilypond
