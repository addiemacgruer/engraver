#pragma once

#include <string>

namespace engraver {

struct ExecResult {
  int result;
  std::string output;
};

ExecResult exec(const std::string &command);

} // namespace engraver
