#pragma once

#include <iostream>  // for std::error
#include <sstream>   //  for std::stringstream
#include <stdexcept> //  for std::runtime_error
#include <string>    //  for std::string

class EngraverError : public std::runtime_error {
public:
  template <class... T>
  explicit EngraverError(T... t) : std::runtime_error{"error"} {
    generate_message(t...);
    mString = mMessage.str();
    mMessage.clear();
  }

  ~EngraverError() override;

  const char *what() const noexcept override {
    return mString.c_str();
  }

private:
  template <class T>
  void generate_message(T t) {
    mMessage << t;
  }

  template <class T, class... U>
  void generate_message(T t, U... u) {
    mMessage << t;
    generate_message(u...);
  }

  std::stringstream mMessage{};
  std::string mString{};
};
