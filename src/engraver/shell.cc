#include "shell.h"

#include <array>

constexpr size_t buffersize = 128;

namespace engraver {

ExecResult exec(const std::string &cmd) {
  auto buffer = std::array<char, buffersize>{};
  auto output = std::string{};
  auto pipe = popen(cmd.c_str(), "r");

  if (!pipe)
    throw std::runtime_error("popen() failed!");

  while (!feof(pipe)) {
    if (fgets(buffer.data(), buffersize, pipe) != nullptr)
      output += buffer.data();
  }

  auto result = pclose(pipe);
  return {result, output};
}

} // namespace engraver
