#include "global.h"

#include "interface/control.h"
#include "util/logging.h"
#include "util/string.h"
#include "xml/node.h"

#include <algorithm>
#include <mutex>
#include <thread>

static std::mutex sGlobalMutex{};
static std::mutex sAdditionMutex{};

Global &Global::get_instance() {
  static Global instance{};
  return instance;
}

Global::Global() {
  //  util::log_alloc(util::Alloc::Construct, "", "Global");
}

Global::~Global() {
  //  util::log_alloc(util::Alloc::Destruct, "", "Global");
}

void Global::parse_graphic_node(const xml::Node *node) {
  auto string = node->get_value<std::string>();
  auto parsedGraphic = std::make_unique<render::SvgPath>(string);
  auto name = node->get_attribute<std::string>("name");

  {
    std::lock_guard<std::mutex> lock{sAdditionMutex};
    mSvgPaths[name] = std::move(parsedGraphic);
  }

  util::debug("Parsed graphic:", name);
}

void Global::parse_graphic_root_node(const xml::Node &node) {
  std::vector<std::thread> threads{};
  node.many("graphic", [&](const xml::Node &n) {
    auto thread = std::thread(&Global::parse_graphic_node, this, &n);
    threads.push_back(std::move(thread));
  });

  for (auto &thread : threads) {
    thread.join();
  }
}

void Global::parse_svg_paths() {
  auto graphicsfile = get_root() /= std::string("graphics.xml");
  auto doc = xml::Node::from_document(graphicsfile);

  parse_graphic_root_node(*doc);

  util::info("Parsed ", mSvgPaths.size(), " shapes");
}

void Global::init_with_root_directory(const char *arg0) {
  util::debug("Setting globals...");
  std::lock_guard<std::mutex> guard{sGlobalMutex};
  if (mRootInitialised)
    throw std::runtime_error{"Attempted to redeclare root directory!"};
  auto runcommand = std::experimental::filesystem::path(std::string{arg0});
  mRoot = std::make_unique<std::experimental::filesystem::path>(
      runcommand.parent_path());
  parse_svg_paths();
  mRootInitialised = true;
}

std::experimental::filesystem::path Global::get_root() const {
  return std::experimental::filesystem::path(*mRoot.get());
}

render::SvgPath &Global::svg_path_for_name(const std::string &name) const {
  auto &rval = mSvgPaths.at(name);
  return *rval;
}

void Global::add_control(std::unique_ptr<interface::Control> &&control) {
  util::debug("Added an extra window controller");
  mControls.push_back(std::move(control));
}

const std::vector<std::unique_ptr<interface::Control>> &
Global::get_controls() const {
  return mControls;
}

void Global::remove_control(const interface::Control *control) {
  util::debug("Removing a window controller...");
  auto matchTheControl = [control](
                             const auto &c) { return c.get() == control; };
  auto remove =
      std::remove_if(mControls.begin(), mControls.end(), matchTheControl);
  if (remove != mControls.end()) {
    mControls.erase(remove);
  } else {
    util::warning("Failed to remove window controller!");
  }
}
