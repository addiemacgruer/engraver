#include "part.h"

#include "bar.h"
#include "clef.h"
#include "key.h"
#include "markup.h"
#include "movement.h"
#include "util/assert.h"
#include "util/logging.h"
#include "xml/node.h"

#include <algorithm>
#include <iostream>

namespace notation {

Part::~Part() noexcept {}

Part::Part(Movement &movement)
    : mpMovement{&movement}, mClefChanges{}, mMarkups{} {
  add_clef_at_bar(0, Clef::Treble());
}

void Part::add_clef_at_bar(size_t bar, std::unique_ptr<Clef> &&clef) {
  if (bar > 1e9) {
    util::info("Disregarding suspiciously high clef change bar:", bar);
    return;
  }
  mClefChanges.add_data(bar, std::move(clef));
}

Clef &Part::get_clef_at_bar(size_t bar) const try {
  return *mClefChanges.get_data_after_position(bar);
} catch (std::exception &) {
  util::error("Failed to get a clef at bar position ", bar);
  throw std::runtime_error{"notation/part::get_clef_at_bar"};
}

Bar &Part::add_bar_at_end() {
  auto rval = std::make_unique<Bar>(*this);
  {
    auto _ = mBarsMutex.lock();
    mBars.push_back(std::move(rval));
    auto &lastbar = mBars.back();
    return *lastbar.get();
  }
}

Bar &Part::add_bar_before(size_t bar) {
  {
    auto _ = mBarsMutex.lock();
    if (bar > (mBars.size() + 1)) {
      util::error(
          "Tried to insert before bar ", bar, " but there are ", mBars.size());
      throw std::runtime_error{"notation/part::addBarBefore"};
    }
  }
  auto rval = std::make_unique<Bar>(*this);
  {
    auto _ = mBarsMutex.lock();
    auto newposition = mBars.begin() + bar - 1;
    mBars.insert(newposition, std::move(rval));
    mClefChanges.increase_after(bar);
    auto &newbar = mBars.at(bar - 1);
    return *newbar.get();
  }
}

void Part::delete_bar(size_t bar) {
  if (bar >= mBars.size()) {
    util::error("Tried to delete bar ", bar, " but there are ", mBars.size());
    return;
  }
  if (mBars.size() == 1) {
    util::error("Tried to delete last bar");
    return;
  }
  mBars.erase(mBars.begin() + bar);
  mClefChanges.decrease_after(bar);
}

Bar &Part::get_bar(size_t bar) const {
  auto &rval = mBars.at(bar);
  return *rval.get();
}

size_t Part::get_bar_number(const Bar &bar) const {
  auto _ = mBarsMutex.lock();
  for (size_t offset = 0; offset < mBars.size(); offset++) {
    if (mBars[offset].get() == &bar)
      return offset;
  }

  util::error("Looked for a bar in a part which did not contain it.");
  return 0;
}

Movement &Part::get_movement() const {
  return *mpMovement;
}

size_t Part::get_bar_count() const {
  auto _ = mBarsMutex.lock();
  return mBars.size();
}

Instrument &Part::get_instrument() {
  return mInstrument;
}

Part &Part::set_instrument(const Instrument &instrument) {
  mInstrument = instrument;
  return *this;
}

size_t Part::get_section_number() const {
  return mSectionNumber;
}

Part &Part::set_section_number(int section) {
  mSectionNumber = section;
  return *this;
}

std::ostream &operator<<(std::ostream &out, Part &part) {
  out << part.get_instrument().get_name();
  if (part.get_section_number() != 0)
    out << ' ' << part.get_section_number();
  return out;
}

const Key Part::get_key_at_bar(size_t bar) const {
  auto &cClefKeyAtBar = get_movement().get_bar_key(bar);
  auto transposition = mInstrument.get_transposition_in_semitones();
  return cClefKeyAtBar.get_transposed_key(-transposition);
}

Part &Part::add_key_at_bar(size_t bar, std::unique_ptr<Key> &&key) {
  auto transposition = mInstrument.get_transposition_in_semitones();
  auto transposedKey = key->get_transposed_key(transposition);
  auto cClefKeyAtBar = std::make_unique<Key>(transposedKey);
  get_movement().add_bar_key_change(bar, std::move(cClefKeyAtBar));
  return *this;
}

bool Part::does_key_change_at_bar(size_t bar) const {
  return get_movement().does_key_change_at_bar(bar);
}

bool Part::does_clef_change_at_bar(size_t bar) const {
  return mClefChanges.has_data_at_position(bar);
}

const std::string Part::get_part_name() const {
  if (!mPartName.empty())
    return mPartName;
  auto instrumentname = mInstrument.get_name();
  if (mSectionNumber == 0) {
    return instrumentname;
  }
  std::stringstream rval{};
  rval << instrumentname << ' ' << mSectionNumber;
  return rval.str();
}

void Part::set_part_name(const std::string &name) {
  mPartName = name;
}

Markup &Part::get_markup_at_bar(size_t bar) const {
  return *mMarkups.get_data_after_position(bar);
}

bool Part::does_markup_change_at_bar(size_t bar) const {
  return mMarkups.has_data_at_position(bar);
}

void Part::add_bar_markup(size_t bar, std::unique_ptr<Markup> &&markup) {
  mMarkups.add_data(bar, std::move(markup));
}

void Part::add_node_for_class(xml::Node &node) {
  auto rval = node.append_child(static_xml_name());

  mInstrument.add_node_for_class(*rval);
  if (!mPartName.empty())
    rval->add_node("partname", mPartName);

  if (mSectionNumber != 0)
    rval->add_node("section", mSectionNumber);

  for (const auto &[bar, change] : mClefChanges) {
    auto ccnode = rval->append_child("clef-changes");
    ccnode->add_node("bar", bar);
    change->add_node_for_class(*ccnode);
  }

  for (const auto &[bar, markup] : mMarkups) {
    auto markupnode = rval->append_child("markups");
    markupnode->add_node("bar", bar);
    markup->add_node_for_class(*markupnode);
  }

  for (const auto &bar : mBars)
    bar->add_node_for_class(*rval);
};

namespace {
void get_clef_changes(Part &rval, const xml::Node &node) {
  auto bar = size_t{};
  auto clef = Clef::Treble();
  node.once("bar", [&](const xml::Node &n) { bar = n.get_value<size_t>(); });
  node.once(Clef::static_xml_name(),
      [&](const xml::Node &n) { clef = Clef::get_class_for_node(n); });
  rval.add_clef_at_bar(bar, std::move(clef));
};

void get_markup_for_node(Part &part, const xml::Node &node) {
  auto bar = size_t{};
  auto markup = std::make_unique<Markup>();
  node.once("bar", [&](const xml::Node &n) { bar = n.get_value<int>(); });
  node.once(Markup::static_xml_name(),
      [&](const xml::Node &n) { markup = Markup::get_class_for_node(n); });
  part.add_bar_markup(bar, std::move(markup));
};
} // namespace

Part *Part::get_class_for_node(Movement *movement, const xml::Node &node) {
  auto &rval = movement->append_part();
  node.once(Instrument::static_xml_name(), [&](const xml::Node &n) {
    auto instrument = Instrument::get_class_for_node(n);
    rval.set_instrument(instrument);
  });
  node.once("partname", [&](const xml::Node &n) {
    auto partname = n.get_value<std::string>();
    rval.set_part_name(partname);
  });
  node.many(
      "markups", [&](const xml::Node &n) { get_markup_for_node(rval, n); });
  node.once("section",
      [&](const xml::Node &n) { rval.set_section_number(n.get_value<int>()); });
  node.many(Bar::static_xml_name(),
      [&](const xml::Node &n) { Bar::get_class_for_node(&rval, n); });
  node.many(
      "clef-changes", [&](const xml::Node &n) { get_clef_changes(rval, n); });
  return &rval; // TODO change signature
};

std::string Part::get_xml_name() {
  return static_xml_name();
}

size_t Part::size() const {
  return mBars.size();
}

Bar &Part::at(size_t s) const {
  assert::is_true(s <= mBars.size());
  auto &rval = mBars.at(s);
  return *rval;
};
} // namespace notation
