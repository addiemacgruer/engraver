#include "instrument.h"

#include "clef.h"
#include "xml/node.h"

namespace notation {

Instrument::Instrument()
    : mName{"Unknown"},
      mFamily{InstrumentType::Piano},
      mLowest{Pitch(0)},
      mHighest{Pitch(127)},
      mTranspositionInSemitones{0} {}

Instrument::Instrument(const std::string &name, const InstrumentType &family,
    const Pitch &lowest, const Pitch &highest, const int transpositionSemitones)
    : mName{name},
      mFamily{family},
      mLowest{lowest},
      mHighest{highest},
      mTranspositionInSemitones{transpositionSemitones} {}

const std::vector<Clef> Instrument::get_clef_list() const {
  return mClefList;
}

const std::string Instrument::get_name() const {
  return mName;
}

void Instrument::set_name(const std::string &name) {
  mName = name;
}

const std::string Instrument::get_short_name() const {
  return mShortName;
}

void Instrument::set_short_name(const std::string &shortName) {
  mShortName = shortName;
}

InstrumentType Instrument::get_instrument_family() const {
  return mFamily;
}

const Pitch &Instrument::get_lowest_pitch() const {
  return mLowest;
}

const Pitch &Instrument::get_highest_pitch() const {
  return mHighest;
}

Instrument &Instrument::add_clef(const Clef &clef) {
  mClefList.push_back(clef);
  return *this;
}

int Instrument::get_transposition_in_semitones() const {
  return mTranspositionInSemitones;
}

void Instrument::add_node_for_class(xml::Node &pnode) {
  auto node = pnode.append_child(static_xml_name());
  node->add_node("name", mName);
  if (!mShortName.empty())
    node->add_node("shortname", mShortName);
  node->add_node("family", mFamily);
  {
    auto lowest = node->append_child("lowest");
    mLowest.add_node_for_class(*lowest);
  }
  {
    auto highest = node->append_child("highest");
    mHighest.add_node_for_class(*highest);
  }
  if (!mClefList.empty()) {
    auto cleflist = node->append_child("clef-list");
    for (auto &clef : mClefList) {
      clef.add_node_for_class(*cleflist);
    }
  }
  if (mTranspositionInSemitones != 0)
    node->add_node("transposition", mTranspositionInSemitones);
};

Instrument Instrument::get_class_for_node(const xml::Node &node) {
  auto name =
      xml::get_or_default<std::string>(node, "name", "Unknown instrument");
  auto shortname = xml::get_or_default<std::string>(node, "shortname");
  auto family{InstrumentType::Piano};
  auto lowest = std::make_unique<Pitch>(0);
  auto highest = std::make_unique<Pitch>(127);
  auto clefList = std::vector<std::unique_ptr<Clef>>{};
  auto transpositionInSemitones{0};
  node.once("family",
      [&](const xml::Node &n) { family = n.get_value<InstrumentType>(); });
  node.once("lowest", [&](const xml::Node &n) {
    n.once("pitch",
        [&](const xml::Node &sn) { lowest = Pitch::get_class_for_node(sn); });
  });
  node.once("highest", [&](const xml::Node &n) {
    n.once("pitch",
        [&](const xml::Node &sn) { highest = Pitch::get_class_for_node(sn); });
  });
  node.many("clef-list", [&](const xml::Node &n) {
    auto clef = Clef::get_class_for_node(n);
    clefList.push_back(std::move(clef));
  });
  node.once("transposition", [&](const xml::Node &n) {
    transpositionInSemitones = n.get_value<int>();
  });
  auto rval = Instrument{
      name, family, *lowest.get(), *highest.get(), transpositionInSemitones};
  for (auto &clef : clefList)
    rval.add_clef(*clef.get());
  rval.set_short_name(shortname);
  return rval;
};

std::string Instrument::get_xml_name() {
  return static_xml_name();
};

} // namespace notation
