#pragma once
#include "notation/accidental.h" // for Accidental
#include "notation/cleftype.h"   // for ClefType
#include "notation/sound.h"      // for Sound
#include "xml/serialise.h"

#include <memory>
#include <string>

namespace notation {

class Pitch : public xml::Serialise<Pitch> {
public:
  // creators
  Pitch(const Sound &sound = Sound::C,
      const Accidental &accidental = Accidental::Natural, const int octave = 4);
  /** generate a pitch using midi pitch number.  semitones; middle C = 60,
   * middle A = 69. */
  explicit Pitch(const int midiPitchNumber);

  // getters
  Accidental get_accidental() const;
  double get_fundamental_frequency_in_hz() const;
  int get_ledger_number() const;
  int get_midi_pitch_number() const;
  int get_octave() const;
  const Pitch get_one_sound_down() const;
  const Pitch get_one_sound_up() const;
  Sound get_sound() const;
  const Pitch operator+(int offset) const;
  const Pitch operator-(int offset) const;
  bool operator<(const Pitch &rhs) const;
  bool operator==(const Pitch &rhs) const;
  bool operator>(const Pitch &rhs) const;

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Pitch> get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "pitch";
  }

private:
  Sound mSound;
  Accidental mAccidental;
  int mOctave;
};

} // namespace notation
