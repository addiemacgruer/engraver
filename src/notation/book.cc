#include "book.h"

#include "movement.h"
#include "util/assert.h"
#include "util/logging.h"
#include "xml/node.h"

namespace notation {

Book::Book() {
  //  util::log_alloc(util::Alloc::Construct, "notation", "Book");
  mKeys[BookKey::title] = "Untitled work";
}

Book::~Book() noexcept {
  //  util::log_alloc(util::Alloc::Destruct, "notation", "Book");
}

std::string Book::get(const BookKey &key) const try {
  return mKeys.at(key);
} catch (std::exception &) {
  return {};
}

void Book::set(const BookKey &key, const std::string &value) {
  mKeys[key] = value;
}

Movement &Book::add_movement() {
  util::debug("Adding movement");
  util::debug("  adding to movements (previous size ", mMovements.size());
  auto newmovement = std::make_unique<Movement>(this);
  mMovements.push_back(std::move(newmovement));
  return get_last_movement();
}

Movement &Book::get_last_movement() const {
  if (mMovements.empty()) {
    util::critical("Tried to getLastMovement, but there are none");
    throw std::runtime_error{"notation/book::get_last_movement"};
  }
  auto &last = mMovements.back();
  return *last.get();
}

void Book::add_node_for_class(xml::Node &parent) {
  auto rval = parent.append_child("book");
  for (const auto &[k, v] : mKeys) {
    if (v.empty())
      continue;
    auto keyname = enums::to_string(k);
    rval->append_child(keyname)->set_value(v);
  }
  for (const auto &movement : mMovements)
    movement->add_node_for_class(*rval);
};

std::unique_ptr<Book> Book::class_for_node(xml::Node &node) {
  auto rval = std::make_unique<Book>();
  for (const auto key : enums::values<BookKey>()) {
    auto keyname = enums::to_string(key);
    auto value = xml::get_or_default<std::string>(node, keyname);
    rval->set(key, value);
  }
  node.many(Movement::static_xml_name(),
      [&](const xml::Node &n) { Movement::get_class_for_node(rval.get(), n); });
  return rval;
}

std::string Book::get_xml_name() {
  return static_xml_name();
}

size_t Book::size() const {
  return mMovements.size();
}

Movement &Book::at(size_t s) const {
  auto &rval = mMovements.at(s);
  return *rval.get();
};

} // namespace notation
