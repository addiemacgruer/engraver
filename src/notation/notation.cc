#include "notation.h"

namespace notation {

Notation::~Notation() noexcept = default;

Notation &Notation::as_notation() {
  return *this;
}

Notation::Notation() = default;
Notation::Notation(const Notation &) = default;
Notation::Notation(Notation &&) noexcept = default;
Notation &Notation::operator=(const Notation &) = default;
Notation &Notation::operator=(Notation &&) noexcept = default;

} // namespace notation
