#include "rehearsalmark.h"

namespace notation {

RehearsalMark::RehearsalMark(size_t number) : mNumber{number} {}

size_t RehearsalMark::get_number() const {
  return mNumber;
}

std::string RehearsalMark::get_text() const {
  return "A"; // TODO calculate a real rehearsal mark
}

} // namespace notation
