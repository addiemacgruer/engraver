#pragma once

#include "notation.h"

#include <vector>

namespace notation {

enum class Accidental;
enum class Sound;
class Pitch;
enum class Mode;

class Key : public Notation {
public:
  // helpers
  static int get_accidentals_in_major_key(const std::string &key);
  static int get_accidentals_in_minor_key(const std::string &key);
  static const std::vector<Sound> get_flat_order_list();
  static const std::vector<Sound> get_sharp_order_list();

  // creators
  Key(const Sound &sound, const Accidental &accidental, const Mode &mode);
  explicit Key(const Key *key);
  explicit Key(const int accidentals);

  // getters
  int accidentals() const;
  const geom::Ratio get_notation_length() const override;
  NotationType get_notation_type() const override;
  const Pitch get_relative_major() const;
  const Pitch get_relative_minor() const;
  const Key get_transposed_key(int semitones) const;
  Pitch get_pitch_in_key(const Pitch &pitch) const;

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Key> get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "key";
  }

private:
  int mAccidentals;
};

} // namespace notation
