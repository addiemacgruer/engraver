#include "position.h"

#include "xml/node.h"

namespace notation {

Position::Position(size_t _bar, size_t _beat) : bar{_bar}, beat{_beat} {};

Position Position::get_position_from_node(const xml::Node &node) {
  size_t bar{0};
  size_t beat{0};
  node.once("bar", [&](const xml::Node &n) { bar = n.get_value<int>(); });
  node.once("beat", [&](const xml::Node &n) { beat = n.get_value<int>(); });
  return Position{bar, beat};
}

void Position::append_position_to_node(xml::Node &node) const {
  if (bar != 0)
    node.add_node<size_t>("bar", bar);
  if (beat != 0)
    node.add_node<size_t>("beat", beat);
}

bool operator<(const Position &first, const Position &second) {
  if (first.bar == second.bar)
    return first.beat < second.beat;
  return first.bar < second.bar;
}

bool operator<=(const Position &first, const Position &second) {
  if (first.bar == second.bar)
    return first.beat <= second.beat;
  return first.bar <= second.bar;
}

bool operator==(const Position &first, const Position &second) {
  return first.bar == second.bar && first.beat == second.beat;
}

bool operator>(const Position &first, const Position &second) {
  if (first.bar == second.bar)
    return first.beat > second.beat;
  return first.bar > second.bar;
}

bool operator>=(const Position &first, const Position &second) {
  if (first.bar == second.bar)
    return first.beat >= second.beat;
  return first.bar >= second.bar;
}

bool operator<(const Position &first, const size_t second) {
  return first.bar < second;
}

bool operator<=(const Position &first, const size_t second) {
  return first.bar <= second;
}

bool operator==(const Position &first, const size_t second) {
  return first.bar == second;
}

bool operator>(const Position &first, const size_t second) {
  return first.bar > second;
}

bool operator>=(const Position &first, const size_t second) {
  return first.bar >= second;
}

} // namespace notation
