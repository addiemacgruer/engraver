#include "bar.h"

#include "clef.h"
#include "movement.h"
#include "notation/barstyle.h" // for BarStyle
#include "note.h"
#include "part.h"
#include "rest.h"
#include "timesignature.h"
#include "tuplet.h"
#include "util/assert.h"
#include "util/hash.h"
#include "util/logging.h"
#include "volta.h"
#include "xml/node.h"

namespace notation {

size_t Bar::size() const {
  return mNotation.size();
}

Notation &Bar::at(size_t s) const {
  assert::is_true(s < mNotation.size());
  auto &rval = mNotation.at(s);
  return *rval;
}

Bar::Bar(Part &part) : mpPart{&part} {}

Bar::~Bar() {}

notation::NotationType Bar::get_notation_type() const {
  return NotationType::Bar;
}

size_t Bar::get_notation_count() const {
  return mNotation.size();
}

Volta Bar::get_volta() const {
  auto barnumber = get_bar_number();
  return get_part().get_movement().get_volta(barnumber);
}

bool Bar::is_rest_bar() const {
  if (mNotation.empty())
    return true;
  for (const auto &notation : mNotation) {
    if (notation->get_notation_type() != NotationType::Rest)
      return false;
    auto rest = dynamic_cast<const Rest *>(notation.get());
    if (!rest->get_articulations().empty())
      return false;
    // TODO text markup on a rest?
  }
  return true;
}

void Bar::check_special_notation(Notation *notation) {
  switch (notation->get_notation_type()) {
  case NotationType::Clef: {
    auto clefNotation = static_cast<Clef *>(notation);
    auto thisBarNumber = get_bar_number();
    auto clefCopy = std::make_unique<notation::Clef>(clefNotation);
    mpPart->add_clef_at_bar(thisBarNumber, std::move(clefCopy));
    break;
  }
  default:
    break;
  }
}

void Bar::append_notation(std::unique_ptr<Notation> &&notation) {
  // TODO  check_special_notation(notation);
  mNotation.push_back(std::move(notation));
  mStatusIsValid = false;
}

void Bar::add_notation_at_position(
    std ::unique_ptr<Notation> &&notation, const size_t position) {
  assert::is_not_null(notation.get());
  assert::is_true(position <= mNotation.size());
  // TODO  check_special_notation(notation);
  mNotation.insert(mNotation.begin() + position, std::move(notation));
  mStatusIsValid = false;
}

std::unique_ptr<Notation> Bar::delete_notation(const size_t position) {
  assert::is_true(position < mNotation.size());
  auto rval = std::move(mNotation.at(position));
  mNotation.erase(mNotation.begin() + position);
  mStatusIsValid = false;
  return rval;
}

int Bar::get_shortest_notation_duration() const {
  if (mNotation.empty())
    return 1;
  auto smallest = mNotation.at(0)->get_notation_length().denominator();
  for (auto &note : mNotation) {
    smallest = std::max(smallest, note->get_notation_length().denominator());
  }
  return smallest;
}

BarStatus Bar::get_bar_status() const {
  if (!mStatusIsValid)
    calculate_status();
  return mBarStatus;
}

const geom::Ratio Bar::get_notation_length() const {
  if (!mStatusIsValid)
    calculate_status();
  return mLength;
}

void Bar::calculate_status() const {
  mLength = {0, 1};
  if (mNotation.empty()) {
    mBarStatus = BarStatus::Empty;
    mStatusIsValid = true;
    return;
  }
  auto length = geom::Ratio{};
  auto tupletStack = TupletStack{};
  for (auto &notation : mNotation) {
    length += tupletStack.get_ratio() * notation->get_notation_length();
    if (notation->get_notation_type() == NotationType::Tuplet) {
      auto tuplet = dynamic_cast<Tuplet *>(notation.get());
      tupletStack.add_tuplet(tuplet);
    }
  }
  mLength = length;
  auto thisBarLength = get_part()
                           .get_movement()
                           .get_bar_time_signature(get_bar_number())
                           .get_bar_length();

  if (length == 0) { // for instance, just grace notes in bar
    mBarStatus = BarStatus::Empty;
  } else if (length == thisBarLength) {
    mBarStatus = BarStatus::OK;
  } else if (length < thisBarLength) {
    mBarStatus = BarStatus::TooEmpty;
  } else {
    mBarStatus = BarStatus::TooFull;
  }
  mStatusIsValid = true;
}

Part &Bar::get_part() const {
  return *mpPart;
}

size_t Bar::get_bar_number() const {
  return mpPart->get_bar_number(*this);
}

BarStyle Bar::get_bar_end_style() const {
  return get_part().get_movement().get_bar_end_style(get_bar_number());
}

Bar &Bar::set_bar_end_style(const BarStyle &barEnd) {
  get_part().get_movement().set_bar_end_style(get_bar_number(), barEnd);
  return *this;
}

BarStyle Bar::get_bar_start_style() const {
  return get_part().get_movement().get_bar_start_style(get_bar_number());
}

Bar &Bar::set_bar_start_style(const BarStyle &bs) {
  get_part().get_movement().set_bar_start_style(get_bar_number(), bs);
  return *this;
}

void Bar::add_node_for_class(xml::Node &node) {
  auto rval = node.append_child("bar");
  for (const auto &notation : mNotation) {
    notation->add_node_for_class(*rval);
  }
}

Bar *Bar::get_class_for_node(Part *part, const xml::Node &node) {
  auto &rval = part->add_bar_at_end();
  node.every([&](const xml::Node &n) {
    switch (n.get_hash()) {
    case util::hash("note"): {
      std::unique_ptr<notation::Notation> note = Note::get_class_for_node(n);
      rval.append_notation(std::move(note));
    } break;
    case util::hash("rest"): {
      auto rest = Rest::get_class_for_node(n);
      rval.append_notation(std::move(rest));
    } break;
    case util::hash("tuplet"): {
      auto tuplet = Tuplet::get_class_for_node(n);
      rval.append_notation(std::move(tuplet));
    } break;
    case util::hash("clef"): {
      auto clef = Clef::get_class_for_node(n);
      rval.append_notation(std::move(clef));
    } break;
    default:
      util::warning("Unknown tag for bar:", n.get_name());
    }
  });
  return &rval;
}

std::string Bar::get_xml_name() {
  return static_xml_name();
};

void Bar::add_postfix(const PostFix &postfix) {
  mPostfixes.push_back(postfix);
}

void Bar::apply_postfixes() {
  for (const auto &postfix : mPostfixes)
    postfix();
  mPostfixes.clear();
}

} // namespace notation
