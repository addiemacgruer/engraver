#pragma once

#include <cstddef> // for std::size_t

namespace notation {

class Volta {
public:
  explicit Volta(size_t value);
  size_t get_value() const;

private:
  size_t mValue;
};

} // namespace notation
