#pragma once

#include "duration.h"
#include "xml/serialise.h"

#include <memory>
#include <string>

namespace notation {

class Tempo : public xml::Serialise<Tempo> {
public:
  // creators
  explicit Tempo(const std::string &text);
  Tempo(const std::string &text, const Duration &beatLength, int beatSpeed);

  // getters
  const Duration &get_beat_length() const;
  int get_beat_speed() const;
  const std::string &get_text() const;

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Tempo> get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "tempo";
  }

private:
  std::string mText;
  Duration mBeatLength{};
  int mBeatSpeed{};
};

} // namespace notation
