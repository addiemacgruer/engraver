#pragma once

#include "geom/ratio.h"
#include "notation.h"

namespace notation {

class TimeSignature : public Notation {
public:
  // creators
  explicit TimeSignature(const int beats = 4, const int beatLengths = 4);
  explicit TimeSignature(const TimeSignature &);

  // getters
  const geom::Ratio get_bar_length() const;
  int get_beat_lengths() const;
  int get_beats() const;

  // notation
  const geom::Ratio get_notation_length() const override;
  NotationType get_notation_type() const override;

  // serialisation
  void add_node_for_class(xml::Node &node) override;
  static std::unique_ptr<TimeSignature> get_class_for_node(
      const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "timesignature";
  }

private:
  int mBeats;
  int mBeatLengths;
};

} // namespace notation
