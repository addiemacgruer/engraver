#include "timednotation.h"

#include "notation/articulations.h" // for Articulation
#include "util/logging.h"
#include "xml/node.h"

namespace notation {

TimedNotation::TimedNotation(const Duration duration) : mDuration{duration} {}

TimedNotation::~TimedNotation() = default;

const geom::Ratio TimedNotation::get_notation_length() const {
  return mDuration.get_length();
}

const Duration TimedNotation::get_duration() const {
  return mDuration;
}

void TimedNotation::set_duration(const Duration &duration) {
  mDuration = duration;
}

void TimedNotation::add_node_for_class(xml::Node &parent) {
  mDuration.add_node_for_class(parent);
  for (const auto &artic : mArticulationSet)
    parent.add_node("articulation", artic);
}

void TimedNotation::decorate_timed_notation(
    TimedNotation *tn, const xml::Node &node) {
  node.once("duration", [&](const xml::Node &n) {
    auto duration = Duration::get_class_for_node(n);
    tn->set_duration(*duration);
  });
  node.many("articulation", [&](const xml::Node &n) {
    auto articulation = n.get_value<Articulation>();
    tn->add_articulation(articulation);
  });
};

const std::set<Articulation> TimedNotation::get_articulations() const {
  return mArticulationSet;
}

bool TimedNotation::add_articulation(const Articulation &articulation) {
  mArticulationSet.insert(articulation);
  return true;
}

bool TimedNotation::remove_articulation(const Articulation &articulation) {
  if (auto found = mArticulationSet.find(articulation);
      found == mArticulationSet.cend()) {
    mArticulationSet.erase(found);
    return true;
  }
  return false;
};

bool TimedNotation::has_articulation(const Articulation &articulation) const {
  auto found = mArticulationSet.find(articulation);
  return (found != mArticulationSet.cend());
}

} // namespace notation
