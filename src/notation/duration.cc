#include "duration.h"

#include "geom/ratio.h"
#include "notation/notenames.h"
#include "util/logging.h"
#include "util/maths.h"
#include "xml/node.h"

#include <cmath>
#include <stdexcept>

namespace notation {

// Duration::~Duration() = default;

Duration::Duration(int baseDuration, int dots, Grace grace)
    : mBaseDuration{baseDuration}, mDots{dots}, mGrace{grace} {
  if (!util::is_power_of_two(baseDuration)) {
    util::log(util::LogLevel::Error,
        "Got a note baseDuration which is not a power of two:", baseDuration);
    throw std::invalid_argument{"notation/duration::Duration"};
  }
  if (dots < 0) {
    util::log(util::LogLevel::Error,
        "Tried to create a Duration with number of dots:", dots);
    throw std::invalid_argument{"notation/duration::Duration"};
  }
};

Duration::Duration(NoteName baseDuration, int dots, Grace grace)
    : Duration(static_cast<int>(baseDuration), dots, grace) {}

int Duration::get_base_duration() const {
  return mBaseDuration;
};

int Duration::get_dots() const {
  return mDots;
}

Grace Duration::get_grace() const {
  return mGrace;
}

const geom::Ratio Duration::get_length() const {
  if (mGrace != Grace::None) // grace notes have no length
    return {0, 1};

  if (mDots == 0) // no dots, simple duration
    return {1, mBaseDuration};

  // otherwise, every dot gets us one step closer to the next duration length
  return geom::Ratio{2, mBaseDuration} -
         geom::Ratio{1, mBaseDuration * util::raise_to_power_of_two(mDots)};
}

std::ostream &operator<<(std::ostream &out, const Duration &duration) {
  int base = duration.get_base_duration();
  switch (base) {
  case 1:
    out << "semibreve";
    break;
  case 2:
    out << "minim";
    break;
  case 4:
    out << "crotchet";
    break;
  default:
    while (base > 4) {
      out << "semi";
      base /= 2;
    }
    out << "quaver";
  }
  for (int i = 0; i < duration.get_dots(); i++)
    out << ".";
  return out;
}

void Duration::add_node_for_class(xml::Node &parent) {
  auto duration = parent.append_child("duration");
  duration->add_node("length", mBaseDuration);
  if (mDots != 0)
    duration->add_node("dots", mDots);
  if (mGrace != Grace::None)
    duration->add_node("grace", mGrace);
}

std::unique_ptr<Duration> Duration::get_class_for_node(const xml::Node &node) {
  auto duration{4};
  auto dots{0};
  auto grace{Grace::None};
  node.once(
      "length", [&](const xml::Node &n) { duration = n.get_value<int>(); });
  node.once("dots", [&](const xml::Node &n) { dots = n.get_value<int>(); });
  node.once("grace", [&](const xml::Node &n) { grace = n.get_value<Grace>(); });
  return std::make_unique<Duration>(duration, dots, grace);
}

std::string Duration::get_xml_name() {
  return static_xml_name();
}

} // namespace notation
