#include "rest.h"

#include "notation/notenames.h" // for NoteName
#include "timesignature.h"
#include "util/assert.h"
#include "util/logging.h"
#include "xml/node.h"

namespace notation {

Rest::Rest() : TimedNotation{Duration{4}}, isWholeBar{false} {};

Rest::Rest(const Duration duration)
    : TimedNotation{duration}, isWholeBar{false}, mWholeBar{} {};

Rest::Rest(const TimeSignature &ts)
    : TimedNotation{Duration{NoteName::Semibreve}},
      isWholeBar{true},
      mWholeBar{ts.get_bar_length()} {}

NotationType Rest::get_notation_type() const {
  return NotationType::Rest;
}

void Rest::add_node_for_class(xml::Node &node) {
  if (isWholeBar) {
    util::error("tried to serialise a whole-bar rest");
    return;
  }
  auto rest = node.append_child("rest");
  this->TimedNotation::add_node_for_class(*rest);
}

std::unique_ptr<Rest> Rest::get_class_for_node(const xml::Node &node) {
  auto rval = std::make_unique<Rest>();
  TimedNotation::decorate_timed_notation(rval.get(), node);
  return rval;
}

std::string Rest::get_xml_name() {
  return static_xml_name();
}

} // namespace notation
