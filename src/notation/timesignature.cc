#include "timesignature.h"

#include "util/assert.h"
#include "util/logging.h"
#include "xml/node.h"

#include <stdexcept>

namespace notation {
TimeSignature::TimeSignature(const int beats, const int beatLengths)
    : mBeats{beats}, mBeatLengths{beatLengths} {}

TimeSignature ::TimeSignature(const TimeSignature &ts)
    : mBeats{ts.mBeats}, mBeatLengths{ts.mBeatLengths} {}

int TimeSignature::get_beats() const {
  return mBeats;
}

int TimeSignature::get_beat_lengths() const {
  return mBeatLengths;
}

const geom::Ratio TimeSignature::get_bar_length() const {
  return {mBeats, mBeatLengths};
}

const geom::Ratio TimeSignature::get_notation_length() const {
  return {};
}

NotationType TimeSignature::get_notation_type() const {
  return NotationType::TimeSignature;
}

void TimeSignature::add_node_for_class(xml::Node &node) {
  auto rval = node.append_child("time-signature");
  rval->add_node("beats", mBeats);
  rval->add_node("lengths", mBeatLengths);
}

std::unique_ptr<TimeSignature> TimeSignature::get_class_for_node(
    const xml::Node &node) {
  auto beats{4};
  auto beatlengths{4};
  node.once("beats", [&](const xml::Node &n) { beats = n.get_value<int>(); });
  node.once(
      "lengths", [&](const xml::Node &n) { beatlengths = n.get_value<int>(); });
  return std::make_unique<TimeSignature>(beats, beatlengths);
};

std::string TimeSignature::get_xml_name() {
  return static_xml_name();
}

} // namespace notation
