#pragma once

#include <string>

namespace notation {

class RehearsalMark {
public:
  // creators
  explicit RehearsalMark(size_t number);

  // getters
  size_t get_number() const;
  std::string get_text() const;

private:
  size_t mNumber;
};

} // namespace notation
