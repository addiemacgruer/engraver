#pragma once

#include "notation.h"

#include <vector>

namespace notation {

class Tuplet : public Notation {
public:
  // creators
  /** start a run of tuplets */
  explicit Tuplet(const geom::Ratio &ratio);
  /** end a run of tuplets */
  Tuplet();

  // getters
  const geom::Ratio &get_ratio() const;
  bool is_start() const;

  // notation
  const geom::Ratio get_notation_length() const override;
  NotationType get_notation_type() const override;

  // serialisation
  void add_node_for_class(xml::Node &node) override;
  static std::unique_ptr<Tuplet> get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "tuplet";
  }

private:
  bool mIsStart;
  geom::Ratio mTupletRatio;
};

class TupletStack {
public:
  TupletStack();
  /** adds tuplet to stack and updates ratio.  If tuplet is starter, return
   * true.  If tuplet is finisher, return true if removed ok and false if
   * tuplets are unbalanced */
  bool add_tuplet(Tuplet *);
  geom::Ratio get_ratio() const;

private:
  std::vector<geom::Ratio> mStack{};
  geom::Ratio mRatio{1};
};

} // namespace notation
