#pragma once

#include <cstddef>

namespace xml {
class Node;
}

namespace notation {

struct Position { //: public xml::Serialise<Position> {
public:
  size_t bar;
  size_t beat{0};
  explicit Position(size_t _bar, size_t _beat = 0);

  static Position get_position_from_node(const xml::Node &node);
  void append_position_to_node(xml::Node &node) const;
};

bool operator<(const Position &first, const Position &second);
bool operator<=(const Position &first, const Position &second);
bool operator==(const Position &first, const Position &second);
bool operator>(const Position &first, const Position &second);
bool operator>=(const Position &first, const Position &second);

bool operator<(const Position &first, const size_t second);
bool operator<=(const Position &first, const size_t second);
bool operator==(const Position &first, const size_t second);
bool operator>(const Position &first, const size_t second);
bool operator>=(const Position &first, const size_t second);

} // namespace notation
