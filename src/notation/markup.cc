#include "markup.h"

#include "xml/node.h"

namespace notation {

Markup::Markup() = default;

Markup::Markup(const std::string &markup) : mText{markup} {}

// getters

const std::string &Markup::get_text() const {
  return mText;
}

bool Markup::is_bold() const {
  return mIsBold;
}

bool Markup::is_italic() const {
  return mIsItalic;
}

MarkupPosition Markup::get_position() const {
  return mPosition;
}

// setters
void Markup::set_text(const std::string &text) {
  mText = text;
}

void Markup::set_bold(bool bold) {
  mIsBold = bold;
}

void Markup::set_italic(bool italic) {
  mIsItalic = italic;
}

void Markup::set_position(MarkupPosition position) {
  mPosition = position;
}

void Markup::add_node_for_class(xml::Node &pnode) {
  auto node = pnode.append_child(static_xml_name());
  node->add_node("text", mText);
  if (mIsBold)
    node->add_node("bold", mIsBold);
  if (mIsItalic)
    node->add_node("italic", mIsItalic);
  if (mPosition != MarkupPosition::Either)
    node->add_node("position", mPosition);
};

std::unique_ptr<Markup> Markup::get_class_for_node(const xml::Node &node) {
  auto text = std::string{};
  auto isBold{false};
  auto isItalic{false};
  auto position{MarkupPosition::Either};
  node.once(
      "text", [&](const xml::Node &n) { text = n.get_value<std::string>(); });
  node.once("bold", [&](const xml::Node &n) { isBold = n.get_value<bool>(); });
  node.once(
      "italic", [&](const xml::Node &n) { isItalic = n.get_value<bool>(); });
  node.once("position",
      [&](const xml::Node &n) { position = n.get_value<MarkupPosition>(); });

  auto rval = std::make_unique<Markup>(text);
  rval->set_bold(isBold);
  rval->set_italic(isItalic);
  rval->set_position(position);
  return rval;
};

std::string Markup::get_xml_name() {
  return static_xml_name();
};

} // namespace notation
