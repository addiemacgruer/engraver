#pragma once

#include "notation/grace.h" // for Grace
#include "xml/serialise.h"

#include <iosfwd>
#include <memory>

namespace geom {
class Ratio;
}

namespace notation {
enum class NoteName : int;

class Duration : public xml::Serialise<Duration> {
public:
  // creator
  explicit Duration(
      int baseDuration = 4, int dots = 0, Grace grace = Grace::None);
  Duration(NoteName baseDuration, int dots = 0, Grace grace = Grace::None);
  // getter
  int get_base_duration() const;
  int get_dots() const;
  Grace get_grace() const;
  const geom::Ratio get_length() const;
  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Duration> get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "duration";
  }

private:
  int mBaseDuration;
  int mDots;
  Grace mGrace;
};

std::ostream &operator<<(std::ostream &out, const Duration &duration);
} // namespace notation
