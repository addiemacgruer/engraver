#include "movement.h"

#include "book.h"
#include "key.h"
#include "markup.h"
#include "notation/barstyle.h"    // for BarStyle
#include "notation/movementkey.h" // for MovementKey
#include "part.h"
#include "rehearsalmark.h"
#include "tempo.h"
#include "timesignature.h"
#include "util/assert.h"
#include "util/logging.h"
#include "volta.h"
#include "xml/node.h"

#include <cmath>
#include <stdexcept>

namespace notation {

Movement::~Movement() {
  //  util::log_alloc(util::Alloc::Destruct, "notation", "Movement");
}

Movement::Movement(Book *book)
    : mBook{book}, mTimeSignatures{std::make_unique<TimeSignature>(4, 4)} {
  //  util::log_alloc(util::Alloc::Construct, "notation", "Movement");
  add_bar_time_signature(0, std::make_unique<TimeSignature>(4, 4));
  add_bar_key_change(0, std::make_unique<Key>(0));
  for (auto key : enums::values<MovementKey>())
    mKVs[key] = std::string{};
}

std::string Movement::get(const MovementKey &key) const try {
  return mKVs.at(key);
} catch (std::exception &) {
  return {};
}

void Movement::set(const MovementKey &key, const std::string &value) {
  mKVs[key] = value;
}

const std::vector<Part *> Movement::parts() const {
  auto _ = mPartListMutex.lock();
  auto rval = std::vector<Part *>();
  for (auto &part : mPartList) {
    rval.push_back(part.get());
  }
  return rval;
}

const Book &Movement::get_book() const {
  return *mBook;
}

size_t Movement::part_count() const {
  auto _ = mPartListMutex.lock();
  return mPartList.size();
}

Part &Movement::get_part(size_t part) const {
  auto _ = mPartListMutex.lock();
  auto &rval = mPartList.at(part);
  return *rval.get();
}

namespace {
void append_empty_bars(Part &part, size_t bars) {
  for (size_t i = 0; i < bars; ++i)
    part.add_bar_at_end();
}
} // namespace

Part &Movement::add_part_at_bottom() {
  auto &rval = append_part();
  append_empty_bars(rval, get_total_bars());
  return rval;
}

Part &Movement::append_part() {
  auto newpart = std::make_unique<Part>(*this);
  //  auto totalbars = get_total_bars();
  {
    auto _ = mPartListMutex.lock();
    mPartList.push_back(std::move(newpart));
    auto &rval = mPartList.back();
    //    append_empty_bars(*rval, totalbars);
    return *rval;
  }
}

Part &Movement::insert_part_at(size_t partno) {
  auto newpart = std::make_unique<Part>(*this);
  auto totalbars = get_total_bars();
  {
    assert::is_true(partno < mPartList.size(),
        "Trying to insert more parts than there are");
    auto _ = mPartListMutex.lock();
    mPartList.insert(mPartList.begin() + partno, std::move(newpart));
    auto &rval = mPartList.at(partno);
    append_empty_bars(*rval, totalbars);
    return *rval;
  }
}

void Movement::delete_part_at(size_t partno) {
  {
    assert::is_true(partno < mPartList.size(),
        "Trying to delete more parts than there are");
    auto _ = mPartListMutex.lock();
    mPartList.erase(mPartList.begin() + partno);
  }
}

const TimeSignature &Movement::get_bar_time_signature(size_t bar) const try {
  return *mTimeSignatures.get_position(Position{bar});
} catch (std::exception &) {
  util::error("Failed to get bar time signature at bar ", bar);
  throw std::runtime_error{"notation/movement::get_bar"};
}

Movement &Movement::add_bar_time_signature(
    size_t bar, std::unique_ptr<TimeSignature> &&ts) {
  mTimeSignatures.add(Position{bar}, std::move(ts));
  return *this;
}

bool Movement::does_time_signature_change_at_bar(size_t bar) const {
  return mTimeSignatures.has_anotation_at_bar(bar);
}

size_t Movement::get_total_bars() const {
  auto _ = mPartListMutex.lock();
  auto total = size_t{};
  for (const auto &part : mPartList) {
    total = std::max(total, part->get_bar_count());
  }
  return total;
}

const geom::Ratio Movement::get_anacrusis() const {
  return mAnacrusis;
}

Movement &Movement::set_anacrusis(const geom::Ratio &anacrusis) {
  mAnacrusis = anacrusis;
  return *this;
}

const Key &Movement::get_bar_key(size_t bar) const try {
  return *mKeys.get_data_after_position(bar);
} catch (std::exception &) {
  util::log(util::LogLevel::Error, "No key at bar ", bar);
  throw std::runtime_error{"notation/movement::get_bar_key"};
}

Movement &Movement::add_bar_key_change(size_t bar, std::unique_ptr<Key> &&key) {
  mKeys.add_data(bar, std::move(key));
  return *this;
}

bool Movement::does_key_change_at_bar(size_t bar) const {
  return mKeys.has_data_at_position(bar);
}

const Tempo &Movement::get_bar_tempo(size_t bar) const {
  return *mTempos.get_data_after_position(bar);
}

bool Movement::does_tempo_change_at_bar(size_t bar) const {
  return mTempos.has_data_at_position(bar);
}

void Movement::add_bar_tempo(size_t bar, std::unique_ptr<Tempo> &&tempo) {
  mTempos.add_data(bar, std::move(tempo));
}

std::optional<Markup *> Movement::get_markup_at_bar(size_t bar) const {
  if (mMarkups.has_data_at_position(bar))
    return mMarkups.get_data_after_position(bar);
  return {};
}

void Movement::add_bar_markup(size_t bar, std::unique_ptr<Markup> &&markup) {
  mMarkups.add_data(bar, std::move(markup));
}

Volta Movement::get_volta(size_t barnumber) const {
  if (mVoltas.has_data_at_position(barnumber))
    return Volta{mVoltas.get_data_after_position(barnumber)};
  return Volta{0};
}

void Movement::set_volta(size_t barnumber, const Volta &volta) {
  mVoltas.add_data(barnumber, volta.get_value());
}

void Movement::add_node_for_class(xml::Node &parent) {
  auto rval = parent.append_child(static_xml_name());

  if (mAnacrusis.numerator() != 0) {
    auto node = rval->append_child("anacrusis");
    mAnacrusis.add_node_for_class(*node);
  }

  for (const auto &[k, v] : mKVs) {
    if (v.empty())
      continue;
    auto keyname = enums::to_string(k);
    rval->append_child(keyname)->set_value(v);
  }

  for (const auto &[bar, volta] : mVoltas) {
    if (volta == 0)
      continue;
    auto node = rval->append_child("volta");
    node->add_node("bar", bar);
    node->add_node("volta", volta);
  }

  for (const auto &[position, time] : mTimeSignatures) {
    auto node = rval->append_child("time-signature-change");
    position.append_position_to_node(*node);
    time->add_node_for_class(*node);
  }

  for (const auto &[bar, key] : mKeys) {
    auto node = rval->append_child("key-signature-change");
    node->add_node("bar", bar);
    key->add_node_for_class(*node);
  }

  for (const auto &[bar, style] : mBarStartStyles) {
    if (style == BarStyle::Single)
      continue;
    auto node = rval->append_child("bar-start-style");
    node->add_node("bar", bar);
    node->add_node("bar-style", style);
  }

  for (const auto &[bar, style] : mBarEndStyles) {
    if (style == BarStyle::Single)
      continue;
    auto node = rval->append_child("bar-end-style");
    node->add_node("bar", bar);
    node->add_node("bar-style", style);
  }

  for (const auto &[bar, tempo] : mTempos) {
    auto node = rval->append_child("tempo-change");
    node->add_node("bar", bar);
    tempo->add_node_for_class(*node);
  }

  // TODO markups

  for (const auto &part : mPartList) {
    part->add_node_for_class(*rval);
  }
};

BarStyle Movement::get_bar_start_style(size_t barnum) const {
  if (mBarStartStyles.has_data_at_position(barnum))
    return mBarStartStyles.get_data_after_position(barnum);
  return BarStyle::Single;
}

BarStyle Movement::get_bar_end_style(size_t barnum) const {
  if (mBarEndStyles.has_data_at_position(barnum))
    return mBarEndStyles.get_data_after_position(barnum);
  return BarStyle::Single;
}

void Movement::set_bar_start_style(size_t barnum, const BarStyle &style) {
  mBarStartStyles.add_data(barnum, style);
}

void Movement::set_bar_end_style(size_t barnum, const BarStyle &style) {
  mBarEndStyles.add_data(barnum, style);
}

std::optional<RehearsalMark> Movement::get_rehearsal_mark_at_bar(
    size_t bar) const {
  auto count = size_t{1};
  // mRehearsalMarks is already a sorted set, so iterate through and see if we
  // find it
  for (const auto &bnum : mRehearsalMarks) {
    if (bnum == bar) // found it
      return {RehearsalMark(count)};
    if (bnum > bar) // didn't find it
      break;
    ++count;
  }
  return {};
}

void Movement::add_rehearsal_mark_at_bar(size_t bar) {
  mRehearsalMarks.insert(bar);
}

void Movement::remove_rehearsal_mark_at_bar(size_t bar) {
  if (auto found = mRehearsalMarks.find(bar); found != mRehearsalMarks.cend())
    mRehearsalMarks.erase(found);
}

void Movement::swap_parts(size_t first, size_t second) {
  auto count = size();
  assert::is_true(first < count, "Part swap out of range");
  assert::is_true(second < count, "Part swap out of range");
  {
    auto _ = mPartListMutex.lock();
    std::swap(mPartList[first], mPartList[second]);
  }
} // namespace notation

namespace {

void add_time_signature_from_node(Movement &movement, const xml::Node &node) {
  auto ts = std::make_unique<TimeSignature>(4, 4);
  auto position = Position::get_position_from_node(node);
  node.once("time-signature",
      [&](const xml::Node &n) { ts = TimeSignature::get_class_for_node(n); });
  movement.add_bar_time_signature(position.bar, std::move(ts));
};

void add_key_signature_from_node(Movement &movement, const xml::Node &node) {
  auto bar = size_t{};
  auto key = std::make_unique<Key>(0);
  node.once("bar", [&](const xml::Node &n) { bar = n.get_value<int>(); });
  node.once(
      "key", [&](const xml::Node &n) { key = Key::get_class_for_node(n); });
  movement.add_bar_key_change(bar, std::move(key));
};

void add_tempo_change_from_node(Movement &movement, const xml::Node &node) {
  auto bar = size_t{};
  auto tempo = std::make_unique<Tempo>("Deserialisation Error");
  node.once("bar", [&](const xml::Node &n) { bar = n.get_value<size_t>(); });
  node.once(Tempo::static_xml_name(),
      [&](const xml::Node &n) { tempo = Tempo::get_class_for_node(n); });
  movement.add_bar_tempo(bar, std::move(tempo));
};

template <class F>
void add_bar_style_from_node(const xml::Node &node, F addition_function) {
  auto bar = size_t{};
  auto key = BarStyle::Single;
  node.once("bar", [&](const xml::Node &n) { bar = n.get_value<size_t>(); });
  node.once(
      "bar-style", [&](const xml::Node &n) { key = n.get_value<BarStyle>(); });
  addition_function(bar, key);
}

void add_bar_start_style_from_node(Movement &movement, const xml::Node &node) {
  add_bar_style_from_node(node, [&movement](size_t bar, BarStyle barStyle) {
    movement.set_bar_start_style(bar, barStyle);
  });
};

void add_bar_end_style_from_node(Movement &movement, const xml::Node &node) {
  add_bar_style_from_node(node, [&movement](size_t bar, BarStyle barStyle) {
    movement.set_bar_end_style(bar, barStyle);
  });
};

void add_volta_from_node(Movement &movement, const xml::Node &node) {
  auto bar = xml::get_or_default<size_t>(node, "bar");
  auto volta = xml::get_or_default<size_t>(node, "volta");
  movement.set_volta(bar, Volta{volta});
};

} // namespace

notation::Movement *notation::Movement::get_class_for_node(
    Book *book, const xml::Node &node) {
  auto &rval = book->add_movement();

  for (const auto key : enums::values<MovementKey>()) {
    auto keyname = enums::to_string(key);
    auto value = xml::get_or_default<std::string>(node, keyname);
    rval.set(key, value);
  }

  node.many("time-signature-change",
      [&](const xml::Node &n) { add_time_signature_from_node(rval, n); });
  node.many("key-signature-change",
      [&](const xml::Node &n) { add_key_signature_from_node(rval, n); });
  node.many("tempo-change",
      [&](const xml::Node &n) { add_tempo_change_from_node(rval, n); });
  node.many("bar-start-style",
      [&](const xml::Node &n) { add_bar_start_style_from_node(rval, n); });
  node.many("bar-end-style",
      [&](const xml::Node &n) { add_bar_end_style_from_node(rval, n); });
  node.many("volta", [&](const xml::Node &n) { add_volta_from_node(rval, n); });
  node.once("anacrusis", [&](const xml::Node &n) {
    n.once("ratio", [&](const xml::Node &sn) {
      auto ratio = geom::Ratio::get_class_for_node(sn);
      rval.set_anacrusis(*ratio.get());
    });
  });
  node.many(
      "part", [&](const xml::Node &n) { Part::get_class_for_node(&rval, n); });
  return &rval;
};

void Movement::add_bar_before(size_t barnum) {
  mTimeSignatures.increase_after(barnum);
  mKeys.increase_after(barnum);
  mBarEndStyles.increase_after(barnum);
  mBarStartStyles.increase_after(barnum);
  mTempos.increase_after(barnum);
  mMarkups.increase_after(barnum);
  mVoltas.increase_after(barnum);
};

void Movement::delete_bar(size_t barnum) {
  mTimeSignatures.decrease_after(barnum);
  mKeys.decrease_after(barnum);
  mBarEndStyles.decrease_after(barnum);
  mBarStartStyles.decrease_after(barnum);
  mTempos.decrease_after(barnum);
  mMarkups.decrease_after(barnum);
  mVoltas.decrease_after(barnum);
};

std::string Movement::get_xml_name() {
  return static_xml_name();
}

size_t Movement::size() const {
  return mPartList.size();
}

Part &Movement::at(size_t s) const {
  auto &rval = mPartList.at(s);
  return *rval.get();
}

} // namespace notation
