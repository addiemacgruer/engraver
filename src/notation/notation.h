#pragma once

#include "geom/ratio.h"
#include "notation/notationtype.h" // for NotationType
#include "xml/serialise.h"

/** \namespace notation \brief models of elements of musical notation, not tied
 * to a specific representation */
namespace notation {

/** super class for all notation */
class Notation :
    /** all notation should be serialisable */
    public xml::Serialise<Notation> {
public:
  /* big five */
  Notation();
  Notation(const Notation &);
  Notation(Notation &&) noexcept;
  Notation &operator=(const Notation &);
  Notation &operator=(Notation &&) noexcept;
  virtual ~Notation() noexcept;
  /** what kind of notation this is,  for casting purposes */
  virtual NotationType get_notation_type() const = 0;
  /** how much time (in semibreve beats,  ie. 4/4 fractions) this notation lasts
   * for */
  virtual const geom::Ratio get_notation_length() const = 0;
  /** cast straight to notation,  for storage */
  Notation &as_notation();
};

} // namespace notation
