#pragma once

#include "annotation.h"          // for Annotation
#include "geom/ratio.h"          // for geom::Ratio
#include "util/iterable.h"       // for util::Container
#include "util/mutex.h"          // for util::Mutex
#include "util/orderedliteral.h" // for util::OrderedLiteral
#include "util/orderedunique.h"  // for util::OrderedUnique
#include "xml/serialise.h"       // for xml::Serialise<>

#include <memory>        // for std::unique_ptr
#include <optional>      // for std::optional
#include <set>           // for std::set
#include <string>        // for std::string
#include <unordered_map> // for std::unordered_map
#include <vector>        // for std::vector

namespace notation {

enum class MovementKey;
class Book;
class Key;
class Markup;
class Part;
class RehearsalMark;
class Tempo;
class TimeSignature;
enum class BarStyle;
class Volta;

class Movement : public xml::Serialise<Movement>, public util::Container<Part> {
public:
  // creators
  explicit Movement(Book *book);
  Movement(const Movement &) = delete;
  Movement &operator=(const Movement &) = delete;
  virtual ~Movement() override;

  //  key/value pairs
  std::string get(const MovementKey &key) const;
  void set(const MovementKey &key, const std::string &value);

  // getters
  bool does_key_change_at_bar(size_t bar) const;
  bool does_tempo_change_at_bar(size_t bar) const;
  bool does_time_signature_change_at_bar(size_t bar) const;
  const geom::Ratio get_anacrusis() const;
  BarStyle get_bar_end_style(size_t barnum) const;
  const Key &get_bar_key(size_t bar) const;
  BarStyle get_bar_start_style(size_t barnum) const;
  const Tempo &get_bar_tempo(size_t bar) const;
  const TimeSignature &get_bar_time_signature(size_t bar) const;
  std::optional<Markup *> get_markup_at_bar(size_t bar) const;
  std::optional<RehearsalMark> get_rehearsal_mark_at_bar(size_t bar) const;
  size_t get_total_bars() const;
  size_t part_count() const;
  const std::vector<Part *> parts() const;
  const Book &get_book() const;
  Part &get_part(size_t part) const;
  Volta get_volta(size_t barnumber) const;

  // mutators
  void add_bar_before(size_t barnum);
  Movement &add_bar_key_change(size_t bar, std::unique_ptr<Key> &&key);
  void add_bar_markup(size_t bar, std::unique_ptr<Markup> &&markup);
  void add_bar_tempo(size_t bar, std::unique_ptr<Tempo> &&tempo);
  Movement &add_bar_time_signature(
      size_t bar, std::unique_ptr<TimeSignature> &&ts);
  /** add a part at the bottom, and make it the same length as the longest part
   */
  Part &add_part_at_bottom();
  void add_rehearsal_mark_at_bar(size_t bar);
  /** add an empty part at the bottom */
  Part &append_part();
  Part &insert_part_at(size_t partno);
  void delete_part_at(size_t partno);
  void delete_bar(size_t barnum);
  void remove_rehearsal_mark_at_bar(size_t bar);
  void swap_parts(size_t first, size_t second);
  Movement &set_anacrusis(const geom::Ratio &anacrusis);
  void set_bar_end_style(size_t barnum, const BarStyle &style);
  void set_bar_start_style(size_t barnum, const BarStyle &style);
  void set_volta(size_t barnumber, const Volta &volta);

  // container
  size_t size() const override;
  Part &at(size_t s) const override;

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static Movement *get_class_for_node(Book *book, const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "movement";
  }

private:
  Book *mBook;
  std::unordered_map<MovementKey, std::string> mKVs{};

  Annotation<TimeSignature> mTimeSignatures;
  util::OrderedUnique<Key> mKeys;
  util::OrderedLiteral<BarStyle> mBarStartStyles;
  util::OrderedLiteral<BarStyle> mBarEndStyles;
  util::OrderedUnique<Tempo> mTempos;
  util::OrderedUnique<Markup> mMarkups;
  util::OrderedLiteral<size_t> mVoltas;
  // TODO rehearsal marks

  std::vector<std::unique_ptr<Part>> mPartList;

  geom::Ratio mAnacrusis{};
  mutable util::Mutex mPartListMutex{};
  std::set<size_t> mRehearsalMarks{};
};

} // namespace notation
