#pragma once

#include "notation/dynamictype.h" // for DynamicType
#include "xml/serialise.h"        // for xml::Serialise
#include <memory>                 // for std::unique_ptr
#include <string>                 // for std::string

namespace notation {

class Dynamic : xml::Serialise<Dynamic> {
public:
  // creator
  explicit Dynamic(const Dynamic &dynamic);
  explicit Dynamic(const DynamicType &type);
  explicit Dynamic(const std::string &text);
  // getter
  std::string get_text() const;
  DynamicType get_type() const;
  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Dynamic> get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "dynamic";
  }

private:
  DynamicType mType;
  std::string mText{};
};

} // namespace notation
