#include "tempo.h"

#include "geom/ratio.h"
#include "util/logging.h"
#include "xml/node.h"

namespace notation {

Tempo::Tempo(const std::string &text) : mText{text} {}

Tempo::Tempo(const std::string &text, const Duration &beatLength, int beatSpeed)
    : mText{text}, mBeatLength{beatLength}, mBeatSpeed{beatSpeed} {}

// getters
const std::string &Tempo::get_text() const {
  return mText;
}
const Duration &Tempo::get_beat_length() const {
  return mBeatLength;
}
int Tempo::get_beat_speed() const {
  return mBeatSpeed;
}

// serialisation
void Tempo::add_node_for_class(xml::Node &parent) {
  auto tempo = parent.append_child(Tempo::static_xml_name());
  xml::add_node_if_true(tempo, "text", mText, true);
  if (mBeatLength.get_length().numerator() != 0)
    mBeatLength.add_node_for_class(*tempo);
  xml::add_node_if_true(tempo, "speed", mBeatSpeed, mBeatSpeed != 0);
}

std::unique_ptr<Tempo> Tempo::get_class_for_node(const xml::Node &node) {
  auto text =
      xml::get_or_default<std::string>(node, "text", "Failed to deserialise");
  auto speed = xml::get_or_default<int>(node, "speed");
  std::unique_ptr<Duration> duration{};
  node.once("duration",
      [&](const xml::Node &n) { duration = Duration::get_class_for_node(n); });
  if (duration)
    return std::make_unique<Tempo>(text, *duration, speed);
  return std::make_unique<Tempo>(text);
}

std::string Tempo::get_xml_name() {
  return static_xml_name();
}

} // namespace notation
