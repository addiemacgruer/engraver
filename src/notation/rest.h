#pragma once

#include "timednotation.h"

namespace notation {

class TimeSignature;

class Rest : public TimedNotation {
public:
  // creators
  Rest();
  /** Create a normal rest */
  explicit Rest(const Duration duration);
  /** Create a whole-bar rest - for view purposes only */
  explicit Rest(const TimeSignature &);

  // notation
  NotationType get_notation_type() const override;

  // serialisers
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Rest> get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "rest";
  }

private:
  bool isWholeBar;
  geom::Ratio mWholeBar;
};

} // namespace notation
