#pragma once

#include "instrument.h"
#include "util/iterable.h"
#include "util/mutex.h"
#include "util/orderedunique.h"
#include "xml/serialise.h"

#include <iosfwd>
#include <memory>
#include <shared_mutex>
#include <vector>

namespace notation {

class Bar;
class Clef;
class Key;
class Markup;
class Movement;

class Part : public xml::Serialise<Part>, public util::Container<Bar> {
public:
  // creators
  explicit Part(Movement &movement);
  Part(const Part &) = delete;
  Part &operator=(const Part &) = delete;
  virtual ~Part() noexcept override;

  // getters
  bool does_clef_change_at_bar(size_t bar) const;
  bool does_key_change_at_bar(size_t bar) const;
  bool does_markup_change_at_bar(size_t bar) const;
  Bar &get_bar(size_t bar) const;
  size_t get_bar_count() const;
  size_t get_bar_number(const Bar &bar) const;
  Clef &get_clef_at_bar(size_t bar) const;
  Instrument &get_instrument();
  /** like the function in Movement, but taking account of transposition */
  const Key get_key_at_bar(size_t bar) const;
  Markup &get_markup_at_bar(size_t bar) const;
  Movement &get_movement() const;
  const std::string get_part_name() const;
  size_t get_section_number() const;

  // setters
  Bar &add_bar_at_end();
  Bar &add_bar_before(size_t bar);
  void add_bar_markup(size_t bar, std::unique_ptr<Markup> &&markup);
  void add_clef_at_bar(size_t bar, std::unique_ptr<Clef> &&clef);
  /** like the function in Movement, but taking account of transposition */
  Part &add_key_at_bar(size_t bar, std::unique_ptr<Key> &&key);
  void delete_bar(size_t bar);
  Part &set_instrument(const Instrument &instrument);
  void set_part_name(const std::string &name);
  Part &set_section_number(int section);

  // Container<Bar>
  size_t size() const override;
  Bar &at(size_t s) const override;

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static Part *get_class_for_node(Movement *movement, const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "part";
  }

private:
  Movement *mpMovement;
  Instrument mInstrument;
  int mSectionNumber{0};
  util::OrderedUnique<Clef> mClefChanges;
  util::OrderedUnique<Markup> mMarkups;
  std::string mPartName{};

  mutable util::Mutex mBarsMutex{};
  std::vector<std::unique_ptr<Bar>> mBars{};
};

std::ostream &operator<<(std::ostream &out, Part &part);

} // namespace notation
