#include "dynamic.h"

#include "util/assert.h"
#include "util/logging.h"
#include "xml/node.h"

#include <stdexcept>
#include <vector>

namespace notation {

Dynamic::Dynamic(const std::string &text) {
  try {
    mType = enums::from_string<DynamicType>(text);
  } catch (std::runtime_error &) {
    mType = DynamicType::custom;
    mText = text;
  }
}

Dynamic::Dynamic(const DynamicType &type) {
  if (type == DynamicType::custom)
    throw std::runtime_error{
        "Tried to initialise a dynamic with 'custom' type"};
  mType = type;
}

Dynamic::Dynamic(const Dynamic &dynamic)
    : mType{dynamic.mType}, mText{dynamic.mText} {}

DynamicType Dynamic::get_type() const {
  return mType;
}

std::string Dynamic::get_text() const {
  if (mType == DynamicType::custom)
    return mText;
  return enums::to_string(mType);
}

void Dynamic::add_node_for_class(xml::Node &parent) {
  auto dynamic = parent.append_child("dynamic");
  dynamic->add_node("type", get_text());
}

std::unique_ptr<Dynamic> Dynamic::get_class_for_node(const xml::Node &node) {
  auto rval = std::string{"f"};
  node.once(
      "type", [&](const xml::Node &n) { rval = n.get_value<std::string>(); });
  return std::make_unique<Dynamic>(rval);
}

std::string Dynamic::get_xml_name() {
  return static_xml_name();
}

} // namespace notation
