#pragma once

#include "duration.h"
#include "notation.h"

#include <set>

namespace notation {

enum class Articulation;

class TimedNotation : public Notation {
public:
  // creators
  explicit TimedNotation(const Duration duration);
  ~TimedNotation() override;

  // getters
  const std::set<Articulation> get_articulations() const;
  const Duration get_duration() const;
  const geom::Ratio get_notation_length() const override;
  bool has_articulation(const Articulation &articulation) const;

  // setters
  bool add_articulation(const Articulation &articulation);
  bool remove_articulation(const Articulation &articulation);
  void set_duration(const Duration &duration);

  // serialisers
  void add_node_for_class(xml::Node &parent) override;
  static void decorate_timed_notation(TimedNotation *tn, const xml::Node &node);

private:
  Duration mDuration;
  std::set<Articulation> mArticulationSet{};
};

} // namespace notation
