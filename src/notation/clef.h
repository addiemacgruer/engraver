#pragma once

#include "notation.h"          // for Notation
#include "notation/cleftype.h" // for ClefType
#include "pitch.h"

#include <iosfwd>
#include <string>
#include <vector>

namespace notation {

enum class Sound;

class Clef : public Notation {
public:
  // creators
  static std::unique_ptr<Clef> with_name(const std::string &);
  explicit Clef(const Pitch &centralNote, ClefType clefType,
      const std::string &name, const int ledgerOffset = 0);
  explicit Clef(const Clef *clef);

  // getters
  const Pitch get_central_note() const;
  ClefType get_clef_symbol() const;
  int get_ledger_offset() const;
  const std::string get_name() const;
  const geom::Ratio get_notation_length() const override;
  NotationType get_notation_type() const override;
  Sound get_sound_at_line(int line) const;

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Clef> get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "clef";
  }

  // standard clefs
  static std::unique_ptr<Clef> Alto();
  static std::unique_ptr<Clef> Baritone();
  static std::unique_ptr<Clef> Bass();
  static std::unique_ptr<Clef> FrenchViolin();
  static std::unique_ptr<Clef> MezzoSoprano();
  static std::unique_ptr<Clef> Percussion();
  static std::unique_ptr<Clef> Soprano();
  static std::unique_ptr<Clef> SubBass();
  static std::unique_ptr<Clef> Tenor();
  static std::unique_ptr<Clef> Treble();

  static std::vector<std::string> get_clefs();

private:
  Pitch mCentralNote;
  ClefType mClefType;
  std::string mName;
  int mLedgerOffset;
};

Pitch get_pitch_for_clef(ClefType);
std::ostream &operator<<(std::ostream &out, const Clef &clef);

} // namespace notation
