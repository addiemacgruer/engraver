#include "volta.h"

namespace notation {

Volta::Volta(size_t value) : mValue{value} {}

size_t Volta::get_value() const {
  return mValue;
}

} // namespace notation
