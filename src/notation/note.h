#pragma once

#include "pitch.h"
#include "timednotation.h"

#include <memory>
#include <optional>

namespace notation {

enum class Articulation : int;
enum class Span;
class Dynamic;

class Note : public TimedNotation {
public:
  // creators
  Note();
  Note(const Pitch &pitch, const Duration duration);
  ~Note() override;

  // getters
  std::optional<Dynamic> get_dynamic() const;
  const std::set<Span> get_end_spans() const;
  const Pitch get_pitch() const;
  const std::set<Span> get_start_spans() const;
  bool has_courtesy_accidental() const;
  bool has_span_end(const Span span) const;
  bool has_span_start(const Span span) const;
  bool is_tied() const;

  // mutators
  void add_span_end(const Span span);
  void add_span_start(const Span span);
  void remove_span_end(const Span span);
  void remove_span_start(const Span span);
  void clear_dynamic();
  void pitch(const Pitch &pitch);
  void set_courtesy_accidental(bool b);
  void set_dynamic(std::unique_ptr<Dynamic> &&dynamic);
  void set_tremolo(int tremolo); // TODO tremolo accessors, display, ...
  void tie_to_next(bool set = true);

  // notation
  NotationType get_notation_type() const override;

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Note> get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "note";
  }

private:
  Pitch mPitch;
  std::set<Span> mSpanStartSet{};
  std::set<Span> mSpanEndSet{};
  bool mHasCourtesyAccidental{false};
  bool mIsTiedToNext{false};
  std::unique_ptr<Dynamic> mDynamic{};
  int mTremolo{0};
};

std::ostream &operator<<(std::ostream &out, const Note &note);

} // namespace notation
