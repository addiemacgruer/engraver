#include "tuplet.h"

#include "util/logging.h"
#include "xml/node.h"

namespace notation {

Tuplet::Tuplet(const geom::Ratio &ratio)
    : mIsStart{true}, mTupletRatio{ratio} {}

Tuplet::Tuplet() : mIsStart{false}, mTupletRatio{} {}

// getters
bool Tuplet::is_start() const {
  return mIsStart;
}
const geom::Ratio &Tuplet::get_ratio() const {
  return mTupletRatio;
}

// notation
NotationType Tuplet::get_notation_type() const {
  return NotationType::Tuplet;
}
const geom::Ratio Tuplet::get_notation_length() const {
  return {};
}

// serialisation
void Tuplet::add_node_for_class(xml::Node &node) {
  auto tuplet = node.append_child("tuplet");
  tuplet->add_node("start", mIsStart);
  if (mIsStart)
    mTupletRatio.add_node_for_class(*tuplet);
}

std::unique_ptr<Tuplet> Tuplet::get_class_for_node(const xml::Node &node) {
  bool isStart{true};
  std::unique_ptr<geom::Ratio> tupletRatio = std::make_unique<geom::Ratio>();
  node.once(
      "start", [&](const xml::Node &n) { isStart = n.get_value<bool>(); });
  node.once("ratio", [&](const xml::Node &n) {
    tupletRatio = geom::Ratio::get_class_for_node(n);
  });
  if (isStart)
    return std::make_unique<Tuplet>(*tupletRatio);
  return std::make_unique<Tuplet>();
}

// TupletStack

TupletStack::TupletStack() = default;

bool TupletStack::add_tuplet(Tuplet *tuplet) {
  if (tuplet->is_start()) {
    mStack.push_back(tuplet->get_ratio());
    mRatio *= tuplet->get_ratio();
    return true;
  } else {
    if (mStack.empty()) {
      util::error("Tuplets are not balanced!");
      return false;
    }
    mRatio /= mStack.back();
    mStack.pop_back();
    return true;
  }
}

geom::Ratio TupletStack::get_ratio() const {
  return mRatio;
}

std::string Tuplet::get_xml_name() {
  return static_xml_name();
}
} // namespace notation
