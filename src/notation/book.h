#pragma once

#include "notation/bookkey.h" // for BookKey
#include "util/iterable.h"    // for util::Container
#include "xml/serialise.h"    // for xml::Serialise

#include <memory>        // for std::unique_ptr
#include <unordered_map> // for std::unordered_map
#include <vector>        // for std::vector

namespace notation {

class Movement;

class Book : public xml::Serialise<Book>, public util::Container<Movement> {
public:
  // creators
  Book();
  Book(const Book &) = delete;
  virtual ~Book() noexcept override;

  //  key/value pairs
  std::string get(const BookKey &key) const;
  void set(const BookKey &key, const std::string &value);

  // getters
  Movement &get_last_movement() const;

  // mutators
  Movement &add_movement();
  Movement &delete_movement(size_t); // TODO implement
  Movement &insert_movement(size_t); // TODO implement

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Book> class_for_node(xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "book";
  }

  // container
  size_t size() const override;
  Movement &at(size_t s) const override;

private:
  std::vector<std::unique_ptr<Movement>> mMovements{};
  std::unordered_map<BookKey, std::string> mKeys{};
};

} // namespace notation
