#include "pitch.h"

#include "util/logging.h"
#include "util/string.h"
#include "xml/node.h"

#include <cmath>
#include <stdexcept>

namespace notation {
// creation
Pitch::Pitch(const Sound &sound, const Accidental &accidental, const int octave)
    : mSound(sound), mAccidental(accidental), mOctave(octave) {
  auto intsound = enums::ordinal(sound);
  if (intsound < 0 || intsound >= 7)
    throw std::invalid_argument("Bad sound");
  auto intaccidental = enums::ordinal(accidental);
  if (intaccidental >= 7)
    throw std::invalid_argument("Bad accidental");
};

namespace { // correspondence of MIDI octave to natural notes
Sound sSound[] = {Sound::C, Sound::C, Sound::D, Sound::D, Sound::E, Sound::F,
    Sound::F, Sound::G, Sound::G, Sound::A, Sound::A, Sound::B};
Accidental sAccidental[] = {Accidental::Natural, Accidental::Sharp,
    Accidental::Natural, Accidental::Sharp, Accidental::Natural,
    Accidental::Natural, Accidental::Sharp, Accidental::Natural,
    Accidental::Sharp, Accidental::Natural, Accidental::Sharp,
    Accidental::Natural};
} // namespace

Pitch::Pitch(const int midiPitchNumber) : mAccidental{Accidental::Natural} {
  if (midiPitchNumber < 0) {
    util::log(util::LogLevel::Error, "Not a valid midi note:", midiPitchNumber);
    throw std::invalid_argument{"notation/pitch::Pitch"};
  }
  mOctave = (midiPitchNumber / 12) - 1;
  auto midiPitchInOctave = midiPitchNumber % 12;
  mSound = sSound[midiPitchInOctave];
  mAccidental = sAccidental[midiPitchInOctave];
}
// Pitch Pitch::helmholtz(const std::string &helmholtz);

// methods

Sound Pitch::get_sound() const {
  return mSound;
}

int Pitch::get_octave() const {
  return mOctave;
}

Accidental Pitch::get_accidental() const {
  return mAccidental;
}

int Pitch::get_midi_pitch_number() const {
  // static cast of mSound gives the semitone offset, as does mAccidental`
  return (mOctave * 12) + 12 + static_cast<int>(mSound) +
         static_cast<int>(mAccidental);
}

int Pitch::get_ledger_number() const {
  return enums::count<Sound>() * mOctave + enums::ordinal(mSound);
}

double Pitch::get_fundamental_frequency_in_hz() const {
  return 440 * std::pow(2.0, double(get_midi_pitch_number() - 69) / 12.0);
}

bool Pitch::operator==(const Pitch &rhs) const {
  return (mSound == rhs.mSound && mAccidental == rhs.mAccidental &&
          mOctave == rhs.mOctave);
}

bool Pitch::operator<(const Pitch &rhs) const {
  return get_midi_pitch_number() < rhs.get_midi_pitch_number();
}

bool Pitch::operator>(const Pitch &rhs) const {
  return !operator<(rhs);
}

const Pitch Pitch::get_one_sound_up() const {
  return operator+(1);
}

const Pitch Pitch::get_one_sound_down() const {
  return operator+(-1);
}

void Pitch::add_node_for_class(xml::Node &node) {
  auto pitch = node.append_child("pitch");
  pitch->add_node("sound", mSound);
  if (mAccidental != Accidental::Natural) {
    pitch->add_node("accidental", mAccidental);
  }
  pitch->add_node("octave", mOctave);
};

std::unique_ptr<Pitch> Pitch::get_class_for_node(const xml::Node &node) {
  auto sound = Sound::C;
  auto accidental = notation::Accidental(Accidental::Natural);
  auto octave = 4;

  node.once("sound", [&](const xml::Node &n) { sound = n.get_value<Sound>(); });

  node.once("accidental",
      [&](const xml::Node &n) { accidental = n.get_value<Accidental>(); });
  node.once("octave", [&](const xml::Node &n) { octave = n.get_value<int>(); });
  return std::make_unique<Pitch>(sound, accidental, octave);
}

const Pitch Pitch::operator+(int offset) const {
  auto newNoteLedger = get_ledger_number() + offset;
  auto sound =
      enums::from_ordinal<Sound>(newNoteLedger % enums::count<Sound>());
  auto octave = newNoteLedger / enums::count<Sound>();
  return {sound, Accidental::Natural, octave};
}

const Pitch Pitch::operator-(int offset) const {
  return operator+(-offset);
}

std::string Pitch::get_xml_name() {
  return static_xml_name();
}
} // namespace notation
