#pragma once

#include "notation/instrumenttype.h" // for InstrumentType
#include "pitch.h"
#include "xml/serialise.h"

#include <vector>

namespace notation {

class Clef;

class Instrument : public xml::Serialise<Instrument> {
public:
  // creators
  Instrument();
  Instrument(const std::string &name,
      const InstrumentType &family = InstrumentType::Piano,
      const Pitch &lowest = Pitch{0}, const Pitch &highest = Pitch{127},
      int transpositionSemitones = 0);

  // getters
  const std::vector<Clef> get_clef_list() const;
  const Pitch &get_highest_pitch() const;
  InstrumentType get_instrument_family() const;
  const Pitch &get_lowest_pitch() const;
  const std::string get_name() const;
  const std::string get_short_name() const;
  int get_transposition_in_semitones() const;

  // mutators
  Instrument &add_clef(const Clef &clef);
  void set_name(const std::string &);
  void set_short_name(const std::string &);

  // serialisation
  void add_node_for_class(xml::Node &node) override;
  static Instrument get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "instrument";
  }

private:
  std::string mName;
  std::string mShortName;
  InstrumentType mFamily;
  Pitch mLowest;
  Pitch mHighest;
  std::vector<Clef> mClefList;
  int mTranspositionInSemitones;
  // TODO whether to display transposed on conductors' part, or what.
};

} // namespace notation
