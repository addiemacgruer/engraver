#pragma once

#include "geom/ratio.h"         // for geom::Ratio
#include "notation.h"           // for Notation
#include "notation/barstatus.h" // for BarStatus
#include "util/iterable.h"      // for util::Container

#include <functional> // for std::function
#include <memory>     // for std::unique_ptr
#include <vector>     // for std::vector

namespace notation {

enum class BarStyle;
class Part;
class Volta;

class Bar : public Notation, public util::Container<Notation> {
public:
  // creation
  explicit Bar(Part &part);
  Bar(const Bar &) = delete;
  Bar &operator=(const Bar &) = delete;
  ~Bar() override;

  // getters
  Part &get_part() const;
  BarStyle get_bar_end_style() const;
  /** which bar number is this (in the part) */
  size_t get_bar_number() const;
  BarStyle get_bar_start_style() const;
  BarStatus get_bar_status() const;
  size_t get_notation_count() const;
  int get_shortest_notation_duration() const;
  /** returns true if the bar is empty, or if it just contains rests with no
   * adornments */
  bool is_rest_bar() const;
  Volta get_volta() const;

  // Notation
  const geom::Ratio get_notation_length() const override;
  NotationType get_notation_type() const override;

  // util::Container<Notation>
  Notation &at(size_t s) const override;
  size_t size() const override;

  // mutators
  void add_notation_at_position(
      std::unique_ptr<Notation> &&notation, const size_t position);
  void append_notation(std::unique_ptr<Notation> &&notation);
  std::unique_ptr<Notation> delete_notation(const size_t position);
  Bar &set_bar_end_style(const BarStyle &barEnd);
  Bar &set_bar_start_style(const BarStyle &bs);

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static Bar *get_class_for_node(Part *part, const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "bar";
  }

  // horrible hack, due to denemo parsing not outputting markup in order.  Add
  // some lambdas to a bar, which will then be executed and cleared once the
  // whole bar is parsed.
  // TODO move this elsewhere
  using PostFix = std::function<void(void)>;
  /** add a function to this bar, to be executed later */
  void add_postfix(const PostFix &postfix);
  /** execute and clear all functions */
  void apply_postfixes();

private:
  void calculate_status() const;
  void check_special_notation(Notation *notation);

  mutable geom::Ratio mLength{};
  mutable BarStatus mBarStatus{BarStatus::TooEmpty};
  mutable bool mStatusIsValid{false};

  Part *mpPart;
  std::vector<std::unique_ptr<Notation>> mNotation{};

  // TODO move this elsewhere
  std::vector<PostFix> mPostfixes{};
};

} // namespace notation
