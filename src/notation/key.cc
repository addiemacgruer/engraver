#include "key.h"

#include "notation/accidental.h" // for Accidental
#include "notation/mode.h"       // for Mode
#include "notation/sound.h"      // for Sound
#include "pitch.h"
#include "util/logging.h"
#include "util/string.h"
#include "xml/node.h"

#include <stdexcept>

namespace notation {

Key::Key(const Key *key) : mAccidentals{key->mAccidentals} {}

namespace {

/** this is the number of sharps / flats in a given key */
const int circleOfFifths[] = {
    0,  // c
    -5, // c#
    2,  // d
    -3, // eb
    4,  // e
    -1, // f
    6,  // f#
    1,  // g
    -4, // ab
    3,  // a
    -2, // bb
    5   // b
};

const char *majors[] = {"Cb\0", "Gb\0", "Db\0", "Ab\0", "Eb\0", "Bb\0", "F\0",
    "C\0", "G\0", "D\0", "A\0", "E\0", "B\0", "F#\0", "C#\0"};

const char *minors[] = {"Ab\0", "Eb\0", "Bb\0", "F\0", "C\0", "G\0", "D\0",
    "A\0", "E\0", "B\0", "F#\0", "C#\0", "G#\0", "D#\0", "A#\0"};

int accidentalsForKey(const std::string &key, const char *mode[]) {
  auto rval{-7};
  for (int i = 0; i < 15; i++) {
    if (key == mode[i]) {
      return rval;
    }
    ++rval;
  }
  util::error("Couldn't find a matching key for ", key);
  return 0;
}

} // namespace

int Key::get_accidentals_in_major_key(const std::string &key) {
  return accidentalsForKey(key, majors);
}
int Key::get_accidentals_in_minor_key(const std::string &key) {
  return accidentalsForKey(key, minors);
}

static const std::vector<Sound> sharpOrder_ = {
    Sound::F, Sound::C, Sound::G, Sound::D, Sound::A, Sound::E, Sound::B};

static const std::vector<Sound> flatOrder_ = {
    Sound::B, Sound::E, Sound::A, Sound::D, Sound::G, Sound::C, Sound::F};

Key::Key(const int accidentals) : mAccidentals{accidentals} {}

static int accidentalsForKey(const Sound &, const Accidental &, const Mode &) {
  util::critical("Key based on sound/accidental/diatonic not implemented");
  throw std::runtime_error("notation/key::Key");
}

Key::Key(const Sound &sound, const Accidental &accidental = Accidental::Natural,
    const Mode &mode = Mode::Major)
    : mAccidentals(accidentalsForKey(sound, accidental, mode)) {}

const Pitch Key::get_relative_major() const {
  switch (mAccidentals) {
  case 0:
    return {Sound::C, Accidental::Natural};
  case 1:
    return {Sound::G, Accidental::Natural};
  case 2:
    return {Sound::D, Accidental::Natural};
  case 3:
    return {Sound::A, Accidental::Natural};
  case 4:
    return {Sound::E, Accidental::Natural};
  case 5:
    return {Sound::B, Accidental::Natural};
  case 6:
    return {Sound::F, Accidental::Sharp};
  case 7:
    return {Sound::C, Accidental::Sharp};
  case -1:
    return {Sound::F, Accidental::Natural};
  case -2:
    return {Sound::B, Accidental::Flat};
  case -3:
    return {Sound::E, Accidental::Flat};
  case -4:
    return {Sound::A, Accidental::Flat};
  case -5:
    return {Sound::D, Accidental::Flat};
  case -6:
    return {Sound::G, Accidental::Flat};
  case -7:
    return {Sound::C, Accidental::Flat};
  default:
    util::error("Too many accidentals in key:", mAccidentals);
    throw std::runtime_error{"notation/key::relativeMajor"};
  }
}

const Pitch Key::get_relative_minor() const {
  switch (mAccidentals) {
  case 0:
    return {Sound::A, Accidental::Natural};
  case 1:
    return {Sound::E, Accidental::Natural};
  case 2:
    return {Sound::B, Accidental::Natural};
  case 3:
    return {Sound::F, Accidental::Sharp};
  case 4:
    return {Sound::C, Accidental::Sharp};
  case 5:
    return {Sound::G, Accidental::Sharp};
  case 6:
    return {Sound::D, Accidental::Sharp};
  case 7:
    return {Sound::A, Accidental::Sharp};
  case -1:
    return {Sound::D, Accidental::Natural};
  case -2:
    return {Sound::G, Accidental::Natural};
  case -3:
    return {Sound::C, Accidental::Natural};
  case -4:
    return {Sound::F, Accidental::Natural};
  case -5:
    return {Sound::B, Accidental::Flat};
  case -6:
    return {Sound::E, Accidental::Flat};
  case -7:
    return {Sound::A, Accidental::Flat};
  default:
    util::error("Too many accidentals in key:", mAccidentals);
    throw std::runtime_error{"notation/key::relativeMinor"};
  }
}

const std::vector<Sound> Key::get_sharp_order_list() {
  return sharpOrder_;
}

const std::vector<Sound> Key::get_flat_order_list() {
  return flatOrder_;
}

const Key Key::get_transposed_key(int semitones) const {
  while (semitones < 0) // ignore whole-octave transpositions
    semitones += 12;
  while (semitones >= 12)
    semitones -= 12;
  if (semitones == 0) // concert pitch
    return *this;

  // TODO check the logic here - looks right for 'simple' keys
  auto keychange = circleOfFifths[semitones];
  auto newaccidentals = mAccidentals + keychange;
  while (newaccidentals < -6)
    newaccidentals += 12;
  while (newaccidentals > 6)
    newaccidentals -= 12;
  return Key{mAccidentals + keychange};
}

namespace {
Accidental get_accidental_for_sound(const Sound &sound, int accidentals) {
  if (accidentals < 0) { // flats
    for (int i = 0; i < abs(accidentals); ++i) {
      if (flatOrder_[i] == sound)
        return Accidental::Flat;
    }
  } else if (accidentals > 0) { // sharps
    for (int i = 0; i < abs(accidentals); ++i) {
      if (sharpOrder_[i] == sound)
        return Accidental::Sharp;
    }
  }
  return Accidental::Natural;
}
} // namespace

Pitch Key::get_pitch_in_key(const Pitch &pitch) const {
  auto sound = pitch.get_sound();
  auto octave = pitch.get_octave();
  auto accidental = get_accidental_for_sound(sound, mAccidentals);
  return {sound, accidental, octave};
}

NotationType Key::get_notation_type() const {
  return NotationType::Key;
}

const geom::Ratio Key::get_notation_length() const {
  return {};
}

void Key::add_node_for_class(xml::Node &node) {
  auto key = node.append_child("key");
  if (mAccidentals != 0)
    key->add_node("accidentals", mAccidentals);
};

std::unique_ptr<Key> Key::get_class_for_node(const xml::Node &node) {
  auto accidentals{0};
  node.once("accidentals",
      [&](const xml::Node &n) { accidentals = n.get_value<int>(); });
  return std::make_unique<Key>(accidentals);
};

int Key::accidentals() const {
  return mAccidentals;
}

std::string Key::get_xml_name() {
  return static_xml_name();
}
} // namespace notation
