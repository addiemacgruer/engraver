#pragma once

#include "position.h"

#include <algorithm>
#include <memory>
#include <vector>

namespace notation {

template <class T>
struct AnnotationPosition {
  Position position;
  std::unique_ptr<T> data;
};

template <class T>
bool operator<(
    const AnnotationPosition<T> &first, const AnnotationPosition<T> &second) {
  return first.position < second.position;
}

template <class T>
class Annotation {
public:
  explicit Annotation(std::unique_ptr<T> &&defaultValue)
      : mDefaultValue{std::move(defaultValue)} {}

  Annotation(const Annotation &other)
      : mList{other.mList}, mDefaultValue{other.mDefaultValue} {}

  Annotation &operator=(const Annotation &other) {
    if (this == other)
      return *this;
    mDefaultValue = other.mDefaultValue;
    mList = other.mList;
    return *this;
  }

  void add(const Position &p, std::unique_ptr<T> &&data) {
    auto duplicates = std::remove_if(mList.begin(), mList.end(),
        [&](const AnnotationPosition<T> &d) { return p == d.position; });
    if (duplicates != mList.end())
      mList.erase(duplicates);
    mList.push_back({p, std::move(data)});
    sort_list();
  }

  T *get_position(const Position &p) const {
    if (p < mList.at(0).position)
      return mDefaultValue.get();
    for (size_t offset{0}; offset < mList.size() - 1; offset++) {
      if (p >= mList.at(offset).position && mList.at(offset + 1).position < p) {
        auto &rval = mList.at(offset).data;
        return rval.get();
      }
    }
    auto &rval = mList.at(mList.size() - 1).data;
    return rval.get();
  }

  auto has_anotation_at_bar(const size_t bar) const {
    for (const auto &data : mList) {
      if (data.position == bar)
        return true;
      if (data.position > bar)
        return false;
    }
    return false;
  }

  void increase_after(size_t bar) {
    for (auto &data : mList) {
      if (data.position >= bar)
        ++data.position.bar;
    }
  }

  void decrease_after(size_t bar) {
    auto removeiterator = std::remove_if(mList.begin(), mList.end(),
        [&](const AnnotationPosition<T> &d) { return d.position.bar == bar; });
    if (removeiterator != mList.end())
      mList.erase(removeiterator);
    for (auto &data : mList) {
      if (data.position > bar)
        --data.position.bar;
    }
  }

  auto begin() noexcept {
    return mList.begin();
  }

  auto cbegin() const noexcept {
    return mList.cbegin();
  }

  auto end() noexcept {
    return mList.end();
  }

  auto cend() const noexcept {
    return mList.cend();
  }

private:
  void sort_list() {
    std::sort(mList.begin(), mList.end());
  }

  std::vector<AnnotationPosition<T>> mList{};
  std::unique_ptr<T> mDefaultValue;
};

} // namespace notation
