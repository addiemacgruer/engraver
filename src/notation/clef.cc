#include "clef.h"

#include "notation/cleftype.h" // for ClefType
#include "notation/sound.h"    // for Sound
#include "util/assert.h"
#include "util/hash.h" // for hash functions
#include "util/logging.h"
#include "util/string.h"
#include "xml/node.h"

#include <iostream>

namespace notation {

std::vector<std::string> Clef::get_clefs() {
  static std::vector<std::string> rval = {"Treble", "FrenchViolin", "Bass",
      "Baritone", "SubBass", "Alto", "Tenor", "MezzoSoprano", "Soprano",
      "Percussion"};
  return rval;
}

std::unique_ptr<Clef> Clef::with_name(const std::string &clef) {
  switch (util::lowercase_hash(clef)) {
  case util::hash("treble"):
    return Treble();
  case util::hash("frenchviolin"):
    return FrenchViolin();
  case util::hash("bass"):
    return Bass();
  case util::hash("baritone"):
    return Baritone();
  case util::hash("subbass"):
    return SubBass();
  case util::hash("alto"):
    return Alto();
  case util::hash("tenor"):
    return Tenor();
  case util::hash("mezzosoprano"):
    return MezzoSoprano();
  case util::hash("soprano"):
    return Soprano();
  case util::hash("percussion"):
    return Percussion();
  }
  throw std::runtime_error{util::concat("Unknown clef: ", clef)};
}

Clef::Clef(const Clef *clef)
    : mCentralNote{clef->mCentralNote},
      mClefType{clef->mClefType},
      mName{clef->mName},
      mLedgerOffset{clef->mLedgerOffset} {
  assert::is_not_null(clef);
}

NotationType Clef::get_notation_type() const {
  return NotationType::Clef;
}
const geom::Ratio Clef::get_notation_length() const {
  return {};
}

Clef::Clef(const Pitch &centralNote, ClefType clefType, const std::string &name,
    const int ledgerOffset)
    : mCentralNote{centralNote},
      mClefType{clefType},
      mName{name},
      mLedgerOffset{ledgerOffset} {}

const Pitch Clef::get_central_note() const {
  return mCentralNote;
}

int Clef::get_ledger_offset() const {
  return mLedgerOffset;
}

ClefType Clef::get_clef_symbol() const {
  return mClefType;
}

const std::string Clef::get_name() const {
  return mName;
}

Sound Clef::get_sound_at_line(int line) const {
  auto centralSound = mCentralNote.get_sound();
  return centralSound + 5 - line;
}

Pitch get_pitch_for_clef(ClefType mType) {
  switch (mType) {
  case ClefType::G:
    return Pitch{Sound::G, Accidental::Natural, 4};
  case ClefType::C:
    return Pitch{Sound::C, Accidental::Natural, 4};
  case ClefType::F:
    return Pitch{Sound::F, Accidental::Natural, 3};
  case ClefType::Neutral:
    return Pitch{Sound::C, Accidental::Natural, 4};
  }
}

std::ostream &operator<<(std::ostream &out, const Clef &clef) {
  out << clef.get_clef_symbol() << "-clef, offset=" << clef.get_ledger_offset()
      << " (" << clef.get_name() << ")";
  return out;
}

void Clef::add_node_for_class(xml::Node &node) {
  auto clef = node.append_child("clef");
  clef->add_node("name", mName);
}

std::unique_ptr<Clef> Clef::get_class_for_node(const xml::Node &node) {
  auto name = std::string{};
  node.once(
      "name", [&](const xml::Node &n) { name = n.get_value<std::string>(); });
  if (name.size() > 0)
    return with_name(name);
  throw std::runtime_error{"Didn't recognised clef symbol"};
}

std::unique_ptr<Clef> Clef::Treble() {
  return std::make_unique<Clef>(
      Pitch{Sound::B, Accidental::Natural, 4}, ClefType::G, "Treble");
}
std::unique_ptr<Clef> Clef::FrenchViolin() {
  return std::make_unique<Clef>(Pitch{Sound::D, Accidental::Natural, 5},
      ClefType::G, "French Violin", -2);
}
std::unique_ptr<Clef> Clef::Bass() {
  return std::make_unique<Clef>(
      Pitch{Sound::D, Accidental::Natural, 3}, ClefType::F, "Bass");
}
std::unique_ptr<Clef> Clef::Baritone() {
  return std::make_unique<Clef>(
      Pitch{Sound::F, Accidental::Natural, 3}, ClefType::F, "Baritone", -2);
}
std::unique_ptr<Clef> Clef::SubBass() {
  return std::make_unique<Clef>(
      Pitch{Sound::B, Accidental::Natural, 2}, ClefType::F, "Sub bass", 2);
}
std::unique_ptr<Clef> Clef::Alto() {
  return std::make_unique<Clef>(
      Pitch{Sound::C, Accidental::Natural, 4}, ClefType::C, "Alto");
}
std::unique_ptr<Clef> Clef::Tenor() {
  return std::make_unique<Clef>(
      Pitch{Sound::A, Accidental::Natural, 3}, ClefType::C, "Tenor", 2);
}
std::unique_ptr<Clef> Clef::MezzoSoprano() {
  return std::make_unique<Clef>(
      Pitch{Sound::E, Accidental::Natural, 4}, ClefType::C, "MezzoSoprano", -2);
}
std::unique_ptr<Clef> Clef::Soprano() {
  return std::make_unique<Clef>(
      Pitch{Sound::G, Accidental::Natural, 4}, ClefType::C, "Soprano", -4);
}
std::unique_ptr<Clef> Clef::Percussion() {
  return std::make_unique<Clef>(Pitch{60}, ClefType::Neutral, "Percussion");
}

std::string Clef::get_xml_name() {
  return static_xml_name();
}
} // namespace notation
