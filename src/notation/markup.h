#pragma once

#include "notation/markupposition.h" // for MarkupPosition
#include "xml/serialise.h"

#include <memory>
#include <string>

namespace notation {

class Markup : xml::Serialise<Markup> {
public:
  // creation
  Markup();
  explicit Markup(const std::string &markup);

  // getters
  MarkupPosition get_position() const;
  const std::string &get_text() const;
  bool is_bold() const;
  bool is_italic() const;

  // setters
  void set_bold(bool bold = true);
  void set_italic(bool italic = true);
  void set_position(MarkupPosition position = MarkupPosition::Either);
  void set_text(const std::string &text);

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Markup> get_class_for_node(const xml::Node &node);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "markup";
  }

private:
  std::string mText{};
  bool mIsBold{false};
  bool mIsItalic{false};
  MarkupPosition mPosition{MarkupPosition::Either};
};

} // namespace notation
