#include "note.h"

#include "dynamic.h"
#include "notation/articulations.h" // for Articulations
#include "notation/span.h"
#include "pitch.h"
#include "util/logging.h"
#include "xml/node.h"

namespace notation {

Note::~Note() = default;

Note::Note() : TimedNotation{Duration{4}}, mPitch{Pitch{60}} {}

NotationType Note::get_notation_type() const {
  return NotationType::Note;
}

Note::Note(const Pitch &pitch, const Duration duration)
    : TimedNotation{duration}, mPitch{pitch} {};

const Pitch Note::get_pitch() const {
  return mPitch;
}

void Note::pitch(const Pitch &pitch) {
  mPitch = pitch;
}

const std::set<Span> Note::get_start_spans() const {
  return mSpanStartSet;
}

bool Note::has_span_start(const Span span) const {
  auto found = mSpanStartSet.find(span);
  return (found != mSpanStartSet.cend());
}

const std::set<Span> Note::get_end_spans() const {
  return mSpanEndSet;
}

bool Note::has_span_end(const Span span) const {
  auto found = mSpanEndSet.find(span);
  return (found != mSpanEndSet.cend());
}

void Note::add_span_start(const Span span) {
  mSpanStartSet.insert(span);
}

void Note::add_span_end(const Span span) {
  mSpanEndSet.insert(span);
}

void Note::remove_span_end(const Span span) {
  mSpanEndSet.erase(span);
}

void Note::remove_span_start(const Span span) {
  mSpanStartSet.erase(span);
}

bool Note::has_courtesy_accidental() const {
  return mHasCourtesyAccidental;
}
void Note::set_courtesy_accidental(bool b) {
  mHasCourtesyAccidental = b;
}

void Note::set_dynamic(std::unique_ptr<Dynamic> &&dynamic) {
  mDynamic.reset(dynamic.release());
}

std::optional<Dynamic> Note::get_dynamic() const {
  if (!mDynamic)
    return {};
  return std::optional<Dynamic>{Dynamic{*mDynamic}};
}

void Note::clear_dynamic() {
  mDynamic.release();
}

bool Note::is_tied() const {
  return mIsTiedToNext;
}

void Note::tie_to_next(
    bool set) { // TODO tied notes - draw this, edit this, ...
  mIsTiedToNext = set;
}

void Note::set_tremolo(
    int tremolo) { // TODO tremolos - draw this, edit this, ...
  mTremolo = tremolo;
}

void Note::add_node_for_class(xml::Node &node) {
  auto note = node.append_child("note");
  this->TimedNotation::add_node_for_class(*note);

  mPitch.add_node_for_class(*note);
  for (const auto &span : mSpanStartSet)
    note->add_node("span-start", enums::to_string(span));
  for (const auto &span : mSpanEndSet)
    note->add_node("span-end", enums::to_string(span));
  if (mHasCourtesyAccidental)
    note->add_node("courtesy-accidental", mHasCourtesyAccidental);
  if (mDynamic)
    mDynamic->add_node_for_class(*note);
  if (mIsTiedToNext)
    note->add_node("tied", mIsTiedToNext);
  if (mTremolo != 0)
    note->add_node("tremolo", mTremolo);
};

std::unique_ptr<Note> Note::get_class_for_node(const xml::Node &node) {
  auto rval = std::make_unique<Note>();
  TimedNotation::decorate_timed_notation(rval.get(), node);

  node.once("pitch", [&](const xml::Node &n) {
    auto pitch = Pitch::get_class_for_node(n);
    rval->pitch(*pitch);
  });
  node.many("span-start", [&](const xml::Node &n) {
    auto span = n.get_value<Span>();
    rval->add_span_start(span);
  });
  node.many("span-end", [&](const xml::Node &n) {
    auto span = n.get_value<Span>();
    rval->add_span_end(span);
  });
  node.once("dynamic", [&](const xml::Node &n) {
    auto dynamic = Dynamic::get_class_for_node(n);
    rval->set_dynamic(std::move(dynamic));
  });
  node.once("tied", [&](const xml::Node &n) {
    auto isTied = n.get_value<bool>();
    rval->tie_to_next(isTied);
  });
  node.once("tremolo",
      [&](const xml::Node &n) { rval->set_tremolo(n.get_value<int>()); });
  node.once("courtesy-accidental", [&](const xml::Node &n) {
    auto hasCourtesyAccidental = n.get_value<bool>();
    rval->set_courtesy_accidental(hasCourtesyAccidental);
  });
  return rval;
};

std::string Note::get_xml_name() {
  return static_xml_name();
}

}; // namespace notation
