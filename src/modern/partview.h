#pragma once

#include "modern.h" // for render::Drawable

#include <unordered_map> // for std::unordered_map
#include <vector>        // for std::vector

namespace notation {
class Part;
enum class Span;
} // namespace notation

namespace modern {

class MovementView;

class PartView : public Modern {
public:
  explicit PartView(
      MovementView &parent, notation::Part &part, int firstBar = 1);
  geom::Rect<> draw(Info &) override;

  void push_span(const notation::Span &span, const geom::Point<> &start);
  geom::Point<> pop_span(const notation::Span &span);

  notation::Part &get_part() const;

private:
  notation::Part *mpPart;
  std::unordered_map<notation::Span, std::vector<geom::Point<>>> mSpans{};
  int mFirstBar;
};

} // namespace modern
