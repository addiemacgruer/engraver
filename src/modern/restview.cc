#include "restview.h"

#include "interface/layoutmanager.h"
#include "notation/rest.h"
#include "render/renderer.h"
#include "util/logging.h"
#include "util/maths.h"

namespace modern {

RestView::RestView(const notation::Rest &rest) : mpRest{&rest} {}

namespace {

geom::Rect<> draw_cached_return_size(
    Info &i, const notation::Notation *lrest, const std::string &cachename) {
  auto &r = i.r();
  auto &lm = i.lm();
  auto rval = r.draw_cached(cachename, lm);
  lm.add_to_notation_list(*lrest, rval);
  return rval;
}
} // namespace

geom::Rect<> RestView::draw(Info &i) {
  util::debug("Rendering rest");
  auto lrest = mpRest;
  auto &r = i.r();
  auto &lm = i.lm();
  auto duration = lrest->get_duration();
  switch (duration.get_base_duration()) {
  case 1: // semibreve rest
    return draw_cached_return_size(i, lrest, "semibreve-rest");
  case 2: // minim rest
    return draw_cached_return_size(i, lrest, "minim-rest");
  case 4: // crotchet rest
    return draw_cached_return_size(i, lrest, "crotchet-rest");
  case 8: // quaver rest
    return draw_cached_return_size(i, lrest, "quaver-rest");
  case 16: // semiquaver rest
    return draw_cached_return_size(i, lrest, "semiquaver-rest");
  default:
    auto feathers =
        util::get_power_of_two(lrest->get_duration().get_base_duration() / 4);
    geom::Rect<> rval = {r.xy(), r.xy()};
    rval &= r.draw_line({r.x(), r.y() + 2.5}, {r.x() + 1, r.y() - 0.5});
    for (decltype(feathers) j = 0; j < feathers; j++) {
      auto sx = r.x() + 1 - j / 3.0;
      auto sy = r.y() - 0.5 + j / 1.0;
      rval &= r.draw_line({sx, sy}, {sx - 1, sy});
      r.draw_ellipse({sx - 1, sy - 0.25}, 0.5, 0.5, true);
    }
    lm.add_to_notation_list(*lrest, rval);
    return rval;
  }
}

template <>
std::unique_ptr<Modern> make_drawable<notation::Rest>(
    const notation::Rest &type) {
  return std::make_unique<modern::RestView>(type);
}

} // namespace modern
