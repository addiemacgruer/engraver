#include "accidental.h"

#include "global.h"
#include "notation/accidental.h"
#include "render/renderer.h"
#include "util/logging.h"
#include "util/string.h"

#include <iostream>

namespace modern {

AccidentalView::AccidentalView(notation::Accidental accidental)
    : mAccidental{accidental} {}

geom::Rect<> AccidentalView::draw(Info &i) {
  auto rval = geom::Rect<>{i.r().xy(), {i.r().x() + 1, i.r().y()}};

  switch (mAccidental) {
  case notation::Accidental::Sharp:
    return i.r().draw_cached("sharp", i.lm());
  case notation::Accidental::Natural:
    return i.r().draw_cached("natural", i.lm());
  case notation::Accidental::Flat:
    return i.r().draw_cached("flat", i.lm());
  case notation::Accidental::DoubleSharp:
    return i.r().draw_cached("doublesharp", i.lm());
  case notation::Accidental::FlatFlat:
    return i.r().draw_cached("flatflat", i.lm());
  default:
    util::error("Unhandled accidental:", mAccidental);
  }
  return rval;
}

template <>
std::unique_ptr<Modern> make_drawable<notation::Accidental>(
    const notation::Accidental &type) {
  return std::make_unique<modern::AccidentalView>(type);
}

} // namespace modern
