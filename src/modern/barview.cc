#include "barview.h"

#include "barendview.h"
#include "clefview.h"
#include "geom/ratio.h"
#include "interface/barlayout.h"
#include "interface/layoutmanager.h"
#include "keyview.h"
#include "notation/bar.h"
#include "notation/clef.h"
#include "notation/key.h"
#include "notation/movement.h"
#include "notation/note.h"
#include "notation/part.h"
#include "notation/rest.h"
#include "notation/timesignature.h"
#include "notation/tuplet.h"
#include "notation/volta.h" // for notation::Volta
#include "noteview.h"
#include "partview.h"
#include "render/renderer.h"
#include "timesigview.h"
#include "tuplet.h" // for modern::Tuplet
#include "types.h"
#include "util/logging.h"
#include "util/string.h"

#include <memory>

namespace modern {

BarView::~BarView() = default;

BarView::BarView(PartView &parent, notation::Bar &bar)
    : mpParent{&parent},
      mpBar{&bar},
      mTupletStack{},
      mpEffectiveKey{std::make_unique<EffectiveKey>(
          bar.get_part().get_key_at_bar(bar.get_bar_number()))} {}

// TODO make volta marks selectable
static void drawVolta(const notation::Volta &volta, render::Renderer &r) {
  auto number = volta.get_value();
  r.set_font(1);
  r.draw_text(util::concat(' ', number), {r.x(), r.y() - 4});
  r.draw_line({r.x(), r.y() - 4}, {r.x(), r.y() - 3});
  r.draw_line({r.x(), r.y() - 4}, {r.x() + 4, r.y() - 4});
}

namespace {
template <class T>
geom::Rect<> draw_notation(Info &i, geom::Rect<> drawRect, const T &notation) {
  auto drawable = make_drawable(notation);
  auto size = drawable->draw(i);
  drawRect &= size;
  if (size.width() > 0) {
    i.r().add_x(size.width());
    i.r().add_x(1);
  }
  return drawRect;
}

geom::Rect<> draw_clef(Info &i, const notation::Part &part,
    geom::Rect<> drawRect, size_t barNumber) {
  auto &startClef = part.get_clef_at_bar(barNumber);
  return draw_notation(i, drawRect, startClef);
}

geom::Rect<> draw_time_signature(Info &i, notation::Movement &movement,
    geom::Rect<> barSize, size_t barNumber) {
  auto &timeSignature = movement.get_bar_time_signature(barNumber);
  return draw_notation(i, barSize, timeSignature);
}

geom::Rect<> draw_key(Info &i, const notation::Part &part, geom::Rect<> barSize,
    size_t barNumber) {
  auto keySignature = part.get_key_at_bar(barNumber);
  auto &clef = part.get_clef_at_bar(barNumber);
  auto ksDrawable = KeyView{keySignature, clef};
  auto ksSize = ksDrawable.draw(i);
  if (ksSize.width() > 0) {
    barSize = barSize & ksSize;
    i.r().add_x(1);
  }
  return barSize;
}

render::PenStyle get_style_for_status(const notation::BarStatus &barStatus) {
  switch (barStatus) {
  case notation::BarStatus::Empty:
    return {{0, 0, 255}, 0.5};
  case notation::BarStatus::TooEmpty:
    return {{0, 255, 0}, 0.5};
  case notation::BarStatus::TooFull:
    return {{255, 0, 0}, 0.5};
  case notation::BarStatus::OK:
    return {{0, 0, 0}, 0.5};
  }
  util::error("Unknown barstatus: ", barStatus);
  return {{0, 0, 255}, 0.5};
}

void draw_bar_colour_status(render::Renderer &r, const geom::Rect<> &barSize,
    const notation::BarStatus &barStatus) {
  auto style = get_style_for_status(barStatus);
  auto pen = r.push_pen(style);
  r.draw_rect({barSize.lower_left().x(), r.y() - 2},
      {barSize.upper_right().x(), r.y() + 2});
}

void draw_ledgers(render::Renderer &r, int offset) {
  for (int i = 6; i <= offset; i += 2) // ledgers below
    r.draw_line(
        {r.x(), r.y() + i / 2.0}, {r.x() + NoteView::fatness, r.y() + i / 2.0});

  for (int i = -6; i >= offset; i -= 2) // ledgers above
    r.draw_line(
        {r.x(), r.y() + i / 2.0}, {r.x() + NoteView::fatness, r.y() + i / 2.0});
};

geom::Rect<> draw_note(Info &i, const notation::Notation *no,
    const notation::Clef &clef, geom::Rect<> drawSize, Modern &drawable) {
  auto note = dynamic_cast<const notation::Note *>(no);
  auto offset = clef.get_central_note().get_ledger_number() -
                note->get_pitch().get_ledger_number();

  i.r().push_point({i.r().x(), i.r().y() + offset / 2.0});
  drawSize = drawable.draw(i);
  i.r().pop_point();

  draw_ledgers(i.r(), offset);
  return drawSize;
};

geom::Rect<> draw_empty_bar(
    Info &i, BarView &barView, notation::Movement &movement, size_t barNumber) {
  auto &timesignature = movement.get_bar_time_signature(barNumber);
  auto wholebarrest = notation::Rest{timesignature};
  auto drawable = modern::drawable_for_notation_type(barView, wholebarrest);
  return drawable->draw(i);
};

void draw_staff_lines(render::Renderer &r, geom::Rect<> &barSize) {
  for (int y = -1; y <= 1; y++)
    r.draw_line({barSize.lower_left().x(), r.y() + y},
        {barSize.upper_right().x(), r.y() + y});
};

} // namespace

geom::Rect<> BarView::draw_bar_start(Info &i, const notation::Bar &lbar) {
  // draw bar start
  auto barStart = BarEndView(lbar.get_bar_start_style(), Direction::Forwards);
  auto barSize = barStart.draw(i);
  i.r().add_x(barSize.width() + 1);
  return barSize;
}

// TODO this method is much too long
geom::Rect<> BarView::draw(Info &i) {
  // draw volta
  if (mpBar->get_volta().get_value() != 0) {
    drawVolta(mpBar->get_volta(), i.r());
  }

  auto barStartX = i.r().x();

  // get aspects of the bar
  auto &part = mpBar->get_part();
  auto barNumber = part.get_bar_number(*mpBar);
  auto clef = part.get_clef_at_bar(barNumber);
  auto &movement = part.get_movement();

  auto barSize = draw_bar_start(i, *mpBar);

  // clef change needed?
  if (barNumber == 0 || part.does_clef_change_at_bar(barNumber))
    barSize = draw_clef(i, part, barSize, barNumber);

  // key needed?
  if (barNumber == 0 || movement.does_key_change_at_bar(barNumber))
    barSize = draw_key(i, part, barSize, barNumber);

  // time signature needed?
  if (barNumber == 0 || movement.does_time_signature_change_at_bar(barNumber))
    barSize = draw_time_signature(i, movement, barSize, barNumber);

  // draw notation
  geom::Ratio timeSoFar = geom::Ratio{};
  auto barLayout = i.lm().get_bar_layout(barNumber);
  if (mpBar->is_rest_bar()) {
    // no notation - draw a whole-bar rest, which looks like a semibreve
    barSize &= draw_empty_bar(i, *this, movement, barNumber);
  }

  auto tupletStack = notation::TupletStack{};

  for (auto &notation : *mpBar) {
    // these notation need special handling
    switch (notation.get_notation_type()) {
    case notation::NotationType::Clef: {
      // update the clef that we're using
      clef = dynamic_cast<notation::Clef &>(notation);
    } break;
    case notation::NotationType::Tuplet: {
      auto tuplet = dynamic_cast<notation::Tuplet *>(&notation);
      tupletStack.add_tuplet(tuplet);
    } break;
    default:
      break;
    }

    auto drawable = modern::drawable_for_notation_type(*this, notation);
    geom::Rect<> drawSize;

    if (notation.get_notation_length().numerator() != 0) {
      // this notation has no length in time (eg. clef change), so just place it
      auto position = barLayout->get_position_for_time(timeSoFar);
      if (barStartX + position > i.r().x())
        i.r().set_x(barStartX + position);
    }

    // notes need to be offset, so are handled differently
    if (notation.get_notation_type() == notation::NotationType::Note) {
      drawSize = draw_note(i, &notation, clef, drawSize, *drawable);
    } else {
      drawSize = drawable->draw(i);
    }

    barSize &= drawSize;
    i.r().add_x(drawSize.width());

    timeSoFar += tupletStack.get_ratio() * notation.get_notation_length();
    barLayout->set_position_for_time(timeSoFar, barSize.width());
  }

  auto currentWidth = i.r().x() - barStartX;
  barLayout->set_minimum_bar_length(currentWidth);
  auto longestWidth = barLayout->get_bar_length();
  i.r().add_x(longestWidth - currentWidth);

  //  draw the bar end
  i.r().add_x(1);
  auto barEnd = BarEndView(mpBar->get_bar_end_style(), Direction::Backwards);
  barSize &= barEnd.draw(i);

  draw_staff_lines(i.r(), barSize);

  i.lm().add_to_notation_list(get_bar().as_notation(), barSize);

  draw_bar_colour_status(i.r(), barSize, mpBar->get_bar_status());

  return barSize;
}

PartView &BarView::get_part_view() {
  auto rval = mpParent;
  if (!rval)
    throw std::runtime_error{"view/barview::partView"};
  return *rval;
}

notation::Bar &BarView::get_bar() {
  return *mpBar;
}

EffectiveKey &BarView::get_effective_key() {
  return *mpEffectiveKey;
}

} // namespace modern
