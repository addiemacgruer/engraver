#include "barendview.h"

#include "notation/barstyle.h"
#include "render/renderer.h"
#include "util/logging.h"

namespace modern {

BarEndView::BarEndView(
    const notation::BarStyle &barEndType, const Direction forwards)
    : mBarEndType{barEndType}, mIsForwards{forwards == Direction::Forwards} {}

geom::Rect<> BarEndView::draw(Info &i) {
  auto x = i.r().x();
  auto y = i.r().y();

  switch (mBarEndType) {
  case notation::BarStyle::RehearsalMark: // TODO add letter to rehearsalmark
  case notation::BarStyle::Single:
    return i.r().draw_line({x, y - 2}, {x, y + 2});
  case notation::BarStyle::Repeat: {
    auto size = geom::Rect<>{i.r().xy(), i.r().xy()};
    if (mIsForwards) {
      size &= i.r().draw_rect({x, y - 2}, {x + 0.25, y + 2}, true);
      size &= i.r().draw_line({x + 0.5, y - 2}, {x + 0.5, y + 2});
      size &= i.r().draw_ellipse({x + 1, y - 0.5}, 0.5, 0.5, true);
      size &= i.r().draw_ellipse({x + 1, y + 0.5}, 0.5, 0.5, true);
    } else {
      size &= i.r().draw_ellipse({x + 0.25, y - 0.5}, 0.5, 0.5, true);
      size &= i.r().draw_ellipse({x + 0.25, y + 0.5}, 0.5, 0.5, true);
      size &= i.r().draw_line({x + 0.75, y - 2}, {x + 0.75, y + 2});
      size &= i.r().draw_rect({x + 1, y - 2}, {x + 1.25, y + 2}, true);
    }
    return size;
  }
  case notation::BarStyle::Double: {
    auto size = geom::Rect<>{i.r().xy(), i.r().xy()};
    size &= i.r().draw_line({x, y - 2}, {x, y + 2});
    size &= i.r().draw_line({x + 0.5, y - 2}, {x + 0.5, y + 2});
    return size;
  }
  case notation::BarStyle::Fine: {
    auto size = geom::Rect<>{i.r().xy(), i.r().xy()};
    if (mIsForwards) {
      size &= i.r().draw_rect({x, y - 2}, {x + 0.25, y + 2}, true);
      size &= i.r().draw_line({x + 0.5, y - 2}, {x + 0.5, y + 2});
    } else {
      size &= i.r().draw_line({x + 0.75, y - 2}, {x + 0.75, y + 2});
      size &= i.r().draw_rect({x + 1, y - 2}, {x + 1.25, y + 2}, true);
    }
    return size;
  }
  default:
    util::error("Failed to draw barline type:", mBarEndType);
    return {{x, y}, {x, y}};
  }
}

} // namespace modern
