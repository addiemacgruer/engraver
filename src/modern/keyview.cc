#include "keyview.h"

#include "accidental.h"
#include "notation/accidental.h"
#include "notation/clef.h"
#include "notation/cleftype.h" // for ClefType
#include "notation/key.h"
#include "render/renderer.h"
#include "util/logging.h"
#include "util/string.h"

#include <iostream>

namespace modern {

namespace {
geom::Rect<> draw_accidentals(Info &i, geom::Rect<double> rval) {
  auto natural = notation::Accidental{notation::Accidental::Natural};
  auto naturalsign = make_drawable(natural);
  auto naturalglyph = naturalsign->draw(i);
  rval &= naturalglyph;
  i.r().add_x(naturalglyph.width());
  return rval;
};

int sharp_restart_for_clef_symbol(const notation::ClefType &cs) {
  switch (cs) {
  case notation::ClefType::G:
    return 5;
  case notation::ClefType::C:
    return 4;
    // TODO check what the restart order is for F clef
  case notation::ClefType::F:
  default:
    return 5;
  }
}

geom::Rect<> draw_sharps(Info &i, size_t accidentals,
    const notation::Clef *lclef, geom::Rect<> rval) {
  auto sharporder = notation::Key::get_sharp_order_list();
  auto sharp = notation::Accidental{notation::Accidental::Sharp};
  auto sharpsign = make_drawable(sharp);

  auto restart = sharp_restart_for_clef_symbol(lclef->get_clef_symbol());

  for (size_t j = 0; j < accidentals; ++j) {
    auto centrenote = lclef->get_central_note().get_sound();
    auto sharpenednote = sharporder.at(j);
    auto offset{0};
    while (centrenote != sharpenednote) {
      offset++;
      if (offset > restart)
        offset -= 7;
      centrenote++;
    }
    i.r().push_point(i.r().xy());
    i.r().add_y(-static_cast<double>(offset) / 2.0);
    auto sharpsize = sharpsign->draw(i);
    rval &= sharpsize;
    i.r().pop_point();
    i.r().add_x(sharpsize.width());
  }
  return rval;
};

geom::Rect<> draw_flats(Info &i, size_t accidentals,
    const notation::Clef *lclef, geom::Rect<> rval) {
  auto flatorder = notation::Key::get_flat_order_list();
  auto flat = notation::Accidental{notation::Accidental::Flat};
  auto sharpsign = make_drawable(flat);

  for (size_t j = 0; j < accidentals; ++j) {
    auto centrenote = lclef->get_central_note().get_sound();
    auto sharpenednote = flatorder.at(j);
    auto offset{0};
    while (centrenote != sharpenednote) {
      offset++;
      if (offset > 3)
        offset -= 7;
      centrenote++;
    }
    i.r().push_point(i.r().xy());
    i.r().add_y(-static_cast<double>(offset) / 2.0);
    auto sharpsize = sharpsign->draw(i);
    rval &= sharpsize;
    i.r().pop_point();
    i.r().add_x(sharpsize.width());
  }
  return rval;
};

} // namespace

KeyView::KeyView(const notation::Key &key, const notation::Clef &clef)
    : mpKey{&key}, mpClef{&clef} {}

EffectiveKey::EffectiveKey(const notation::Key &key) {
  auto natural = notation::Accidental::Natural;
  for (size_t i = 0; i < 7; i++) {
    mpEffectiveAccidentals[i] = natural;
  }
  auto accidentals = key.accidentals();
  if (accidentals > 0) { // sharps
    auto sharp = notation::Accidental::Sharp;
    for (auto &sound : key.get_sharp_order_list()) {
      auto intsound = enums::ordinal(sound);
      mpEffectiveAccidentals.at(intsound) = sharp;
      --accidentals;
      if (accidentals == 0)
        return;
    }
  } else if (accidentals < 0) { // flats
    auto flat = notation::Accidental::Flat;
    for (auto &sound : key.get_flat_order_list()) {
      auto intsound = enums::ordinal(sound);
      mpEffectiveAccidentals.at(intsound) = flat;
      ++accidentals;
      if (accidentals == 0)
        return;
    }
  } // else C-major / A-minor
}

void EffectiveKey::set_accidental_for_sound(
    const notation::Sound &sound, const notation::Accidental &accidental) {
  auto intsound = enums::ordinal(sound);
  mpEffectiveAccidentals.at(intsound) = accidental;
}

notation::Accidental EffectiveKey::get_accidental_for_sound(
    const notation::Sound &sound) const {
  auto intsound = enums::ordinal(sound);
  auto rval = mpEffectiveAccidentals.at(intsound);
  return rval;
}

geom::Rect<> KeyView::draw(Info &i) {
  auto rval = geom::Rect<>{i.r().xy(), i.r().xy()};
  auto accidentals = mpKey->accidentals();

  if (accidentals == 0) { // no key
    rval = draw_accidentals(i, rval);
  } else if (accidentals > 0) { // sharps
    auto abs_accidentals = static_cast<size_t>(accidentals);
    rval = draw_sharps(i, abs_accidentals, mpClef, rval);
  } else if (accidentals < 0) { // flats
    auto abs_accidentals = static_cast<size_t>(-accidentals);
    rval = draw_flats(i, abs_accidentals, mpClef, rval);
  }
  return rval;
}

} // namespace modern
