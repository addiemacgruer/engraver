#pragma once

#include "modern.h"
#include <memory> // for std::unique_ptr

namespace notation {
class Notation;
}
namespace render {
class Drawable;
}

/** \namespace modern \brief Classes and functions for drawing modern classical
 * notation on screen */
namespace modern {

class BarView;

std::unique_ptr<Modern> drawable_for_notation_type(
    BarView &parent, const notation::Notation &type);
} // namespace modern
