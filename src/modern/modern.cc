#include "modern.h"

namespace modern {

ErrorMessage::ErrorMessage(const std::string &m, const geom::Rect<> &w)
    : message{m}, where{w} {}

namespace { // size of error boxes
constexpr double E = 0.5;
}

ErrorMessage::ErrorMessage(const std::string &m, const geom::Point<> &w)
    : message{m}, where{{w.x() - E, w.y() - E}, {w.x() + E, w.y() + E}} {}

geom::Rect<> ErrorMessage::get_location() const {
  return where;
}

std::string ErrorMessage::get_message() const {
  return message;
}

Modern::~Modern() = default;

Info::Info(render::Renderer *r, interface::LayoutManager *lm)
    : renderer{r}, layoutmanager{lm} {}

render::Renderer &Info::r() const {
  return *renderer;
}

interface::LayoutManager &Info::lm() const {
  return *layoutmanager;
}

void Info::add_error(const ErrorMessage &e) {
  mErrorMessages.push_back(e);
}

Info::ErrorMessages Info::get_errors() {
  return mErrorMessages;
}

} // namespace modern
