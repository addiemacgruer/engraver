#pragma once

#include "modern.h"
#include <memory>

namespace notation {
class Tuplet;
}

namespace modern {
class TupletView : public Modern {
public:
  explicit TupletView(const notation::Tuplet &tuplet);

  geom::Rect<> draw(Info &) override;

private:
  const notation::Tuplet *mpTuplet;
};

} // namespace modern
