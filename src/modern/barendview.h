#pragma once

#include "modern.h"            // for render::Modern
#include "modern/direction.h"  // for Direction
#include "notation/barstyle.h" // for notation::BarStyle

namespace modern {

class BarEndView : public Modern {
public:
  explicit BarEndView(const notation::BarStyle &barEndType,
      const Direction forwards = Direction::Forwards);

  geom::Rect<> draw(Info &) override;

private:
  notation::BarStyle mBarEndType;
  bool mIsForwards;
};

} // namespace modern
