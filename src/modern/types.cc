#include "types.h"

#include "barview.h"
#include "clefview.h"
#include "notation/bar.h"
#include "notation/clef.h"
#include "notation/notation.h"
#include "notation/note.h"
#include "notation/rest.h"
#include "notation/timesignature.h"
#include "notation/tuplet.h"
#include "noteview.h"
#include "render/drawable.h"
#include "restview.h"
#include "timesigview.h"
#include "tuplet.h"
#include "util/logging.h"

#include <stdexcept>

namespace {
template <class Cast, class View>
std::unique_ptr<View> make_ref(const notation::Notation &nt) {
  auto &casted = dynamic_cast<const Cast &>(nt);
  return std::make_unique<View>(casted);
}
} // namespace

namespace modern {
std::unique_ptr<Modern> drawable_for_notation_type(
    BarView &parent, const notation::Notation &type) {
  switch (type.get_notation_type()) {
  case notation::NotationType::Note: {
    auto &note = dynamic_cast<const notation::Note &>(type);
    return std::make_unique<NoteView>(parent, note);
  }
  case notation::NotationType::Rest:
    return make_ref<notation::Rest, RestView>(type);
  case notation::NotationType::Clef:
    return make_ref<notation::Clef, ClefView>(type);
  case notation::NotationType::TimeSignature:
    return make_ref<notation::TimeSignature, TimeSigView>(type);
  case notation::NotationType::Tuplet:
    return make_ref<notation::Tuplet, TupletView>(type);
  default:
    util::error("Unknown notationtype:", type.get_notation_type());
    throw std::runtime_error{"view/types::drawableForNotationNotationType"};
  }
}

} // namespace modern
