#pragma once

#include "geom/rect.h"                // for geom::Rect<>
#include "interface/insertionpoint.h" // for interface::InsertionPoint
#include "interface/layoutmanager.h"  // for interface::LayoutManager
#include "render/renderer.h"          // for Renderer
#include <memory>                     // for std::unique_ptr

namespace modern {

struct ErrorMessage {
public:
  ErrorMessage(const std::string &message, const geom::Rect<> &where);
  ErrorMessage(const std::string &message, const geom::Point<> &where);

  geom::Rect<> get_location() const;
  std::string get_message() const;

private:
  std::string message;
  geom::Rect<> where;
};

struct Info {
public:
  using ErrorMessages = std::vector<ErrorMessage>;

  Info(render::Renderer *r, interface::LayoutManager *);
  Info(const Info &) = delete;
  render::Renderer &r() const;
  interface::LayoutManager &lm() const;
  void add_error(const ErrorMessage &);
  ErrorMessages get_errors();

private:
  render::Renderer *renderer;
  interface::LayoutManager *layoutmanager;
  ErrorMessages mErrorMessages;
};

class Modern {
public:
  virtual ~Modern();
  virtual geom::Rect<> draw(Info &) = 0;
};

template <class T>
std::unique_ptr<Modern> make_drawable(const T &type);

} // namespace modern
