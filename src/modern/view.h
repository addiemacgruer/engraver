#pragma once

// TODO this include file is not used

#include "geom/rect.h"               // for geom::Rect<>
#include "interface/layoutmanager.h" // for interface::LayoutManager

namespace interface {
class InsertionPoint;
} // namespace interface
namespace notation {
class Movement;
}
namespace render {
class Renderer;
}

namespace modern {

class ModernView {
public:
  ModernView(render::Renderer &renderer, notation::Movement &movement);

  interface::LayoutManager &get_layout_manager() const;

  void draw_insertion_point(interface::InsertionPoint &ip);
  geom::Rect<> draw(geom::Rect<> windowSize) const;

private:
  render::Renderer *mpRender;
  notation::Movement *mpMovement;
  interface::LayoutManager mLayoutManager;
  geom::Rect<> mDrawSize{};
};

} // namespace modern
