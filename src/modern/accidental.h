#pragma once

#include "modern.h"              // for Drawable
#include "notation/accidental.h" // for Accidental

#include <memory>

namespace modern {
class AccidentalView : public Modern {
public:
  explicit AccidentalView(notation::Accidental accidental);
  virtual geom::Rect<> draw(Info &) override;

private:
  notation::Accidental mAccidental;
};

template <>
std::unique_ptr<Modern> make_drawable<notation::Accidental>(
    const notation::Accidental &type);

} // namespace modern
