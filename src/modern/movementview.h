#pragma once

#include "modern.h"

namespace interface {
class LayoutManager;
}
namespace notation {
class Movement;
}

namespace modern {

class MovementView : public Modern {
public:
  explicit MovementView(notation::Movement &movement);
  geom::Rect<> draw(Info &) override;

private:
  notation::Movement *mpMovement;
  //  interface::LayoutManager *mLm;
};

} // namespace modern
