#include "clefview.h"

#include "global.h"
#include "notation/clef.h"
#include "notation/cleftype.h"
#include "render/renderer.h"
#include "render/svgpath.h"
#include "util/logging.h"

namespace modern {

ClefView::ClefView(const notation::Clef &clef) : mpClef{&clef} {}

namespace {

geom::Rect<> draw_clef(render::Renderer &r, interface::LayoutManager &lm,
    const notation::ClefType &clefSymbol) {
  auto x = r.x();
  auto y = r.y();
  switch (clefSymbol) {
  case notation::ClefType::G:
    return r.draw_cached("g-clef", lm);
  case notation::ClefType::F:
    return r.draw_cached("f-clef", lm);
  case notation::ClefType::C:
    return r.draw_cached("c-clef", lm);
  case notation::ClefType::Neutral: {
    geom::Rect<> rval = {r.xy(), r.xy()};
    rval &= r.draw_rect({x - 1, y - 1}, {x - 0.3, y + 2});
    rval &= r.draw_rect({x + 0.3, y - 1}, {x + 1, y + 2});
    return rval;
  }
  default:
    throw std::runtime_error{"view/clefview::draw_clef"};
  }
}

} // namespace

geom::Rect<> ClefView::draw(Info &i) {
  auto &r = i.r();
  r.push_point({r.x(), r.y() - mpClef->get_ledger_offset() / 2.0});
  auto rval = draw_clef(r, i.lm(), mpClef->get_clef_symbol());
  r.pop_point();
  return rval;
}

template <>
std::unique_ptr<Modern> make_drawable<notation::Clef>(
    const notation::Clef &type) {
  return std::make_unique<modern::ClefView>(type);
}

} // namespace modern
