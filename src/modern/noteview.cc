#include "noteview.h"

#include "accidental.h"
#include "barview.h"
#include "global.h"
#include "interface/layoutmanager.h"
#include "keyview.h"
#include "notation/articulations.h"
#include "notation/dynamic.h"
#include "notation/note.h"
#include "notation/part.h" // for notation::Part
#include "notation/span.h" // for notation::Span
#include "partview.h"
#include "render/renderer.h"
#include "util/logging.h"
#include "util/maths.h"

namespace modern {

NoteView::NoteView(BarView &parent, const notation::Note &note)
    : mpParent{&parent}, mpNote{&note} {}

geom::Rect<> NoteView::draw_accidentals(
    Info &i, geom::Rect<> &rect, const notation::Note &lnote) {
  auto needsAccidental{false};
  if (lnote.has_courtesy_accidental())
    needsAccidental = true;

  auto &effectivekey = get_bar_view().get_effective_key();
  if (!needsAccidental) {
    auto sound = lnote.get_pitch().get_sound();
    auto keyAccidental = effectivekey.get_accidental_for_sound(sound);
    needsAccidental = keyAccidental != lnote.get_pitch().get_accidental();
  }

  if (needsAccidental) {
    auto accidental = lnote.get_pitch().get_accidental();
    auto accidentalview = make_drawable(accidental);

    auto accidentalsize = accidentalview->draw(i);
    rect &= accidentalsize;
    i.r().add_x(accidentalsize.width());
  }

  effectivekey.set_accidental_for_sound(
      lnote.get_pitch().get_sound(), lnote.get_pitch().get_accidental());
  return rect;
}

namespace {
geom::Rect<> draw_note_as_shapes(render::Renderer &r,
    interface::LayoutManager &, bool hasStem, bool isFilled, int tails) {
  auto &fatness = NoteView::fatness;
  auto &stemHeight = NoteView::stemHeight;
  auto rect =
      r.draw_ellipse({r.x() + fatness / 2, r.y()}, fatness, 1, isFilled);

  if (hasStem) {
    rect &= r.draw_line(
        {r.x() + fatness, r.y()}, {r.x() + fatness, r.y() - stemHeight});
    for (int i = 0; i < tails; i++) {
      rect &= r.draw_line({r.x() + fatness, r.y() - stemHeight + i / 2.0},
          {r.x() + fatness + 0.5, r.y() - stemHeight + i / 2.0 + 0.5});
    }
  } else {
    rect &= r.draw_line({r.x(), r.y()}, {r.x(), r.y() + stemHeight});
    for (int i = 0; i < tails; i++) {
      rect &= r.draw_line({r.x(), r.y() + stemHeight - i / 2.0},
          {r.x() + 0.5, r.y() + stemHeight - i / 2.0 - 0.5});
    }
    rect &= {r.xy(), {r.x() + fatness + 0.5, r.y()}};
  }
  return rect;
}

geom::Rect<> draw_note(
    render::Renderer &r, interface::LayoutManager &lm, int baseDuration) {
  // draw note shapes
  switch (baseDuration) {
  case 1: // semibreve
    return r.draw_cached("semibreve", lm);
  case 2: // minim
    return r.draw_cached("minim", lm);
  case 4: // crotchet
    return r.draw_cached("crotchet", lm);
  case 8:
    return r.draw_cached("quaver", lm);
  case 16:
    return r.draw_cached("semiquaver", lm);
  default: // crotchet, or semi of some kind
    int tails = util::get_power_of_two(baseDuration / 4);
    return draw_note_as_shapes(r, lm, true, true, tails);
  }
}

geom::Rect<> draw_dots(render::Renderer &r, geom::Rect<> rval, int dots) {
  auto &fatness = NoteView::fatness;
  for (int i = 0; i < dots; i++) {
    rval &= r.draw_ellipse(
        {r.x() + fatness + 0.75 + 0.5 * i, r.y()}, 0.5, 0.5, true);
  }
  return rval;
}

geom::Rect<> draw_articulations(render::Renderer &r,
    interface::LayoutManager &lm, geom::Rect<> rval,
    const notation::Note *mpNote) {
  auto &fatness = NoteView::fatness;
  for (auto &articulation : mpNote->get_articulations()) {
    auto noteCentre = r.x() + fatness / 2;
    auto articposY = rval.upper_right().y() + 1;
    switch (articulation) {
    case notation::Articulation::Staccatissimo: {
      rval &= r.draw_line({noteCentre, articposY}, {noteCentre, articposY + 1});
      break;
    }
    case notation::Articulation::Staccato: {
      rval &= r.draw_ellipse({noteCentre, articposY}, 0.5, 0.5);
      break;
    }
    case notation::Articulation::Accent: {
      rval &= r.draw_line(
          {noteCentre - 0.5, articposY}, {noteCentre + 0.5, articposY + 0.5});
      rval &= r.draw_line({noteCentre - 0.5, articposY + 1},
          {noteCentre + 0.5, articposY + 0.5});
      break;
    }
    case notation::Articulation::LeftHandPizzicato: {
      auto y = rval.lower_left().y();
      r.push_point({r.x(), y});
      auto &pizzspec = Global::get_instance().svg_path_for_name("pizzicato");
      rval &= pizzspec.draw(r, lm);
      r.pop_point();
      break;
    }
    case notation::Articulation::UpBow: {
      auto y = rval.lower_left().y();
      r.push_point({r.x(), y});
      rval &= r.draw_cached("upbow", lm);
      r.pop_point();
      break;
    }
    case notation::Articulation::Fermata: {
      auto y = rval.lower_left().y();
      r.push_point({r.x(), y});
      rval &= r.draw_cached("fermata", lm);
      r.pop_point();
      break;
    }
    default:
      util::error("Didn't draw articulation for note:",
          articulation); // TODO implement more articulations
    }
  }
  return rval;
}

} // namespace

void NoteView::draw_volume_change(
    Info &i, const notation::Span &span, double startSpread, double endSpread) {
  auto &r = i.r();
  auto &lm = i.lm();

  auto start = get_bar_view().get_part_view().pop_span(span);
  auto &part = get_bar_view().get_part_view().get_part();
  auto &movement = part.get_movement();
  auto partNumber = size_t{};
  while (true) {
    if (&movement.at(partNumber) == &part)
      break;
    ++partNumber;
  }

  auto centreline = lm.offset_for(partNumber);
  start = {start.x(), centreline + 3};

  auto end = geom::Point<>{r.x() + fatness / 2, r.y()};
  end = {end.x(), centreline + 3};
  r.draw_line(
      {start.x(), start.y() - startSpread}, {end.x(), end.y() - endSpread});
  r.draw_line(
      {start.x(), start.y() + startSpread}, {end.x(), end.y() + endSpread});
}

geom::Rect<> NoteView::draw(Info &i) {
  util::debug("Rendering note");
  auto &r = i.r();
  auto &lm = i.lm();

  auto rval = geom::Rect<>{r.xy(), r.xy()};
  rval = draw_accidentals(i, rval, *mpNote);
  rval &= draw_note(r, lm, mpNote->get_duration().get_base_duration());
  rval &= draw_dots(r, rval, mpNote->get_duration().get_dots());
  rval &= draw_articulations(r, lm, rval, mpNote);

  // push start spans
  for (const auto &span : mpNote->get_start_spans()) {
    get_bar_view().get_part_view().push_span(
        span, {r.x() + fatness / 2, r.y()});
  }
  // draw end spans
  for (const auto &span : mpNote->get_end_spans()) {
    switch (span) {
    case notation::Span::Slur: {
      auto start = get_bar_view().get_part_view().pop_span(span);
      start = {start.x(), start.y() + 1};

      auto end = geom::Point<>{r.x() + fatness / 2, r.y()};
      end = {end.x(), end.y() + 1};

      auto lowest = std::max(start.y(), end.y()) + 1;
      auto halfway = (start.x() + end.x()) / 2.0;

      r.draw_bl(start.x(), start.y(), halfway - start.x(), lowest - start.y());
      r.draw_br(halfway, end.y(), halfway - start.x(), lowest - end.y());
      break;
    }
    case notation::Span::Crescendo:
      draw_volume_change(i, span, 0, 0.5);
      break;
    case notation::Span::Diminuendo:
      draw_volume_change(i, span, 0.5, 0);
      break;
    default:
      util::error("Didn't draw endspans for note:", span);
      break;
    }
  }

  if (auto dynamic = mpNote->get_dynamic(); dynamic) {
    auto text = dynamic.value().get_text();
    r.draw_text(text, {r.x(), r.y() + 1});
  }

  if (mpNote->is_tied()) {
    constexpr double tiewidth{0.75};
    constexpr double tiedown{0.75};
    r.draw_bl(r.x() + 0.5, r.y() + tiedown, tiewidth, 0.5);
    r.draw_br(r.x() + 0.5 + tiewidth, r.y() + tiedown, tiewidth, 0.5);
  }
  lm.add_to_notation_list(*mpNote, rval);
  return rval;
}

BarView &NoteView::get_bar_view() {
  return *mpParent;
}

} // namespace modern
