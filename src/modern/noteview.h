#pragma once

#include "modern.h"         // for render::Drawable
#include "modern/barview.h" // for view::BarView

namespace notation {
class Note;
enum class Span;
} // namespace notation

namespace modern {

class NoteView : public Modern {
public:
  NoteView(BarView &parent, const notation::Note &note);
  virtual geom::Rect<> draw(Info &i) override;

  BarView &get_bar_view();

  static constexpr double fatness{1.5};
  static constexpr int stemHeight{3};

private:
  void draw_volume_change(
      Info &, const notation::Span &span, double startSpread, double endSpread);
  geom::Rect<> draw_accidentals(
      Info &, geom::Rect<> &rect, const notation::Note &lnote);
  BarView *mpParent;
  const notation::Note *mpNote;
};

} // namespace modern
