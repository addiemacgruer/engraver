#pragma once

#include "modern.h"
#include "notation/accidental.h" // for Accidental

#include <memory>

namespace notation {
class Key;
enum class Sound;
class Clef;
} // namespace notation

namespace modern {
class KeyView : public Modern {
public:
  explicit KeyView(const notation::Key &key, const notation::Clef &clef);
  virtual geom::Rect<> draw(Info &) override;

private:
  const notation::Key *mpKey;
  const notation::Clef *mpClef;
};

class EffectiveKey {
public:
  explicit EffectiveKey(const notation::Key &key);

  EffectiveKey(const EffectiveKey &other) = delete;
  EffectiveKey &operator=(const EffectiveKey &other) = delete;

  void set_accidental_for_sound(
      const notation::Sound &sound, const notation::Accidental &accidental);
  notation::Accidental get_accidental_for_sound(
      const notation::Sound &sound) const;

private:
  std::array<notation::Accidental, 7> mpEffectiveAccidentals{};
};

} // namespace modern
