#include "timesigview.h"

#include "notation/timesignature.h"
#include "render/renderer.h"
#include "util/logging.h"
#include "util/string.h"

#include <iostream>

namespace modern {

TimeSigView::TimeSigView(const notation::TimeSignature &ts) : mpTs{&ts} {}

geom::Rect<> TimeSigView::draw(Info &i) {
  auto &r = i.r();
  auto rval = geom::Rect<>{r.xy(), r.xy()};
  r.set_font(2.0);
  rval &= r.draw_text(util::concat(mpTs->get_beats()), {r.x(), r.y() - 2.5});
  rval &=
      r.draw_text(util::concat(mpTs->get_beat_lengths()), {r.x(), r.y() - 0.5});
  return {{r.x(), r.y() - 2}, {r.x() + 2, r.y() + 2}};
}

template <>
std::unique_ptr<Modern> make_drawable<notation::TimeSignature>(
    const notation::TimeSignature &type) {
  return std::make_unique<modern::TimeSigView>(type);
}

} // namespace modern
