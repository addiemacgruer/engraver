#pragma once

#include "modern.h"
#include "notation/clef.h"

#include <memory>

namespace modern {
class ClefView : public Modern {
public:
  explicit ClefView(const notation::Clef &clef);
  virtual geom::Rect<> draw(Info &) override;

private:
  const notation::Clef *mpClef;
};

template <>
std::unique_ptr<Modern> make_drawable<notation::Clef>(
    const notation::Clef &type);

} // namespace modern
