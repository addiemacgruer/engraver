#pragma once

#include "modern.h"

namespace notation {
class TimeSignature;
}

namespace modern {
class TimeSigView : public Modern {
public:
  explicit TimeSigView(const notation::TimeSignature &ts);
  virtual geom::Rect<> draw(Info &) override;

private:
  const notation::TimeSignature *mpTs;
};

template <>
std::unique_ptr<Modern> make_drawable<notation::TimeSignature>(
    const notation::TimeSignature &type);

} // namespace modern
