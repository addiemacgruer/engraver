#include "partview.h"

#include "barview.h"
#include "clefview.h"
#include "keyview.h"
#include "movementview.h"
#include "notation/part.h" // for notation::Part
#include "notation/span.h" // for notation::Span
#include "render/renderer.h"
#include "timesigview.h"
#include "util/logging.h"

namespace modern {

PartView::PartView(MovementView &, notation::Part &part, int firstBar)
    : mpPart{&part}, mFirstBar{firstBar} {
  for (auto span : enums::values<notation::Span>()) {
    mSpans.emplace(span, std::vector<geom::Point<>>{});
  }
}

notation::Part &PartView::get_part() const {
  return *mpPart;
}

geom::Rect<> PartView::draw(Info &i) {
  util::debug("Drawing part");
  auto &r = i.r();

  auto drawRect = geom::Rect<>{r.xy(), r.xy()};

  auto fbcount = mFirstBar - 1;

  for (auto &bar : *mpPart) {
    if (fbcount > 0) {
      fbcount--;
      continue;
    }
    auto drawable = modern::BarView{*this, bar};
    auto startpoint = r.xy();
    r.push_point(startpoint);
    auto drawSize = drawable.draw(i);
    r.pop_point();
    r.add_x(drawSize.width());
    drawRect = drawRect & drawSize;
  }

  for (auto span : enums::values<notation::Span>()) {
    if (auto spanlist = mSpans[span]; !spanlist.empty()) {
      for (auto &s : spanlist) {
        i.add_error({"Span not closed", s});
      }
    }
  }
  return drawRect;
}

void PartView::push_span(
    const notation::Span &span, const geom::Point<> &start) {
  mSpans[span].push_back(start);
}

geom::Point<> PartView::pop_span(const notation::Span &span) {
  if (mSpans[span].empty()) {
    util::error("Tried to close more spans than are open! ", span);
    return {};
  }
  auto rval = mSpans[span].back();
  mSpans[span].pop_back();
  return rval;
}

} // namespace modern
