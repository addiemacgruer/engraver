#include "notation/tuplet.h"

#include "render/renderer.h"
#include "tuplet.h"
#include "util/string.h"

namespace modern {

TupletView::TupletView(const notation::Tuplet &tuplet) : mpTuplet{&tuplet} {}

geom::Rect<> TupletView::draw(Info &i) {
  auto &r = i.r();
  auto x = r.x();
  auto y = r.y();
  auto redpen = r.push_pen({{255, 0, 0}, 0.5});
  if (mpTuplet->is_start()) {
    r.draw_line({x - 0.4, y}, {x - 0.4, y + 5});
    r.draw_line({x - 0.4, y + 5}, {x + 0.4, y + 5});
    auto tuplettime = mpTuplet->get_ratio();
    auto desc =
        util::concat(tuplettime.denominator(), ":", tuplettime.numerator());
    r.draw_text(desc, {x + 0.5, y + 3});
  } else {
    r.draw_line({x - 0.6, y}, {x - 0.6, y + 5});
    r.draw_line({x - 0.6, y + 5}, {x - 1.6, y + 5});
  }
  auto rval = geom::Rect{{x, y}, {x, y}};
  i.lm().add_to_notation_list(*mpTuplet, rval);
  return rval;
}

} // namespace modern
