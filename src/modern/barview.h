#pragma once

#include "geom/ratio.h"      // for geom::Ratio
#include "keyview.h"         // for modern::EffectiveKey
#include "notation/bar.h"    // for notation::Bar
#include "partview.h"        // for modern::PartView
#include "render/drawable.h" // for render::Drawable

#include <memory>
#include <vector>

namespace notation {
class Tuplet;
}

namespace modern {

class BarView : public Modern {
public:
  BarView(PartView &parent, notation::Bar &bar);
  ~BarView() override;

  virtual geom::Rect<> draw(Info &) override;

  notation::Bar &get_bar();

  PartView &get_part_view();

  EffectiveKey &get_effective_key();

private:
  PartView *mpParent;
  notation::Bar *mpBar;
  std::vector<geom::Ratio> mTupletStack;

  std::unique_ptr<EffectiveKey> mpEffectiveKey{};

  geom::Ratio update_tuplets(notation::Tuplet *tuplet);
  geom::Rect<> draw_bar_start(Info &, const notation::Bar &lbar);
};

} // namespace modern
