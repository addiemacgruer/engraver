#include "movementview.h"

#include "interface/layoutmanager.h"
#include "notation/movement.h"
#include "notation/movementkey.h"
#include "partview.h"
#include "render/layoutrenderer.h"
#include "render/renderer.h"
#include "util/logging.h"

#include <thread>

namespace modern {

MovementView::MovementView(notation::Movement &movement)
    : mpMovement{&movement} {}

geom::Rect<> MovementView::draw(Info &info) {
  util::debug("Starting drawing movement");

  auto &mLm = info.lm();
  auto drawRect = geom::Rect<>{};

  if (mpMovement->parts().empty()) {
    util::info("Nothing to draw");
    return drawRect;
  }

  mLm.check_offsets_count();

  auto infoBarHeight = -1;

  do {
    util::debug("LayoutRendering...");
    mLm.make_clean();

    drawRect = {{0, mLm.offset_for(0)}, {0, mLm.offset_for(0)}};

    size_t j{0};

    std::vector<std::thread> threads{};

    for (auto &part : *mpMovement) {
      util::debug("Preparing thread for:", &part);
      auto thread = std::thread([&, j]() {
        auto lr = render::LayoutRenderer();
        auto partView = PartView{*this, part};
        lr.push_point({0, 0});
        auto threadinfo = modern::Info{&lr, &mLm};
        auto partsize = partView.draw(threadinfo);
        auto top = partsize.lower_left().y();
        auto bottom = partsize.upper_right().y();

        mLm.update_part_height(j, top + infoBarHeight, bottom + infoBarHeight);
        util::debug("  Completed layout thread: ", j);
      });
      threads.push_back(std::move(thread));
      ++j;
    }

    for (auto &thread : threads)
      thread.join();

  } while (mLm.needs_redraw());

  mLm.finalise();

  auto &r = info.r();

  do {
    util::debug("view::Rendering...");
    r.clear_canvas();

    drawRect = {{0, mLm.offset_for(0)}, {0, mLm.offset_for(0)}};

    auto j = size_t{0};

    for (auto &part : *mpMovement) {
      auto offset = mLm.offset_for(j);
      r.push_point({0, offset});

      auto partView = PartView{*this, part};
      auto partsize = partView.draw(info);

      drawRect = drawRect & partsize;
      ++j;
    }
  } while (false);

  auto movementTitle = mpMovement->get(notation::MovementKey::title);
  if (movementTitle.empty())
    movementTitle = "Untitled Movement";
  auto &v = r.get_viewing_rect();
  r.set_font(1);
  r.draw_text(movementTitle,
      {v.upper_left().x(), 0}); // v.upper_left().y() - infoBarHeight});

  util::debug("Finished drawing movement");
  return drawRect;
};

} // namespace modern
