#pragma once

#include "modern.h"

#include <memory>

namespace notation {
class Rest;
}

namespace modern {
class RestView : public Modern {
public:
  explicit RestView(const notation::Rest &rest);

  virtual geom::Rect<> draw(Info &) override;

private:
  const notation::Rest *mpRest;
};

template <>
std::unique_ptr<Modern> make_drawable<notation::Rest>(
    const notation::Rest &type);

} // namespace modern
