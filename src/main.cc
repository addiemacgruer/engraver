#include "engraver/engravererror.h"
#include "global.h"
#include "gui/toolkit.h"
#include "interface/control.h"
#include "interface/engraverwindow.h"
#include "notation/accidental.h"
#include "util/logging.h"
#include "util/string.h"

#include <stdexcept>

int *XSynchronize(int, bool);
#ifdef X_SYNCHRONISE
#include <FL/x.H>
#include <X11/Xlib.h>
namespace {
constexpr bool xSynchronise = true;
} // namespace
#else
/** dummy implementation */
int *XSynchronize(int, bool) {
  static int a = 0;
  return &a;
}
namespace {
constexpr int fl_display{0};
constexpr bool xSynchronise{false};
} // namespace
#endif

namespace {
/** process arguments - return true if a window was opened */
bool process_command_line_arguments(int argc, char **argv) {
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] != '-') { // ie. not an argument
      auto filename = std::string(argv[i]);
      auto lowerfilename = util::lower_case_string(filename);
      if (util::ends_with(lowerfilename, ".grave")) {
        interface::Control::open_engraver(filename);
        return true;
      } else if (util::ends_with(lowerfilename, ".denemo")) {
        interface::Control::open_denemo(filename);
        return true;
      } else if (util::ends_with(lowerfilename, ".musicxml")) {
        interface::Control::open_music_xml(filename);
        return true;
      }
    }
  }
  return false;
}
} // namespace

int main(int argc, char **argv) try {
  auto &globalInstance = Global::get_instance();
  globalInstance.init_with_root_directory(argv[0]);
  gui::initialise_toolkit();
  util::set_log_level(util::LogLevel::Info);

  auto didOpenWindow = process_command_line_arguments(argc, argv);
  if (!didOpenWindow) {
    interface::Control::new_document();
  }

  if constexpr (xSynchronise) {
    util::info("Synchronising XWindow display:", fl_display);
    XSynchronize(fl_display, true);
  }

  gui::run_toolkit();
} catch (std::exception &e) {
  util::error("Crashed due to unhandled exception: ", e.what());
}
