#pragma once

#include "command.h"
#include "interface/insertionpoint.h"

namespace command {

class SetVolta : public Command {
public:
  SetVolta(const interface::InsertionPoint &);
  void execute(interface::Control &) override;

private:
  interface::InsertionPoint mInsertionPoint;
};

} // namespace command
