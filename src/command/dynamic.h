#pragma once

#include "command.h"
#include "interface/control.h"
#include "notation/dynamictype.h"

namespace command {
class Dynamic : public Command {
public:
  Dynamic(const interface::Control &, const notation::DynamicType &dt);
  Dynamic(const interface::Control &);
  ~Dynamic() override;

  void execute(interface::Control &) override;

private:
  bool mClearExisting;
  interface::Control::SelectionList mSelected;
  notation::DynamicType mDT;
};
} // namespace command
