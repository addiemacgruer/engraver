#pragma once

#include "command.h"
#include "interface/insertionpoint.h"

namespace notation {
class TimeSignature;
}

namespace command {

class ChangeTimeSignature : public Command {
public:
  explicit ChangeTimeSignature(const interface::InsertionPoint &insertionPoint);
  void execute(interface::Control &) override;
  ~ChangeTimeSignature() override;

private:
  interface::InsertionPoint mInsertionPoint;
  bool mHavePrompted{false};
  std::unique_ptr<notation::TimeSignature> mTimeSignature{};
  std::unique_ptr<notation::TimeSignature> mOriginalTimeSignature{};

  std::unique_ptr<notation::TimeSignature> get_time_signature(
      const std::string &title, notation::TimeSignature *oldTS);
};

} // namespace command
