#pragma once

#include "command.h"
#include "interface/insertionpoint.h"

namespace notation {
class Clef;
}

namespace command {

class ChangeClef : public Command {
public:
  explicit ChangeClef(const interface::InsertionPoint &insertionPoint);
  void execute(interface::Control &) override;
  ~ChangeClef() override;

private:
  interface::InsertionPoint mInsertionPoint;
  bool mHavePrompted{false};
  std::unique_ptr<notation::Clef> mClef{};
  std::unique_ptr<notation::Clef> mOriginalClef{};

  std::unique_ptr<notation::Clef> get_clef(
      const std::string &title, notation::Clef *);
};

} // namespace command
