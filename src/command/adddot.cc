#include "adddot.h"

#include "util/logging.h"
// #include "notation/notation.h"
#include "notation/note.h"
#include "notation/rest.h"

namespace command {

AddDot::AddDot(const interface::Control &control) : SelectionCommand(control){};

void AddDot::apply_to_note(notation::Note &note) {
  auto baseDuration = note.get_duration().get_base_duration();
  auto dots = note.get_duration().get_dots();
  note.set_duration(notation::Duration{baseDuration, dots + 1});
}

void AddDot::apply_to_rest(notation::Rest &rest) {
  auto baseDuration = rest.get_duration().get_base_duration();
  auto dots = rest.get_duration().get_dots();
  rest.set_duration(notation::Duration{baseDuration, dots + 1});
}
} // namespace command
