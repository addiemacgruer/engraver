#include "tuplet.h"

#include "gui/genericdialogue.h"
#include "interface/control.h"
#include "notation/bar.h"
#include "notation/tuplet.h"
#include "util/string.h"

namespace command {

namespace {
std::unique_ptr<notation::Tuplet> ask_for_tuplet() {
  auto gd = gui::GenericDialogue{"Confirm tuplet size"};
  auto num = int{2};
  auto denom = int{3};
  gd.add_integer("Tuplet beats", //
      [&]() { return util::concat(denom); },
      [&](const std::string &text) { denom = std::atoi(text.c_str()); });
  gd.add_integer("Backing beats", //
      [&]() { return util::concat(num); },
      [&](const std::string &text) { num = std::atoi(text.c_str()); });
  gd.show();
  if (num <= 0 || denom <= 0)
    throw std::runtime_error{"Bad tuplet ratio"};
  auto tupletRatio = geom::Ratio{num, denom};
  return std::make_unique<notation::Tuplet>(tupletRatio);
}
} // namespace

Tuplet::Tuplet(const interface::InsertionPoint &ip, const geom::Ratio ratio)
    : mIP{ip}, mRatio{ratio} {}

Tuplet::Tuplet(const interface::InsertionPoint &ip, TupletType tq)
    : mIP{ip}, mPrompt{tq == TupletType::Ask}, mEnd{tq == TupletType::End} {}

void Tuplet::execute(interface::Control &control) {
  auto tuplet = [this]() {
    if (mPrompt)
      return ask_for_tuplet();
    if (mEnd)
      return std::make_unique<notation::Tuplet>();
    return std::make_unique<notation::Tuplet>(mRatio);
  }();
  auto &bar = mIP.get_bar();
  auto number = mIP.get_notation_number();
  bar.add_notation_at_position(std::move(tuplet), number);
  auto newIP = mIP;
  newIP.move_insertion_point_horizontally(1);
  control.set_insertion_point(newIP);
};

} // namespace command
