#pragma once

namespace interface {
class Control;
}

/** \namespace command
\brief Commands which can be executed in the gui, and which are potentially
undoable.
*/
namespace command {

class Command {
public:
  virtual ~Command() = default;
  virtual void execute(interface::Control &) = 0;
  virtual bool is_undoable();
  virtual void undo(interface::Control &);
};

} // namespace command
