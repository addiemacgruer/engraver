#include "removedot.h"

#include "notation/note.h"
#include "notation/rest.h"

namespace command {

RemoveDot::RemoveDot(const interface::Control &control)
    : SelectionCommand(control){};

void RemoveDot::apply_to_note(notation::Note &note) {
  auto baseDuration = note.get_duration().get_base_duration();
  auto dots = note.get_duration().get_dots();
  dots = dots > 0 ? dots - 1 : 0;
  note.set_duration(notation::Duration{baseDuration, dots});
}

void RemoveDot::apply_to_rest(notation::Rest &rest) {
  auto baseDuration = rest.get_duration().get_base_duration();
  auto dots = rest.get_duration().get_dots();
  dots = dots > 0 ? dots - 1 : 0;
  rest.set_duration(notation::Duration{baseDuration, dots});
}
} // namespace command
