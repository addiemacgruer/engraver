#pragma once

#include "command.h"

#include <cstddef>

namespace command {

class AddPart : public Command {
public:
  AddPart(size_t movement, size_t part);
  void execute(interface::Control &) override;

private:
  size_t mMovement;
  size_t mPart;
};

} // namespace command
