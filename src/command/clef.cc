#include "clef.h"

#include "gui/combobox.h"
#include "gui/genericdialogue.h"
#include "notation/clef.h"
#include "notation/part.h"
#include "util/maths.h"
#include "util/string.h"

namespace command {

ChangeClef::ChangeClef(const interface::InsertionPoint &insertionPoint)
    : mInsertionPoint{insertionPoint} {}

ChangeClef::~ChangeClef() = default;

void ChangeClef::execute(interface::Control &) {
  auto barNo = mInsertionPoint.get_bar_number();
  if (!mHavePrompted) {
    mOriginalClef = std::make_unique<notation::Clef>(
        mInsertionPoint.get_part().get_clef_at_bar(barNo));
    auto prompt = barNo == 0 ? "Change initial clef"
                             : util::concat("Change clef at bar ", barNo);
    mClef = get_clef(prompt, mOriginalClef.get());
    mHavePrompted = true;
  }
  auto clefcopy = std::make_unique<notation::Clef>(mClef.get());
  mInsertionPoint.get_part().add_clef_at_bar(barNo, std::move(clefcopy));
}

std::unique_ptr<notation::Clef> ChangeClef::get_clef(
    const std::string &title, notation::Clef *original) {
  auto gd = gui::GenericDialogue{title.c_str()};
  auto oldClefType = original->get_name();

  auto combobox = gd.add_combobox("Clef type", //
      [&]() { return oldClefType; },
      [&](const std::string &text) { oldClefType = text; });
  for (auto &clefname : notation::Clef::get_clefs())
    combobox->add_option(clefname);

  gd.show();

  return notation::Clef::with_name(oldClefType);
}

} // namespace command
