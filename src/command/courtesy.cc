#include "courtesy.h"

#include "notation/note.h"

namespace command {

Courtesy::Courtesy(const interface::Control &control)
    : SelectionCommand(control){};

void Courtesy::apply_to_note(notation::Note &note) {
  note.set_courtesy_accidental(!note.has_courtesy_accidental());
}

} // namespace command
