#pragma once

#include "notation/span.h" // for Span
#include "selection.h"

namespace command {
class ToggleSpan : public SelectionCommand {
public:
  enum class Position { Start, End };
  ToggleSpan(const interface::Control &, const notation::Span &span,
      Position position);

protected:
  void apply_to_note(notation::Note &) override;

private:
  notation::Span mSpan;
  Position mPosition;
};
} // namespace command
