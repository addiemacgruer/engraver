#include "barstyle.h"

#include "notation/movement.h"

namespace command {

BarStyle::BarStyle(const Position position, const notation::BarStyle barStyle,
    const interface::InsertionPoint &insertionPoint)
    : mPosition{position},
      mBarStyle{barStyle},
      mInsertionPoint{insertionPoint} {}

void BarStyle::execute(interface::Control &) {
  auto barnumber = mInsertionPoint.get_bar_number();
  auto &movement = mInsertionPoint.get_movement();
  switch (mPosition) {
  case Position::Start:
    movement.set_bar_start_style(barnumber, mBarStyle);
    break;
  case Position::End:
    movement.set_bar_end_style(barnumber, mBarStyle);
    break;
  }
};

} // namespace command
