#pragma once

#include "command.h"

#include <interface/insertionpoint.h>

namespace notation {
class Key;
}

namespace command {

class ChangeKeySignature : public Command {
public:
  explicit ChangeKeySignature(const interface::InsertionPoint &insertionPoint);
  void execute(interface::Control &) override;
  ~ChangeKeySignature() override;

private:
  interface::InsertionPoint mInsertionPoint;
  bool mHavePrompted{false};
  std::unique_ptr<notation::Key> mKeySignature{};
  std::unique_ptr<notation::Key> mOriginalKeySignature{};

  std::unique_ptr<notation::Key> get_key_signature(
      const std::string &title, notation::Key *oldKS);
};

} // namespace command
