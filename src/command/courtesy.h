#pragma once

#include "selection.h"

namespace command {
class Courtesy : public SelectionCommand {
public:
  explicit Courtesy(const interface::Control &);

protected:
  void apply_to_note(notation::Note &) override;
};
} // namespace command
