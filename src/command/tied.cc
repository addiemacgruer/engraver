#include "tied.h"

#include "notation/note.h"

namespace command {

Tied::Tied(const interface::Control &control) : SelectionCommand(control){};

void Tied::apply_to_note(notation::Note &note) {
  note.tie_to_next(!note.is_tied());
}

} // namespace command
