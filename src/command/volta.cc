#include "volta.h"

#include "gui/genericdialogue.h"
#include "notation/movement.h"
#include "notation/volta.h"
#include "util/string.h"

namespace command {

SetVolta::SetVolta(const interface::InsertionPoint &ip) : mInsertionPoint{ip} {}

namespace {
size_t get_volta_number(size_t old_volta_number = 0) {
  auto gd = gui::GenericDialogue{"Set volta number"};
  auto rval = static_cast<int>(old_volta_number);
  // TODO can't add more than 2^31 volta marks...
  gd.add_integer("Set volta number (0=off)", //
      [&]() { return util::concat(rval); },  //
      [&](const std::string &text) { rval = std::atoi(text.c_str()); });
  gd.show();
  if (rval < 0)
    rval = 0;
  return static_cast<size_t>(rval);
}
} // namespace

void SetVolta::execute(interface::Control &) {
  auto barnumber = mInsertionPoint.get_bar_number();
  auto &movement = mInsertionPoint.get_movement();
  auto oldVoltaNumber = movement.get_volta(barnumber).get_value();
  auto newVoltaNumber = get_volta_number(oldVoltaNumber);
  movement.set_volta(barnumber, notation::Volta{newVoltaNumber});
}

} // namespace command
