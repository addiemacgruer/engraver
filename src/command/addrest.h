#pragma once

#include "addnote.h"

namespace notation {
class Notation;
}

namespace command {

class AddRest : public AddNote {
public:
  AddRest(interface::Control &control, int noteLength);

protected:
  std::unique_ptr<notation::Notation> get_notation() override;
};

} // namespace command
