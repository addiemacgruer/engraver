#pragma once

#include "command.h"
#include "geom/ratio.h"
#include "interface/insertionpoint.h"

namespace command {

enum class TupletType { Ask, End };

class Tuplet : public Command {
public:
  /** start a tuplet with a given ratio */
  Tuplet(const interface::InsertionPoint &ip, const geom::Ratio ratio);
  Tuplet(const interface::InsertionPoint &ip, TupletType tq = TupletType::End);
  void execute(interface::Control &) override;

private:
  interface::InsertionPoint mIP;
  bool mPrompt{false};
  bool mEnd{false};
  geom::Ratio mRatio{};
};

} // namespace command
