#pragma once

#include "command.h"

#include <vector>

namespace notation {
class Notation;
class Note;
class Rest;
} // namespace notation
namespace interface {
class Control;
}

namespace command {

class SelectionCommand : public Command {
public:
  explicit SelectionCommand(const interface::Control &);
  void execute(interface::Control &) override;

protected:
  virtual void apply_to_note(notation::Note &);
  virtual void apply_to_rest(notation::Rest &);
  virtual void unapply_to_note(notation::Note &);
  virtual void unapply_to_rest(notation::Rest &);

private:
  std::vector<notation::Notation *> mSelection;
};

} // namespace command
