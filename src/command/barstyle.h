#pragma once

#include "command.h"
#include "interface/insertionpoint.h"
#include "notation/barstyle.h"

namespace command {

enum class Position { Start, End };

class BarStyle : public Command {
public:
  BarStyle(const Position, const notation::BarStyle,
      const interface::InsertionPoint &insertionPoint);
  void execute(interface::Control &) override;

private:
  Position mPosition;
  notation::BarStyle mBarStyle;
  interface::InsertionPoint mInsertionPoint;
};

} // namespace command
