#pragma once

#include "selection.h"

namespace command {
class Flatten : public SelectionCommand {
public:
  explicit Flatten(const interface::Control &);

protected:
  void apply_to_note(notation::Note &) override;
};
} // namespace command
