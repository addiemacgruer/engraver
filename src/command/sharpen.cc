#include "sharpen.h"

#include "notation/note.h"

namespace command {

Sharpen::Sharpen(const interface::Control &control)
    : SelectionCommand(control){};

void Sharpen::apply_to_note(notation::Note &note) {
  auto pitch = note.get_pitch();
  auto accidental = pitch.get_accidental();
  if (accidental != notation::Accidental::DoubleSharp) {
    ++accidental;
  }
  note.pitch({pitch.get_sound(), accidental, pitch.get_octave()});
}

} // namespace command
