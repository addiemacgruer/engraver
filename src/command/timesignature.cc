#include "timesignature.h"

#include "gui/genericdialogue.h"
#include "notation/movement.h"
#include "notation/timesignature.h"
#include "util/maths.h"
#include "util/string.h"

namespace command {

ChangeTimeSignature::ChangeTimeSignature(
    const interface::InsertionPoint &insertionPoint)
    : mInsertionPoint{insertionPoint} {}

ChangeTimeSignature::~ChangeTimeSignature() = default;

void ChangeTimeSignature::execute(interface::Control &) {
  auto barNo = mInsertionPoint.get_bar_number();
  if (!mHavePrompted) {
    mOriginalTimeSignature = std::make_unique<notation::TimeSignature>(
        mInsertionPoint.get_movement().get_bar_time_signature(barNo));
    auto prompt = barNo == 0
                      ? "Change initial time signature"
                      : util::concat("Change time signature at bar ", barNo);
    mTimeSignature = get_time_signature(prompt, mOriginalTimeSignature.get());
    mHavePrompted = true;
  }
  auto timesigcopy = std::make_unique<notation::TimeSignature>(*mTimeSignature);
  mInsertionPoint.get_movement().add_bar_time_signature(
      barNo, std::move(timesigcopy));
}

std::unique_ptr<notation::TimeSignature>
ChangeTimeSignature::get_time_signature(
    const std::string &title, notation::TimeSignature *oldTS) {
  auto gd = gui::GenericDialogue{0, 0, 0, 0, title.c_str()};
  auto beats{oldTS->get_beats()};
  auto beatlengths{oldTS->get_beat_lengths()};

  gd.add_integer("Number of beats", //
      [&]() { return util::concat(beats); },
      [&](const std::string &text) { beats = std::atoi(text.c_str()); });
  gd.add_integer("Length of beats", //
      [&]() { return util::concat(beatlengths); },
      [&](const std::string &text) { beatlengths = std::atoi(text.c_str()); });

  gd.show();

  if (beats <= 1)
    throw std::runtime_error{
        "Number of beats must be greater than or equal to 1"};
  if (!util::is_power_of_two(beatlengths))
    throw std::runtime_error{"Beat lengths must be a power of 2"};
  return std::make_unique<notation::TimeSignature>(beats, beatlengths);
}

} // namespace command
