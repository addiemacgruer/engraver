#include "command.h"

namespace command {

bool Command::is_undoable() {
  return false;
}

void Command::undo(interface::Control &) {}

} // namespace command
