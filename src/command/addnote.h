#pragma once

#include "command.h"
#include "interface/insertionpoint.h"

namespace notation {
class Notation;
}

namespace command {

class AddNote : public Command {
public:
  AddNote(interface::Control &control, int noteLength);
  void execute(interface::Control &) override;

protected:
  virtual std::unique_ptr<notation::Notation> get_notation();
  int mNoteLength;
  interface::InsertionPoint mInsertionPoint;
};

} // namespace command
