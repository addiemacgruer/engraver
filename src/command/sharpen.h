#pragma once

#include "selection.h"

namespace command {
class Sharpen : public SelectionCommand {
public:
  explicit Sharpen(const interface::Control &);

protected:
  void apply_to_note(notation::Note &) override;
};
} // namespace command
