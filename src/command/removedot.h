#pragma once

#include "selection.h"

namespace command {
class RemoveDot : public SelectionCommand {
public:
  explicit RemoveDot(const interface::Control &);

protected:
  void apply_to_note(notation::Note &) override;
  void apply_to_rest(notation::Rest &) override;
};
} // namespace command
