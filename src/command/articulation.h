#pragma once

#include "notation/articulations.h"
#include "selection.h"

namespace command {
class ToggleArticulation : public SelectionCommand {
public:
  ToggleArticulation(
      const interface::Control &, const notation::Articulation &articulation);

protected:
  void apply_to_note(notation::Note &) override;

private:
  notation::Articulation mArticulation;
};
} // namespace command
