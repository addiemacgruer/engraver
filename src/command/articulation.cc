#include "articulation.h"

#include "notation/note.h"

namespace command {

ToggleArticulation::ToggleArticulation(const interface::Control &control,
    const notation::Articulation &articulation)
    : SelectionCommand(control), mArticulation{articulation} {};

void ToggleArticulation::apply_to_note(notation::Note &note) {
  auto hasmArticulation = note.has_articulation(mArticulation);
  if (hasmArticulation) {
    note.remove_articulation(mArticulation);
  } else {
    note.add_articulation(mArticulation);
  }
}

} // namespace command
