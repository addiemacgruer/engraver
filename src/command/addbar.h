#pragma once

#include "command.h"
#include "interface/insertionpoint.h"

namespace command {

enum class Insertion { Global, Local };

class AddBar : public Command {
public:
  AddBar(const Insertion insertion,
      const interface::InsertionPoint &insertionPoint);
  void execute(interface::Control &) override;

private:
  Insertion mInsertion;
  interface::InsertionPoint mInsertionPoint;
};

} // namespace command
