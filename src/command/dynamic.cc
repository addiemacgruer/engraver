#include "dynamic.h"

#include "interface/control.h"
#include "notation/bar.h"
#include "notation/dynamic.h"
#include "notation/note.h"
#include "util/logging.h"

namespace command {

Dynamic::Dynamic(
    const interface::Control &control, const notation::DynamicType &dt)
    : mClearExisting{false},
      mSelected{control.get_selection()},
      mDT{dt} {}

Dynamic::Dynamic(const interface::Control &control)
    : mClearExisting{true},
      mSelected{control.get_selection()},
      mDT{notation::DynamicType::custom} {};

Dynamic::~Dynamic() = default;

void Dynamic::execute(interface::Control &) {
  util::info("Selected dynamic:", mDT, " clear=", mClearExisting);

  if (mDT == notation::DynamicType::custom) {
    throw std::runtime_error{"Custom dynamics not implemented yet"};
  }

  for (auto n : mSelected) {
    if (n->get_notation_type() != notation::NotationType::Note)
      continue; // can only apply dynamics to notes
    auto *note = dynamic_cast<notation::Note *>(n);
    if (mClearExisting) {
      note->clear_dynamic();
    } else {
      auto dynamic = std::make_unique<notation::Dynamic>(mDT);
      note->set_dynamic(std::move(dynamic));
    }
  }
};

} // namespace command
