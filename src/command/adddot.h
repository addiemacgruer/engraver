#pragma once

#include "selection.h"

namespace command {
class AddDot : public SelectionCommand {
public:
  explicit AddDot(const interface::Control &);

protected:
  void apply_to_note(notation::Note &) override;
  void apply_to_rest(notation::Rest &) override;
};
} // namespace command
