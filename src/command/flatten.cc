#include "flatten.h"

#include "notation/note.h"

namespace command {

Flatten::Flatten(const interface::Control &control)
    : SelectionCommand(control){};

void Flatten::apply_to_note(notation::Note &note) {
  auto pitch = note.get_pitch();
  auto accidental = pitch.get_accidental();
  if (accidental != notation::Accidental::FlatFlat) {
    --accidental;
  }
  note.pitch({pitch.get_sound(), accidental, pitch.get_octave()});
}

} // namespace command
