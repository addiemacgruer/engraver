#include "selection.h"

#include "interface/control.h"
#include "notation/notation.h"
#include "notation/note.h"
#include "notation/rest.h"

namespace command {

SelectionCommand::SelectionCommand(const interface::Control &control)
    : mSelection{control.get_selection()} {}

void SelectionCommand::execute(interface::Control &) {
  for (auto &notation : mSelection) {
    switch (notation->get_notation_type()) {
    case notation::NotationType::Note: {
      auto note = dynamic_cast<notation::Note *>(notation);
      apply_to_note(*note);
      break;
    }
    case notation::NotationType::Rest: {
      auto rest = dynamic_cast<notation::Rest *>(notation);
      apply_to_rest(*rest);
      break;
    }
    default:
      break;
    }
  }
}

void SelectionCommand::apply_to_note(notation::Note &) {}
void SelectionCommand::apply_to_rest(notation::Rest &) {}
void SelectionCommand::unapply_to_note(notation::Note &) {}
void SelectionCommand::unapply_to_rest(notation::Rest &) {}

} // namespace command
