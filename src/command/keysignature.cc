#include "keysignature.h"

#include "gui/genericdialogue.h"
#include "notation/key.h"
#include "notation/movement.h"
#include "util/maths.h"
#include "util/string.h"

#include <memory>

namespace command {

ChangeKeySignature::ChangeKeySignature(
    const interface::InsertionPoint &insertionPoint)
    : mInsertionPoint{insertionPoint} {}

ChangeKeySignature::~ChangeKeySignature() = default;

void ChangeKeySignature::execute(interface::Control &) {
  auto barNo = mInsertionPoint.get_bar_number();
  if (!mHavePrompted) {
    mOriginalKeySignature = std::make_unique<notation::Key>(
        mInsertionPoint.get_movement().get_bar_key(barNo));
    auto prompt = barNo == 0
                      ? "Change initial key signature"
                      : util::concat("Change key signature at bar ", barNo);
    mKeySignature = get_key_signature(prompt, mOriginalKeySignature.get());
    mHavePrompted = true;
  }
  auto keysigcopy = std::make_unique<notation::Key>(mKeySignature.get());
  mInsertionPoint.get_movement().add_bar_key_change(
      barNo, std::move(keysigcopy));
}

std::unique_ptr<notation::Key> ChangeKeySignature::get_key_signature(
    const std::string &title, notation::Key *oldKS) {
  auto gd = gui::GenericDialogue{title.c_str()};
  auto accidentals{oldKS->accidentals()};

  gd.add_integer("Number of accidentals", //
      [&]() { return util::concat(accidentals); },
      [&](const std::string &text) { accidentals = std::atoi(text.c_str()); });
  gd.show();

  if (accidentals < -7 || accidentals > 7)
    throw std::runtime_error{
        "Number of accidentals must be greater than or equal to 1"};
  return std::make_unique<notation::Key>(accidentals);
}

} // namespace command
