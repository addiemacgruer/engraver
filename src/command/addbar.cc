#include "addbar.h"

#include "interface/control.h"
#include "notation/bar.h"
#include "notation/movement.h"
#include "notation/part.h"

namespace command {

AddBar::AddBar(
    const Insertion insertion, const interface::InsertionPoint &insertionPoint)
    : mInsertion{insertion}, mInsertionPoint{insertionPoint} {}

void AddBar::execute(interface::Control &) {
  auto barnumber = mInsertionPoint.get_bar_number() + 1;
  switch (mInsertion) {
  case Insertion::Global: {
    auto &movement = mInsertionPoint.get_movement();
    for (auto &part : movement.parts()) {
      part->add_bar_before(barnumber);
    }
    movement.add_bar_before(barnumber);
    break;
  }
  case Insertion::Local:
    mInsertionPoint.get_part().add_bar_before(barnumber);
    break;
  }
}

} // namespace command
