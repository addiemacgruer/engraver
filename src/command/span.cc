#include "span.h"

#include "notation/note.h"

namespace command {

ToggleSpan::ToggleSpan(const interface::Control &control,
    const notation::Span &span, Position position)
    : SelectionCommand(control), mSpan{span}, mPosition{position} {};

void ToggleSpan::apply_to_note(notation::Note &note) {
  auto hasSpan = (mPosition == Position::Start ? note.has_span_start(mSpan)
                                               : note.has_span_end(mSpan));
  if (hasSpan) {
    if (mPosition == Position::Start)
      note.remove_span_start(mSpan);
    else
      note.remove_span_end(mSpan);
  } else {
    if (mPosition == Position::Start)
      note.add_span_start(mSpan);
    else
      note.add_span_end(mSpan);
  }
}

} // namespace command
