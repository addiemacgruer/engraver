#pragma once

#include "selection.h"

namespace command {
class Tied : public SelectionCommand {
public:
  explicit Tied(const interface::Control &);

protected:
  void apply_to_note(notation::Note &) override;
};
} // namespace command
