#include "addrest.h"

// #include "interface/control.h"
#include "notation/duration.h"
#include "notation/rest.h"

namespace command {
AddRest::AddRest(interface::Control &control, int noteLength)
    : AddNote(control, noteLength){};

std::unique_ptr<notation::Notation> AddRest::get_notation() {
  auto duration = notation::Duration{mNoteLength};
  return std::make_unique<notation::Rest>(duration);
}

} // namespace command
