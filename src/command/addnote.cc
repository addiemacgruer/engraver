#include "addnote.h"

#include "addbar.h"
#include "interface/control.h"
#include "notation/bar.h"
#include "notation/clef.h"
#include "notation/duration.h"
#include "notation/key.h"
#include "notation/movement.h"
#include "notation/note.h"
#include "notation/part.h"
#include "util/assert.h"
#include "util/logging.h"

namespace {
const notation::Key &get_key_for_bar(const notation::Bar &bar) {
  auto barnumber = bar.get_bar_number();
  auto &part = bar.get_part();
  auto &movement = part.get_movement();
  return movement.get_bar_key(barnumber);
}
} // namespace

namespace command {
AddNote::AddNote(interface::Control &control, int noteLength)
    : mNoteLength{noteLength}, mInsertionPoint{control.get_insertion_point()} {}

std::unique_ptr<notation::Notation> AddNote::get_notation() {
  auto &currentBar = mInsertionPoint.get_bar();
  auto currentClef =
      mInsertionPoint.get_part().get_clef_at_bar(currentBar.get_bar_number());
  auto centrePitch = currentClef.get_central_note();
  auto offset = mInsertionPoint.get_height();
  auto newPitch = centrePitch + offset;
  auto &key = get_key_for_bar(currentBar);
  auto keypitch = key.get_pitch_in_key(newPitch);
  auto duration = notation::Duration{mNoteLength};
  std::unique_ptr<notation::Note> rval =
      std::make_unique<notation::Note>(keypitch, duration);
  assert::is_not_null(rval.get());
  return rval;
}

void AddNote::execute(interface::Control &control) {
  util::debug("Adding note");
  auto note = get_notation();
  auto &currentBar = mInsertionPoint.get_bar();
  currentBar.add_notation_at_position(
      std::move(note), mInsertionPoint.get_notation_number());
  auto newInsertionPoint = mInsertionPoint;
  // move right after note
  newInsertionPoint.move_insertion_point_horizontally(1);
  // if at the end of the piece, add another bar
  if (newInsertionPoint.at_end_of_part()) {
    util::info("Final note, adding another bar");
    newInsertionPoint = newInsertionPoint.get_bar_after_end();
    auto addbar =
        std::make_unique<command::AddBar>(Insertion::Global, newInsertionPoint);
    control.execute_command(std::move(addbar));
  }
  // if at the end of a filled bar, move right again.
  else if (newInsertionPoint.at_end_of_bar()) {
    util::debug("At end of bar");
    newInsertionPoint.move_insertion_point_horizontally(1);
  } else {
    util::debug("Neither end of part nor end of bar");
  }
  control.set_insertion_point(newInsertionPoint);
} // namespace command
} // namespace command
