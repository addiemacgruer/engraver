#include "addpart.h"

#include "interface/control.h"
#include "notation/book.h"
#include "notation/movement.h"
#include "notation/part.h"
#include "util/logging.h"

namespace command {

AddPart::AddPart(size_t movement, size_t part)
    : mMovement{movement}, mPart{part} {}

void AddPart::execute(interface::Control &control) {
  auto &movement = control.get_book().at(mMovement);
  auto partcount = movement.parts().size();

  if (mPart == partcount) { // append
    movement.add_part_at_bottom();
  } else { // insert
    movement.insert_part_at(mPart);
  }
  auto ip = control.get_insertion_point();
  ip.select_part(mPart);
  ip.move_to_end_of_bar(); // really the start of the bar, because it will be
                           // empty
  control.set_insertion_point(ip);
}

} // namespace command
