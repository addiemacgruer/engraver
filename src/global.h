#pragma once

#include "render/svgpath.h"
#include <experimental/filesystem>
#include <memory>
#include <unordered_map>

namespace xml {
class Node;
}
namespace interface {
class Control;
}

class Global {
public:
  static Global &get_instance();
  Global(const Global &) = delete;
  void operator=(const Global &) = delete;
  ~Global();

  void init_with_root_directory(const char *arg0);
  std::experimental::filesystem::path get_root() const;

  //  void set_gui_factory(std::unique_ptr<gui::Factory> factory);
  //  gui::Factory &get_gui_factory() const;

  render::SvgPath &svg_path_for_name(const std::string &name) const;

  void add_control(std::unique_ptr<interface::Control> &&control);
  const std::vector<std::unique_ptr<interface::Control>> &get_controls() const;
  void remove_control(const interface::Control *control);

private:
  Global();
  std::unique_ptr<std::experimental::filesystem::path> mRoot;
  bool mRootInitialised{false};

  std::unordered_map<std::string, std::unique_ptr<render::SvgPath>> mSvgPaths{};
  std::vector<std::unique_ptr<interface::Control>> mControls{};

  void parse_svg_paths();
  void parse_graphic_root_node(const xml::Node &node);
  void parse_graphic_node(const xml::Node *node);
};
