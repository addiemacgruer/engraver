#pragma once

#include "point.h"

#include <cmath>
#include <iostream>

namespace geom {

namespace {

template <class T>
const Point<T> get_lower_left_from_points(
    const Point<T> &one, const Point<T> &two) {
  return {std::min(one.x(), two.x()), std::min(one.y(), two.y())};
}

template <class T>
const Point<T> get_upper_right_from_points(
    const Point<T> &one, const Point<T> &two) {
  return {std::max(one.x(), two.x()), std::max(one.y(), two.y())};
}

} // namespace

template <class T = double>
class Rect {
public:
  Rect() : mLowerLeft{0, 0}, mUpperRight{0, 0} {}

  Rect(const Point<T> &lowerLeft, const Point<T> &upperRight)
      : mLowerLeft{get_lower_left_from_points(lowerLeft, upperRight)},
        mUpperRight{get_upper_right_from_points(lowerLeft, upperRight)} {}

  Rect(const Point<T> &lowerLeft, const T width, const T height)
      : mLowerLeft{lowerLeft},
        mUpperRight{lowerLeft.x() + width, lowerLeft.y() + height} {
    if (width < 0 || height < 0)
      throw std::runtime_error{"Rect with negative width / height"};
  }

  Rect(const Rect<T> &other)
      : mLowerLeft{other.mLowerLeft}, mUpperRight{other.mUpperRight} {}

  Rect &operator=(const Rect<T> &other) {
    if (&other == this)
      return *this;
    mLowerLeft = other.mLowerLeft;
    mUpperRight = other.mUpperRight;
    return *this;
  }

  const Point<T> lower_left() const {
    return mLowerLeft;
  }

  const Point<T> upper_right() const {
    return mUpperRight;
  }

  const Point<T> upper_left() const {
    return {mLowerLeft.x(), mUpperRight.y()};
  }

  const Point<T> lower_right() const {
    return {mUpperRight.x(), mLowerLeft.y()};
  }

  const T width() const {
    return mUpperRight.x() - mLowerLeft.x();
  }

  const T height() const {
    return mUpperRight.y() - mLowerLeft.y();
  }

  const T area() const {
    return width() * height();
  }

  Rect &operator+=(const Point<T> &point) {
    mLowerLeft += point;
    mUpperRight += point;
    return *this;
  }

  Rect &operator*=(T factor) {
    mLowerLeft *= factor;
    mUpperRight *= factor;
    return *this;
  }

  Rect &operator/=(T factor) {
    mLowerLeft /= factor;
    mUpperRight /= factor;
    return *this;
  }

  Rect operator&=(const Rect<T> &other) {
    auto minX = std::min(mLowerLeft.x(), other.mLowerLeft.x());
    auto maxX = std::max(mUpperRight.x(), other.mUpperRight.x());
    auto minY = std::min(mLowerLeft.y(), other.mLowerLeft.y());
    auto maxY = std::max(mUpperRight.y(), other.mUpperRight.y());
    mUpperRight = {maxX, maxY};
    mLowerLeft = {minX, minY};
    return *this;
  }

  Rect operator&=(const Point<T> &other) {
    return operator&=({other, other});
  }

  bool operator==(const Rect<T> &other) const {
    return mLowerLeft.x() == other.mUpperLeft.x() &&   //
           mLowerLeft.y() == other.mUpperLeft.y() &&   //
           mUpperRight.x() == other.mLowerRight.x() && //
           mUpperRight.y() == other.mLowerRight.y();
  }

  /** compare which rect is biggest */
  bool operator<(const Rect<T> &other) const {
    return area() < other.area();
  }

private:
  Point<T> mLowerLeft;
  Point<T> mUpperRight;
};

/** calculate union of two rects. nb. first parameter is copied */
template <class T>
Rect<T> operator&(Rect<T> copy, const Rect<T> &other) {
  return copy &= other;
}

/** calculate union of rect and point. nb. first parameter is copied */
template <class T>
Rect<T> operator&(Point<T> copy, const Point<T> &other) {
  return copy &= other;
}

/** calculate linear scaling of a rectangle. nb. first parameter is copied */
template <class T>
Rect<T> operator*(Rect<T> copy, const T factor) {
  return copy *= factor;
}

/** calculate linear scaling of a rectangle. nb. first parameter is copied */
template <class T>
Rect<T> operator/(Rect<T> copy, const T factor) {
  return copy /= factor;
}

template <class T>
Rect<T> operator+(Rect<T> copy, const Point<T> &point) {
  return copy += point;
}

template <class T>
std::ostream &operator<<(std::ostream &out, const Rect<T> &rect) {
  out << '(' << rect.lower_left() << ',' << rect.upper_right() << ')';
  return out;
}

template <class T>
bool do_rectangles_overlap(const geom::Rect<T> &one, const geom::Rect<T> &two) {
  // If one rectangle is on left side of other
  if (one.lower_left().x() > two.upper_right().x() ||
      two.lower_left().x() > one.upper_right().x()) {
    return false;
  }

  // If one rectangle is above other
  if (one.lower_left().y() > two.upper_right().y() ||
      two.lower_left().y() > one.upper_right().y()) {
    return false;
  }
  return true;
}

template <class T>
bool is_point_in_rect(const Point<T> &point, const Rect<T> &rect) {
  return point.x() >= rect.lower_left().x() &&  //
         point.x() <= rect.upper_right().x() && //
         point.y() >= rect.lower_left().y() &&  //
         point.y() <= rect.upper_right().y();
}

} // namespace geom
