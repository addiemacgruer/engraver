#pragma once

#include "xml/serialise.h"

#include <iosfwd>
#include <memory>

namespace geom {

class Ratio : public xml::Serialise<Ratio> {
public:
  // creators
  Ratio();
  Ratio(const int numerator, const int denominator = 1);

  // static methods
  static int gcd(const int a, const int b);

  // getters
  int numerator() const;
  int denominator() const;
  double as_double() const;
  explicit operator double() const;

  // operator methods
  bool operator==(const Ratio &other) const;
  bool operator<(const Ratio &other) const;
  bool operator<=(const Ratio &other) const;
  bool operator>(const Ratio &other) const;
  bool operator>=(const Ratio &other) const;

  Ratio operator+=(const Ratio &other);
  Ratio operator-=(const Ratio &other);
  Ratio operator*=(const Ratio &other);
  Ratio operator/=(const Ratio &other);

  // serialisation
  void add_node_for_class(xml::Node &parent) override;
  static std::unique_ptr<Ratio> get_class_for_node(const xml::Node &);
  std::string get_xml_name() override;
  static constexpr const char *static_xml_name() {
    return "ratio";
  }

  // printing
  std::ostream &operator<<(std::ostream &os);

private:
  int mNumerator;
  int mDenominator;

  void normalise();
};

Ratio operator+(Ratio copy, const Ratio &other);
Ratio operator-(Ratio copy, const Ratio &other);
Ratio operator*(Ratio copy, const Ratio &other);
Ratio operator/(Ratio copy, const Ratio &other);

std::ostream &operator<<(std::ostream &, const Ratio &ratio);

} // namespace geom
