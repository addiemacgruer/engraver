#include "ratio.h"

#include "util/logging.h"
#include "xml/node.h"

#include <iostream>
#include <stdexcept>

namespace geom {

Ratio::Ratio() : mNumerator{0}, mDenominator{1} {}

Ratio::Ratio(const int numerator, const int denominator)
    : mNumerator{numerator}, mDenominator{denominator} {
  normalise();
}

void Ratio::normalise() {
  if (mDenominator == 0) {
    util::error("Ratio with zero denominator:", mNumerator, ":0");
    throw std::invalid_argument{"notation/ratio::ratio"};
  }
  auto divisor = gcd(mNumerator, mDenominator);
  if (mDenominator > 0) {
    mNumerator = mNumerator / divisor;
    mDenominator = mDenominator / divisor;
  } else {
    mNumerator = -mNumerator / divisor;
    mDenominator = -mDenominator / divisor;
  }
}

int Ratio::gcd(const int a, const int b) {
  return b == 0 ? a : gcd(b, a % b);
}

bool Ratio::operator==(const Ratio &other) const {
  return mNumerator == other.mNumerator && mDenominator == other.mDenominator;
}

bool Ratio::operator<(const Ratio &other) const {
  auto lhs = mNumerator * other.mDenominator;
  auto rhs = other.mNumerator * mDenominator;
  return lhs < rhs;
}

bool Ratio::operator<=(const Ratio &other) const {
  auto lhs = mNumerator * other.mDenominator;
  auto rhs = other.mNumerator * mDenominator;
  return lhs <= rhs;
}

bool Ratio::operator>(const Ratio &other) const {
  auto lhs = mNumerator * other.mDenominator;
  auto rhs = other.mNumerator * mDenominator;
  return lhs > rhs;
}

bool Ratio::operator>=(const Ratio &other) const {
  auto lhs = mNumerator * other.mDenominator;
  auto rhs = other.mNumerator * mDenominator;
  return lhs >= rhs;
}

Ratio Ratio::operator+=(const Ratio &other) {
  mNumerator =
      mNumerator * other.mDenominator + other.mNumerator * mDenominator;
  mDenominator = mDenominator * other.mDenominator;
  normalise();
  return *this;
}

Ratio Ratio::operator-=(const Ratio &other) {
  mNumerator =
      mNumerator * other.mDenominator - other.mNumerator * mDenominator;
  mDenominator = mDenominator * other.mDenominator;
  normalise();
  return *this;
}

Ratio Ratio::operator*=(const Ratio &other) {
  mNumerator *= other.mNumerator;
  mDenominator *= other.mDenominator;
  normalise();
  return *this;
}

Ratio Ratio::operator/=(const Ratio &other) {
  mNumerator *= other.mDenominator;
  mDenominator *= other.mNumerator;
  normalise();
  return *this;
}

int Ratio::numerator() const {
  return mNumerator;
}
int Ratio::denominator() const {
  return mDenominator;
}

std::ostream &operator<<(std::ostream &out, const Ratio &ratio) {
  out << '[' << ratio.numerator() << ":" << ratio.denominator() << ']';
  return out;
}

Ratio operator+(Ratio copy, const Ratio &other) {
  return copy += other;
}

Ratio operator-(Ratio copy, const Ratio &other) {
  return copy -= other;
}

Ratio operator*(Ratio copy, const Ratio &other) {
  return copy *= other;
}

Ratio operator/(Ratio copy, const Ratio &other) {
  return copy /= other;
}

Ratio::operator double() const {
  return static_cast<double>(mNumerator) / mDenominator;
}

double Ratio::as_double() const {
  return static_cast<double>(*this);
}

void Ratio::add_node_for_class(xml::Node &parent) {
  auto subnode = parent.append_child("ratio");
  subnode->add_node("numerator", mNumerator);
  subnode->add_node("denominator", mDenominator);
}

std::unique_ptr<Ratio> Ratio::get_class_for_node(const xml::Node &node) {
  auto numerator = 0;
  auto denominator = 1;
  node.once(
      "numerator", [&](const xml::Node &n) { numerator = n.get_value<int>(); });
  node.once("denominator",
      [&](const xml::Node &n) { denominator = n.get_value<int>(); });
  return std::make_unique<Ratio>(numerator, denominator);
}

std::ostream &Ratio::operator<<(std::ostream &os) {
  os << '{' << mNumerator << '/' << mDenominator << '}';
  return os;
}

std::string Ratio::get_xml_name() {
  return static_xml_name();
}

} // namespace geom
