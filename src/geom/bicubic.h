#pragma once

#include "geom/point.h"

namespace geom {

/** returns the position that is t% between points a and b */
template <class T>
const geom::Point<T> pointInLine(
    const geom::Point<T> &a, const geom::Point<T> &b, T t) {
  return {a.x() - ((a.x() - b.x()) * t), a.y() - ((a.y() - b.y()) * t)};
}

/** generates the bicubic drawn between start and end, using control points cp1
 * and cp2, and outputs line segments to LineFunc line */
template <class T, class LineFunc>
void draw_bicubic(const geom::Point<T> &start, const geom::Point<T> &cp1,
    const geom::Point<T> &cp2, const geom::Point<T> &end, const LineFunc &line,
    T accuracy = 5) {
  auto previousPoint = start;
  for (size_t j = 1; j < accuracy; ++j) {
    double i = static_cast<double>(j) / accuracy;
    auto A = pointInLine(start, cp1, i);
    auto B = pointInLine(cp1, cp2, i);
    auto C = pointInLine(cp2, end, i);
    auto D = pointInLine(A, B, i);
    auto E = pointInLine(B, C, i);
    auto F = pointInLine(D, E, i);
    line(previousPoint, F);
    previousPoint = F;
  }
  line(previousPoint, end);
}

} // namespace geom
