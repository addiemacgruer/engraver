#pragma once
#include <cmath>
#include <iostream>

namespace geom {

template <class T = double>
class Point {
public:
  // creators
  Point(const T &x = 0, const T &y = 0) : mX{x}, mY{y} {}
  virtual ~Point() = default;
  Point(const Point<T> &other) : mX{other.mX}, mY{other.mY} {}

  // getters
  const T x() const {
    return mX;
  }
  const T y() const {
    return mY;
  }

  const T distance_to(Point<T> other) const {
    auto dx = mX - other.mX;
    auto dy = mY - other.mY;
    return std::sqrt(dx * dx + dy * dy);
  }

  Point &operator*=(const T factor) {
    mX *= factor;
    mY *= factor;
    return *this;
  }

  Point &operator/=(const T factor) {
    mX /= factor;
    mY /= factor;
    return *this;
  }

  Point &operator+=(const Point<T> &other) {
    mX += other.mX;
    mY += other.mY;
    return *this;
  }

  Point &operator-=(const Point<T> &other) {
    mX -= other.mX;
    mY -= other.mY;
    return *this;
  }

  bool operator==(const Point &other) const {
    return (mX == other.mX) && (mY == other.mY);
  }

  // mutators
  Point &operator=(const Point<T> &other) {
    if (&other == this)
      return *this;
    mX = other.mX;
    mY = other.mY;
    return *this;
  }

private:
  T mX;
  T mY;
};

template <class T>
Point<T> operator+(Point<T> copy, const Point<T> &second) {
  return copy += second;
}

template <class T>
Point<T> operator-(Point<T> copy, const Point<T> &second) {
  return copy -= second;
}

template <class T>
Point<T> operator*(Point<T> copy, T factor) {
  return copy *= factor;
}

template <class T>
Point<T> operator/(Point<T> copy, T factor) {
  return copy /= factor;
}

template <class T>
std::ostream &operator<<(std::ostream &out, const Point<T> &point) {
  out << '(' << point.x() << ',' << point.y() << ')';
  return out;
}

} // namespace geom
