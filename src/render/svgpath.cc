#include "svgpath.h"

#include "geom/bicubic.h"
#include "geom/point.h"
#include "renderer.h"
#include "scanline.h"
#include "util/logging.h"

#include <algorithm>
#include <cmath>
#include <sstream>

namespace render {

class PathElement {
public:
  virtual ~PathElement() noexcept;
  virtual void add_lines_to_renderer(Renderer &renderer) = 0;
  virtual void add_lines_to_scanline(Scanline &sl, double scale) = 0;
};

PathElement::~PathElement() noexcept {};

static constexpr double lineAccuracy{1};

namespace {

double next_double(std::istringstream &in) {
  std::string word;
  in >> word;
  return std::atof(word.c_str());
}

} // namespace

class LineElement : public PathElement {
public:
  LineElement(const geom::Point<> &start, const geom::Point<double> &end)
      : start_{start}, end_{end} {}

  ~LineElement() override;

  void add_lines_to_renderer(Renderer &renderer) override {
    renderer.draw_line(renderer.xy() + start_, renderer.xy() + end_);
  }

  void add_lines_to_scanline(Scanline &sl, double) override {
    sl.add_line(start_, end_);
  }

private:
  const geom::Point<> start_;
  const geom::Point<> end_;
};

LineElement::~LineElement() = default;

class BicubicElement : public PathElement {
public:
  BicubicElement(const geom::Point<> &start, const geom::Point<double> &cp1,
      const geom::Point<> &cp2, const geom::Point<double> &end)
      : start_{start}, cp1_{cp1}, cp2_{cp2}, end_{end} {}

  ~BicubicElement() override;

  void add_lines_to_renderer(Renderer &renderer) override {
    auto drawlinelambda = [&](const geom::Point<> &start,
                              const geom::Point<> &end) {
      renderer.draw_line(renderer.xy() + start, renderer.xy() + end);
    };

    auto length = start_.distance_to(end_) * renderer.scale();
    size_t accuracy = length / lineAccuracy;
    geom::draw_bicubic<double>(
        start_, cp1_, cp2_, end_, drawlinelambda, accuracy);
  }

  void add_lines_to_scanline(Scanline &sl, double scale) override {
    auto drawlinelambda = [&](const geom::Point<> &start,
                              const geom::Point<> &end) {
      sl.add_line(start, end);
    };

    auto length = start_.distance_to(end_) * scale;
    size_t accuracy = length / lineAccuracy;
    geom::draw_bicubic<double>(
        start_, cp1_, cp2_, end_, drawlinelambda, accuracy);
  }

private:
  const geom::Point<> start_;
  const geom::Point<> cp1_;
  const geom::Point<> cp2_;
  const geom::Point<> end_;
};

BicubicElement::~BicubicElement() = default;

SvgPath::SvgPath(const std::string &spec) {
  geom::Rect<> rval{};
  geom::Point<> last{0, 0};
  geom::Point<> first{0, 0}; // needed to close paths
  char command = '?';

  auto stringcopy = std::string{spec};
  std::replace(stringcopy.begin(), stringcopy.end(), ',', ' ');
  std::istringstream iss{stringcopy, std::istringstream::in};

  std::string word;
  while (iss >> word) {
    if (word.length() == 1 && word[0] >= 'A' &&
        word[0] <= 'z') { // process single letter commands
      command = word[0];
      if (command == 'z' || command == 'Z') {
        mPathElements.push_back(std::make_unique<LineElement>(last, first));
        last = first;
      }
      continue;
    }
    // otherwise we've found a number.  Process based on last command
    auto wordvalue = std::atof(word.c_str());
    switch (command) {
    case 'M': // absolute moveto
      last = {wordvalue, next_double(iss)};
      mBoundingBox &= last;
      first = last;
      break;
    case 'm': // relative moveto
      last = {last.x() + wordvalue, last.y() + next_double(iss)};
      mBoundingBox &= last;
      first = last;
      break;
    case 'L': { // absolute lineto
      geom::Point<> next = {wordvalue, next_double(iss)};
      mPathElements.push_back(std::make_unique<LineElement>(last, next));
      mBoundingBox &= next;
      last = next;
    } break;
    case 'l': { // relative lineto
      geom::Point<> next = {last.x() + wordvalue, last.y() + next_double(iss)};
      mPathElements.push_back(std::make_unique<LineElement>(last, next));
      mBoundingBox &= next;
      last = next;
    } break;
    case 'c': { // relative bicubic curveto
      geom::Point<> cp1 = {last.x() + wordvalue, last.y() + next_double(iss)};
      geom::Point<> cp2 = {
          last.x() + next_double(iss), last.y() + next_double(iss)};
      geom::Point<> next = {
          last.x() + next_double(iss), last.y() + next_double(iss)};
      mPathElements.push_back(
          std::make_unique<BicubicElement>(last, cp1, cp2, next));
      mBoundingBox &= cp1;
      mBoundingBox &= cp2;
      mBoundingBox &= next;
      last = next;
    } break;
    case 'C': { // absolute  bicubic curveto
      geom::Point<> cp1 = {wordvalue, next_double(iss)};
      geom::Point<> cp2 = {next_double(iss), next_double(iss)};
      geom::Point<> next = {next_double(iss), next_double(iss)};
      mPathElements.push_back(
          std::make_unique<BicubicElement>(last, cp1, cp2, next));
      mBoundingBox &= cp1;
      mBoundingBox &= cp2;
      mBoundingBox &= next;
      last = next;
    } break;
    case 'H': { // absolute horizontalto
      geom::Point<> next = {wordvalue, last.y()};
      mPathElements.push_back(std::make_unique<LineElement>(last, next));
      mBoundingBox &= next;
      last = next;
    } break;
    case 'h': { // relative horizontalto
      geom::Point<> next = {last.x() + wordvalue, last.y()};
      mPathElements.push_back(std::make_unique<LineElement>(last, next));
      mBoundingBox &= next;
      last = next;
    } break;
    case 'V': { // absolute verticalto
      geom::Point<> next = {last.x(), wordvalue};
      mPathElements.push_back(std::make_unique<LineElement>(last, next));
      mBoundingBox &= next;
      last = next;
    } break;
    case 'v': { // relative verticalto
      geom::Point<> next = {last.x(), last.y() + wordvalue};
      mPathElements.push_back(std::make_unique<LineElement>(last, next));
      mBoundingBox &= next;
      last = next;
    } break;
    default:
      util::error("Failed to draw SVG correctly: command ", command);
    }
  };
}

SvgPath::~SvgPath() {
  //  util::log_alloc(util::Alloc::Destruct, "view", "SvgPath");
}

geom::Rect<> SvgPath::draw(Renderer &renderer, interface::LayoutManager &) {
  for (const auto &element : mPathElements)
    element->add_lines_to_renderer(renderer);
  return mBoundingBox + renderer.xy();
}

const geom::Rect<> SvgPath::get_bounding_box() const {
  return mBoundingBox;
}

void SvgPath::draw_to_scanline(
    Renderer &renderer, interface::LayoutManager &lm) const {
  auto sl = Scanline{};
  for (const auto &element : mPathElements)
    element->add_lines_to_scanline(sl, renderer.scale());
  sl.draw(renderer, lm);
}

} // namespace render
