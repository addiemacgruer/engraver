#pragma once
#include "geom/point.h"
#include "geom/rect.h"

#include <memory>        // for std::unique_ptr
#include <unordered_map> // for std::map
#include <vector>        // for std::vector

namespace interface {
class ManuscriptCanvas;
class LayoutManager;
} // namespace interface
namespace notation {
class Notation;
}

/** \namespace render \brief Classes and functions for drawing things on screen
 */
namespace render {

class Colour {
public:
  uint8_t red{0};
  uint8_t green{0};
  uint8_t blue{0};
  uint8_t alpha{255};
};

class PenStyle {
public:
  Colour colour{};
  double width{1};
};

enum class FontFamily { Default };

enum class FontStyle { Normal };

enum class FontWeight { Normal };

enum class FontUnderline { Underline = true, NoUnderline = false };

class Renderer;
class PenPusher {
public:
  explicit PenPusher(render::Renderer *);
  ~PenPusher();

private:
  render::Renderer *mpRenderer;
};

class Renderer {
public:
  Renderer(const Renderer &) = delete;
  virtual ~Renderer();
  explicit Renderer();

  virtual Renderer &push_point(const geom::Point<> &point) = 0;
  virtual geom::Point<> pop_point() = 0;

  virtual PenPusher push_pen(const PenStyle newStyle) = 0;
  virtual void pop_pen() = 0;

  virtual const geom::Rect<> &get_viewing_rect() const = 0;
  virtual void set_viewing_rect(const geom::Rect<> &vRect) = 0;

  virtual const geom::Point<> xy() const = 0;
  virtual double x() const = 0;
  virtual double y() const = 0;
  virtual double scale() const = 0;

  virtual void schedule_redraw() = 0;

  virtual Renderer &add_x(double addx) = 0;
  virtual Renderer &add_y(double addy) = 0;
  virtual Renderer &set_x(double newx) = 0;
  virtual Renderer &set_y(double newy) = 0;

  virtual void clear_canvas() = 0;

  virtual void set_font(const int size,
      const FontFamily &fontFamily = FontFamily::Default,
      const FontStyle &fontStyle = FontStyle::Normal,
      const FontWeight &fontWeight = FontWeight::Normal,
      const FontUnderline &FontUnderline =
          FontUnderline::NoUnderline) const = 0;

  virtual geom::Rect<> draw_text(
      const std::string &text, const geom::Point<> &point) = 0;

  virtual geom::Rect<> draw_line(
      const geom::Point<> &start, const geom::Point<double> &end) = 0;

  virtual geom::Rect<> draw_rect(const geom::Point<> &topLeft,
      const geom::Point<> &bottomRight, bool isFilled = false) = 0;

  virtual geom::Rect<> draw_ellipse(const geom::Point<> &centre,
      const double width, const double height, bool isFilled = false) = 0;

  virtual geom::Rect<> draw_bicubic(const geom::Point<> &start,
      const geom::Point<> &cp1, const geom::Point<double> &cp2,
      const geom::Point<> &end) = 0;

  virtual geom::Rect<> draw_tl(
      double x, double y, double w, double h, bool isFilled = false) = 0;
  virtual geom::Rect<> draw_tr(
      double x, double y, double w, double h, bool isFilled = false) = 0;
  virtual geom::Rect<> draw_br(
      double x, double y, double w, double h, bool isFilled = false) = 0;
  virtual geom::Rect<> draw_bl(
      double x, double y, double w, double h, bool isFilled = false) = 0;

  virtual geom::Rect<> draw_cached(
      const std::string &name, interface::LayoutManager &lm) = 0;
  virtual void draw_pixel(int x_, int y_, int alpha = 255) = 0;
};

} // namespace render
