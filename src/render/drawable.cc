#include "drawable.h"

namespace render {

Drawable::Drawable() = default;
Drawable::~Drawable() = default;

Drawable::Drawable(const Drawable &) = default;
Drawable::Drawable(Drawable &&) noexcept = default;

} // namespace render
