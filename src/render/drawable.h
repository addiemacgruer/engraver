#pragma once

#include "geom/rect.h"               // for geom::Rect<>
#include "interface/layoutmanager.h" // for interface::LayoutManager
#include "render/renderer.h"         // for Renderer

namespace render {

class Drawable {
public:
  Drawable();
  virtual ~Drawable();
  Drawable(const Drawable &);
  Drawable(Drawable &&) noexcept;

  virtual geom::Rect<> draw(Renderer &, interface::LayoutManager &) = 0;
};

} // namespace render
