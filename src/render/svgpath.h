#pragma once

#include "drawable.h"
#include "geom/rect.h"

#include <memory>
#include <vector>

namespace render {

class PathElement;

class SvgPath : public Drawable {
public:
  explicit SvgPath(const std::string &spec);
  SvgPath(const SvgPath &) = delete;
  ~SvgPath() override;

  geom::Rect<> draw(Renderer &, interface::LayoutManager &) override;
  void draw_to_scanline(Renderer &renderer, interface::LayoutManager &lm) const;
  const geom::Rect<> get_bounding_box() const;

private:
  std::vector<std::unique_ptr<PathElement>> mPathElements;
  geom::Rect<> mBoundingBox;
};

} // namespace render
