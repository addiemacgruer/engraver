#pragma once

#include "renderer.h"

#include <shared_mutex>

namespace render {

/** this class essentially just returns the empty boxes that various shapes
 * would occupy - lets us do layout sizing without the expense of actually
 * drawing to screen */
class LayoutRenderer : public Renderer {
public:
  explicit LayoutRenderer();

  LayoutRenderer &push_point(const geom::Point<> &point) override;
  geom::Point<> pop_point() override;

  render::PenPusher push_pen(const PenStyle newStyle) override;
  void pop_pen() override;

  const geom::Rect<> &get_viewing_rect() const override;
  void set_viewing_rect(const geom::Rect<> &vRect) override;

  const geom::Point<> xy() const override;
  double x() const override;
  double y() const override;
  double scale() const override;

  void schedule_redraw() override;

  LayoutRenderer &add_x(double addx) override;
  LayoutRenderer &add_y(double addy) override;
  LayoutRenderer &set_x(double newx) override;
  LayoutRenderer &set_y(double newy) override;

  void clear_canvas() override;

  void set_font(const int size,
      const render::FontFamily &fontFamily = render::FontFamily::Default,
      const render::FontStyle &fontStyle = render::FontStyle::Normal,
      const render::FontWeight &fontWeight = render::FontWeight::Normal,
      const render::FontUnderline &FontUnderline =
          render::FontUnderline::NoUnderline) const override;

  geom::Rect<> draw_text(
      const std::string &text, const geom::Point<> &point) override;

  geom::Rect<> draw_line(
      const geom::Point<> &start, const geom::Point<double> &end) override;

  geom::Rect<> draw_rect(const geom::Point<> &topLeft,
      const geom::Point<> &bottomRight, bool isFilled = false) override;

  geom::Rect<> draw_ellipse(const geom::Point<> &centre, const double width,
      const double height, bool isFilled = false) override;

  geom::Rect<> draw_bicubic(const geom::Point<> &start,
      const geom::Point<> &cp1, const geom::Point<double> &cp2,
      const geom::Point<> &end) override;

  geom::Rect<> draw_tl(
      double x, double y, double w, double h, bool isFilled = false) override;
  geom::Rect<> draw_tr(
      double x, double y, double w, double h, bool isFilled = false) override;
  geom::Rect<> draw_br(
      double x, double y, double w, double h, bool isFilled = false) override;
  geom::Rect<> draw_bl(
      double x, double y, double w, double h, bool isFilled = false) override;

  geom::Rect<> draw_cached(
      const std::string &name, interface::LayoutManager &lm) override;
  void draw_pixel(int x_, int y_, int alpha = 255) override;

private:
  mutable std::shared_mutex mMutex{};
  std::vector<geom::Point<>> mPointsList{};

  void set_pen_from_stack();
  double x_offset();
  double y_offset();
};

} // namespace render
