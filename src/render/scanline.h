#pragma once

#include "drawable.h"
#include "geom/point.h"
#include "geom/rect.h"

#include <memory>
#include <vector>

namespace render {

class Edge;

class Scanline : public Drawable {
public:
  Scanline();
  ~Scanline() override;

  void add_line(const geom::Point<> &a, const geom::Point<double> &b);
  geom::Rect<> draw(Renderer &renderer, interface::LayoutManager &lm) override;

private:
  std::vector<Edge> mEdges;
  std::vector<Edge> mActiveEdges;
  std::vector<Edge> mWorkingEdges;
  geom::Rect<> mBoundingRect;
  std::unique_ptr<uint8_t[]> mPixels;
  int mWidth{};
  int mHeight{};
  double mScale{10};

  void move_working_to_active(double y);
  void clear_expired(double y);
  void fill_pixels(int y, double xstart, double xend, Renderer &r);
  void sort_active();
};

} // namespace render
