#include "layoutrenderer.h"

#include "global.h"
#include "interface/layoutmanager.h"
#include "interface/manuscriptcanvas.h"
#include "scanline.h"
#include "util/logging.h"

#include <algorithm>
#include <cmath>
#include <sstream>

namespace render {

using p = geom::Point<>;
using r = geom::Rect<>;

namespace {

template <class T>
const constexpr geom::Point<T> pointInLine(
    const geom::Point<T> &a, const geom::Point<T> &b, T t) {
  return {a.x() - ((a.x() - b.x()) * t), a.y() - ((a.y() - b.y()) * t)};
}
} // namespace

void LayoutRenderer::schedule_redraw() {
  util::warning(
      "view::LayoutRenderer::schedule_redraw() called,  but it does nothing");
}

LayoutRenderer::LayoutRenderer() = default;

void LayoutRenderer::clear_canvas() {}

double LayoutRenderer::x_offset() {
  return 0;
}

double LayoutRenderer::y_offset() {
  return 0;
}

LayoutRenderer &LayoutRenderer::push_point(const p &point) {
  std::unique_lock<std::shared_mutex> _{mMutex};
  mPointsList.push_back(point);
  return *this;
}

p LayoutRenderer::pop_point() {
  std::unique_lock<std::shared_mutex> _{mMutex};
  auto rval = mPointsList.back();
  mPointsList.pop_back();
  return rval;
}

void LayoutRenderer::set_pen_from_stack() {}

render::PenPusher LayoutRenderer::push_pen(const PenStyle) {
  return render::PenPusher{this};
}
void LayoutRenderer::pop_pen() {}

const p LayoutRenderer::xy() const {
  std::shared_lock<std::shared_mutex> _{mMutex};
  return mPointsList.back();
}
double LayoutRenderer::x() const {
  std::shared_lock<std::shared_mutex> _{mMutex};
  return mPointsList.back().x();
}
double LayoutRenderer::y() const {
  std::shared_lock<std::shared_mutex> _{mMutex};
  return mPointsList.back().y();
}
LayoutRenderer &LayoutRenderer::add_x(double addx) {
  auto point = pop_point();
  return push_point({point.x() + addx, point.y()});
}

LayoutRenderer &LayoutRenderer::set_x(double newx) {
  auto point = pop_point();
  return push_point({newx, point.y()});
}

LayoutRenderer &LayoutRenderer::set_y(double newy) {
  auto point = pop_point();
  return push_point({point.x(), newy});
}

LayoutRenderer &LayoutRenderer::add_y(double addy) {
  auto point = pop_point();
  return push_point({point.x(), point.y() + addy});
}

void LayoutRenderer::set_font(const int, const FontFamily &, const FontStyle &,
    const FontWeight &, const FontUnderline &) const {}

// static std::mutex mFontMutex{};

r LayoutRenderer::draw_text(const std::string &, const p &point) {
  // TODO evaluate font size without FLTK
  return {point, point};
  //  std::lock_guard<std::mutex> _{mFontMutex};
  //  auto textwidth =
  //      fl_width(text.c_str()); //  this call is not thread-safe,  fucking
  //      hell
  //  return {point, {point.x() + textwidth, point.y() + fl_height()}};
}

r LayoutRenderer::draw_line(const p &start, const p &end) {
  return {{start.x(), start.y()}, {end.x(), end.y()}};
}

r LayoutRenderer::draw_rect(const p &topLeft, const p &bottomRight, bool) {
  return {topLeft, bottomRight};
}

r LayoutRenderer::draw_ellipse(
    const p &centre, const double width, const double height, bool) {
  return {{centre.x() - width / 2, centre.y() - height / 2},
      {centre.x() + width / 2, centre.y() + height / 2}};
}

r LayoutRenderer::draw_bicubic(
    const p &start, const p &, const p &, const p &end) {
  return geom::Rect<>{start, end};
}

geom::Rect<> LayoutRenderer::draw_tl(
    double x, double y, double w, double h, bool) {
  return {{x, y}, w, h};
}
geom::Rect<> LayoutRenderer::draw_tr(
    double x, double y, double w, double h, bool) {
  return {{x, y}, w, h};
}
geom::Rect<> LayoutRenderer::draw_br(
    double x, double y, double w, double h, bool) {
  return {{x, y}, w, h};
}
geom::Rect<> LayoutRenderer::draw_bl(
    double x, double y, double w, double h, bool) {
  return {{x, y}, w, h};
}

void LayoutRenderer::draw_pixel(int, int, int) {}

double LayoutRenderer::scale() const {
  return 10;
}

geom::Rect<> LayoutRenderer::draw_cached(
    const std::string &name, interface::LayoutManager &) {
  auto &shape = Global::get_instance().svg_path_for_name(name);
  auto bb = shape.get_bounding_box();
  auto bounds = bb + xy();
  return bounds;
}

void LayoutRenderer::set_viewing_rect(const geom::Rect<> &) {}

static geom::Rect<> sNone = {};
const geom::Rect<> &LayoutRenderer::get_viewing_rect() const {
  return sNone;
}

} // namespace render
