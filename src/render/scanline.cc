#include "scanline.h"

#include "renderer.h"
#include "util/logging.h"
#include "util/maths.h"

#include <algorithm>  //  for std::sort
#include <functional> //  for std::equal_to

namespace render {

class Edge {
public:
  Edge(const geom::Point<> a, const geom::Point<double> b);

  double mMinY;
  double mMaxY;
  double x;
  double m;
};

Edge::Edge(const geom::Point<> a, const geom::Point<double> b) {
  mMinY = std::min(a.y(), b.y());
  mMaxY = std::max(a.y(), b.y());
  x = a.y() < b.y() ? a.x() : b.x();
  m = (a.x() - b.x()) / (a.y() - b.y());
}

namespace {
//  shut up,  warning: comparing floating point with == or != is unsafe
template <class T>
bool float_equal(const T &lhs, const T &rhs) {
  return std::equal_to<T>()(lhs, rhs);
}

// backwards, so that we can use pop_back on the sorted vector
bool edgeSortFunction(const Edge &a, const Edge &b) {
  if (!float_equal(a.mMinY, b.mMinY))
    return a.mMinY > b.mMinY;
  return a.x > b.x;
}

bool xSortFunction(const Edge &a, const Edge &b) {
  return a.x < b.x;
}

} // namespace

void Scanline::add_line(const geom::Point<> &a, const geom::Point<double> &b) {
  if (float_equal(a.y(), b.y())) // we don't need to draw horizontal lines
    return;
  mEdges.push_back(Edge(a, b));
  mBoundingRect &= a;
  mBoundingRect &= b;
}
Scanline::Scanline() = default;
Scanline::~Scanline() = default;

void Scanline::move_working_to_active(double y) {
  while (true) {
    if (mWorkingEdges.empty())
      return;
    if (mWorkingEdges.back().mMinY <= y) {
      mActiveEdges.push_back(mWorkingEdges.back());
      mWorkingEdges.pop_back();
    } else
      return;
  }
}

void Scanline::sort_active() {
  std::sort(mActiveEdges.begin(), mActiveEdges.end(), xSortFunction);
}

void Scanline::clear_expired(double y) {
  if (mActiveEdges.empty())
    return;
  auto astart = mActiveEdges.begin();
  auto aend = mActiveEdges.end();
  auto eraser =
      std::remove_if(astart, aend, [&](const Edge &e) { return e.mMaxY <= y; });
  if (eraser != mActiveEdges.end())
    mActiveEdges.erase(eraser, mActiveEdges.end());
}

void Scanline::fill_pixels(int y, double xstart, double xend, Renderer &r) {
  auto start = util::get_whole_and_fraction(xstart);
  int xstartint = start.whole + 1;

  auto xendfraction = util::get_whole_and_fraction(xend);
  int xendint = xendfraction.whole;

  int xoffset = r.x() * r.scale();
  int yoffset = r.y() * r.scale() + y;

  for (auto x = xstartint; x <= xend; ++x) {
    r.draw_pixel(xoffset + x, yoffset);
  }

  // antialiasing
  r.draw_pixel(xoffset + xstartint - 1, yoffset, 0xff * (1 - start.fraction));
  r.draw_pixel(xoffset + xendint + 1, yoffset, 0xff * xendfraction.fraction);
}

geom::Rect<> Scanline::draw(Renderer &renderer, interface::LayoutManager &) {
  mWidth = mBoundingRect.width() * renderer.scale();
  mHeight = mBoundingRect.height() * renderer.scale();
  mScale = renderer.scale();

  //  util::log(util::LogLevel::Info, "Rendering scanline with ", edges_.size(),
  //            " edges, bounding rect:", boundingRect_, " scale=", scale);

  std::sort(mEdges.begin(), mEdges.end(), edgeSortFunction);
  mWorkingEdges = mEdges;

  int i = (mWorkingEdges.back().mMinY - renderer.y()) * mScale;
  double anomaly = renderer.x() / mScale;

  while (true) {
    auto y = (static_cast<double>(i) + renderer.y()) / mScale;
    move_working_to_active(y);
    clear_expired(y);

    if (mActiveEdges.size() % 2 != 0) {
      util::log(util::LogLevel::Error, "Need an even number of active edges");
      throw std::runtime_error{"view/scanline::draw"};
    }

    sort_active();

    for (auto it = mActiveEdges.begin(); it != mActiveEdges.end(); it += 2) {
      auto startx = it->x;
      auto endx = (it + 1)->x;
      fill_pixels(
          i, (startx - anomaly) * mScale, (endx - anomaly) * mScale, renderer);
    }

    for (auto &edge : mActiveEdges) {
      edge.x += edge.m / mScale;
    }

    if (mActiveEdges.empty() && mWorkingEdges.empty())
      break;
    ++i;
  }

  return mBoundingRect;
}

} // namespace render
