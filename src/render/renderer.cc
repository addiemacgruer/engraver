#include "renderer.h"

render::PenPusher::PenPusher(render::Renderer *r) : mpRenderer{r} {}
render::PenPusher::~PenPusher() {
  mpRenderer->pop_pen();
}

render::Renderer::Renderer() = default;

render::Renderer::~Renderer() = default;
