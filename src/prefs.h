#pragma once

#include <cstddef> // for size_t

template <class T>
class Pref {
public:
  explicit Pref(T value) : mValue{value} {}
  T get() const {
    return mValue;
  }
  void set(T value) {
    mValue = value;
  }

private:
  T mValue;
};

/** Store user preferences - options not tied to any particular file */
// TODO implement some kind of persistent store, prefs window, ...
class Prefs {
public:
  static Prefs &get_instance();
  Pref<size_t> thread_count() const;
  Pref<size_t> default_zoom() const;

private:
  Prefs();
  ~Prefs();
  Pref<size_t> mThreadCount;
  Pref<size_t> mDefaultZoom{20};
};
