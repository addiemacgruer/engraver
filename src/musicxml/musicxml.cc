#include "musicxml.h"

#include "notation/articulations.h"
#include "notation/bar.h"
#include "notation/barstyle.h"
#include "notation/book.h"
#include "notation/clef.h"
#include "notation/cleftype.h"
#include "notation/movement.h"
#include "notation/movementkey.h" // for MovementKey
#include "notation/note.h"
#include "notation/part.h"
#include "notation/span.h"
#include "notation/timesignature.h"
#include "notation/volta.h" // for Volta
#include "util/logging.h"
#include "util/string.h"

#include <cstdlib>
#include <cstring>
#include <future>
#include <iostream>
#include <pugixml.hpp>
#include <stdexcept>
#include <stdlib.h>

namespace musicxml {

namespace {

using namespace notation;
using namespace pugi;
using cxnode = const xml_node;
using spMovement = std::shared_ptr<Movement>;
using spNote = notation::Note *;
using spPart = notation::Part *;

void parse_error [[noreturn]] (const std::string &where, const char *name) {
  util::error("Parse error in '", where, "' - unknown node (", name, ")");
  throw std::invalid_argument{
      util::concat("Parse error in '", where, "' - unknown node (", name, ")")};
}

#define TRY(title) if (util::compare(subnode.name(), title))

#define MISSING(title)                                                         \
  if (util::compare(subnode.name(), title))                                    \
  missing(title)

#define IGNORING(title)                                                        \
  if (util::compare(subnode.name(), title))                                    \
  ignoring(title)

#define PARSEFAIL(where) parse_error(where, subnode.name());

void missing(const std::string &which) {
  util::log(util::LogLevel::Warning, "Not yet implemented: ", which);
}

void ignoring(const std::string &which) {
  util::log(util::LogLevel::Info, "Ignoring ", which);
}

void movement_title(Movement &movement, cxnode &node) {
  movement.set(MovementKey::title, node.child_value());
}

void note_pitch_step(spNote &note, cxnode &node) {
  if (strlen(node.child_value()) != 1) {
    util::log(util ::LogLevel::Error,
        "Tried to add an unknown note:", node.child_value());
    throw std::runtime_error{"readers/musicxml::note_pitch_step"};
  }
  switch (node.child_value()[0]) {
  case 'A':
    note->pitch({Sound::A, note->get_pitch().get_accidental(),
        note->get_pitch().get_octave()});
    break;
  case 'B':
    note->pitch({Sound::B, note->get_pitch().get_accidental(),
        note->get_pitch().get_octave()});
    break;
  case 'C':
    note->pitch({Sound::C, note->get_pitch().get_accidental(),
        note->get_pitch().get_octave()});
    break;
  case 'D':
    note->pitch({Sound::D, note->get_pitch().get_accidental(),
        note->get_pitch().get_octave()});
    break;
  case 'E':
    note->pitch({Sound::E, note->get_pitch().get_accidental(),
        note->get_pitch().get_octave()});
    break;
  case 'F':
    note->pitch({Sound::F, note->get_pitch().get_accidental(),
        note->get_pitch().get_octave()});
    break;
  case 'G':
    note->pitch({Sound::G, note->get_pitch().get_accidental(),
        note->get_pitch().get_octave()});
    break;
  default:
    util::error("Unknown note for note_pitch_step:", node.child_value());
    throw std::runtime_error("readers/musicxml::note_pitch_step");
  }
}

void note_pitch_octave(spNote &note, cxnode &node) {
  int octave = std::stoi(node.child_value());
  note->pitch({note->get_pitch().get_sound(),
      note->get_pitch().get_accidental(), octave});
}

void note_pitch(spNote note, cxnode &node) {
  for (const auto &subnode : node) {
    TRY("step") note_pitch_step(note, subnode);
    else TRY("octave") note_pitch_octave(note, subnode);
    else PARSEFAIL("note_pitch");
  }
}

void note_type(spNote note, cxnode &node) {
  auto noteValue{node.child_value()};
  int baseDuration{1};
  if (util::compare(noteValue, "quarter")) { // crotchet
    baseDuration = 4;
  } else if (util::compare(noteValue, "eighth")) { // quaver
    baseDuration = 8;
  } else if (util::compare(noteValue, "16th")) { // semiquaver
    baseDuration = 16;
  } else {
    util::error("Unknown note length:", noteValue);
    throw std::runtime_error{"readers/musicxml::note_type"};
  }
  note->set_duration(Duration{baseDuration, 0});
};

void note_dot(spNote note) {
  auto oldDots{note->get_duration().get_dots()};
  note->set_duration(
      Duration{note->get_duration().get_base_duration(), oldDots + 1});
}

void note_voice(spNote, cxnode &node) {
  auto voice{node.child_value()};
  if (!util::compare(voice, "1")) {
    util::error("Not yet equipped to handle multiple voices");
    throw std::runtime_error{"readers/musicxml::note_voice"};
  }
}

void note_notation_slur(spNote &note, cxnode &node) {
  if (util::compare(node.attribute("type").as_string(), "start")) {
    note->has_span_start(Span::Slur);
  } else if (util::compare(node.attribute("type").as_string(), "stop")) {
    note->has_span_end(Span::Slur);
  } else {
    util::error("Don't understand slur command:", node.attribute("type"));
    throw std::runtime_error{"readers/musicxml::note_notation_slur"};
  }
}

void note_notation_articulations(spNote &note, cxnode &node) {
  for (auto &subnode : node) {
    TRY("staccatissimo")
    note->add_articulation(Articulation::Staccatissimo);
    else PARSEFAIL("note_notation_articulations");
  }
}

void note_notation(spNote note, cxnode &node) {
  for (const auto &subnode : node) {
    TRY("slur") note_notation_slur(note, subnode);
    else TRY("articulations") note_notation_articulations(note, subnode);
    else PARSEFAIL("note_notation");
  }
}

void note(Bar &bar, cxnode &node) {
  auto note = std::make_unique<Note>();
  for (const auto &subnode : node) {
    TRY("pitch") note_pitch(note.get(), subnode);
    else TRY("duration"); // this looks like MIDI shit, which can determine from
                          // the 'type' instead
    else TRY("voice") note_voice(note.get(), subnode);
    else TRY("type") note_type(note.get(), subnode);
    else TRY("stem"); // disregard, lilypond will do this
    else TRY("beam"); // and this
    else TRY("notations") note_notation(note.get(), subnode);
    else TRY("dot") note_dot(note.get());
    else PARSEFAIL("note");
  }
  bar.append_notation(std::move(note));
}

void sound(Bar &, cxnode &node) {
  for (const auto &attribute : node.attributes()) {
    util::warning("Not yet implemented: sound attrib ", attribute.name(), "=",
        attribute.value());
  }
}

ClefType clef_sign(cxnode &node) {
  std::string cs = node.child_value();
  return enums::from_string<notation::ClefType>(cs);
}

int clef_line(cxnode &node) {
  auto cl = std::atoi(node.child_value());
  return cl;
}

std::unique_ptr<Clef> clef(
    Bar &, cxnode &node) { // TODO finish implementing clef parser
  std::string name{"musicxml clef"};
  ClefType clefSymbol{ClefType::G};
  int clefLine{5};

  for (const auto &subnode : node) {
    TRY("sign") clefSymbol = clef_sign(subnode);
    else TRY("line") clefLine = clef_line(subnode);
    else PARSEFAIL("clef");
  }
  // musicxml ledger lines are counted from the bottom, as opposed to
  // part.ledgerOffset, which is counted from the centre, counting upwards.
  // plus, only counts lines, not spaces (ie -1, /2)
  int ledgerOffset{0};
  switch (clefSymbol) {
  case notation::ClefType::G:
    ledgerOffset = (clefLine * 2) - 1 - 3;
    break;
  case notation::ClefType::C:
    ledgerOffset = (clefLine * 2) - 1 - 5;
    break;
  case notation::ClefType::F:
    ledgerOffset = (clefLine * 2) - 1 - 7;
    break;
  default:
    util::error("Confused by clef type:", enums::to_string(clefSymbol));
    throw std::runtime_error{"readers/musicxml::clef"};
  }

  Pitch clefPitch = notation::get_pitch_for_clef(clefSymbol);
  int iter = ledgerOffset;
  while (iter != 0) {
    if (iter < 0) {
      clefPitch = clefPitch.get_one_sound_up();
      iter++;
    } else {
      clefPitch = clefPitch.get_one_sound_down();
      iter--;
    }
  }

  throw std::runtime_error{"readers/musicxml::clef"};
  //  auto clef = std::make_unique<Clef>(clefPitch, clefSymbol, name,
  //  ledgerOffset);
  // return clef;
}

std::unique_ptr<notation::TimeSignature> attributes_time(Bar &, cxnode &node) {
  auto beats{4};
  auto beat_type{4};
  for (const auto &subnode : node) {
    TRY("beats") beats = std::atoi(subnode.child_value());
    else TRY("beat-type") beat_type = std::atoi(subnode.child_value());
    else PARSEFAIL("time");
  }
  auto timesig = std::make_unique<notation::TimeSignature>(beats, beat_type);
  return timesig;
}

void attributes(Bar &bar, cxnode &node) {
  for (const auto &subnode : node) {
    MISSING("divisions");
    else MISSING("key");
    else TRY("time") {
      auto headerimesignature = attributes_time(bar, subnode);
      bar.get_part().get_movement().add_bar_time_signature(
          0, std::move(headerimesignature));
    }
    else TRY("clef") {
      auto headerclef = clef(bar, subnode);
      bar.get_part().add_clef_at_bar(0, std::move(headerclef));
    }
    else PARSEFAIL("attributes");
  }
}

void measure_barline_ending(Bar &bar, cxnode &node) { // ie. volta
  if (util::compare(node.attribute("type").as_string(), "stop")) {
    return; // we will work out by ourselves where the voltas stop
  }
  int voltaNumber = node.attribute("number").as_int(-1);
  if (voltaNumber == -1) {
    util::error("Failed to read volta number");
    throw std::runtime_error{"readers/musicxml::measure_barline_ending"};
  }
  auto barnumber = bar.get_bar_number();
  auto &movement = bar.get_part().get_movement();

  movement.set_volta(barnumber, Volta{static_cast<size_t>(voltaNumber)});
}

void measure_barline_repeat(Bar &bar, cxnode &node) {
  if (util::compare(node.attribute("direction").as_string(), "backward")) {
    bar.set_bar_end_style(BarStyle::Repeat);
  } else if (util::compare(
                 node.attribute("direction").as_string(), "forward")) {
    bar.set_bar_start_style(BarStyle::Repeat);
  } else {
    util::error(
        "Unknown bar ending: ", node.attribute("direction").as_string());
    throw std::runtime_error("readers/musicxml::measure_barline_repeat");
  }
}

void measure_barline(Bar &bar, cxnode &node) {
  for (const auto &subnode : node) {
    TRY("ending") measure_barline_ending(bar, subnode); // ie. volta
    else IGNORING("bar-style");
    else TRY("repeat") measure_barline_repeat(bar, subnode);
    else PARSEFAIL("measure_barline");
  }
}

void measure(notation::Part &part, cxnode &node) {
  auto &bar = part.add_bar_at_end();
  auto expected = part.size();
  auto got = static_cast<size_t>(
      node.attribute("number").as_ullong(bar.get_bar_number()));
  if (expected != got) {
    util::error("Error adding bars: tried to add bar ", got,
        " when expecting to add bar ", expected);
    throw std::runtime_error{"readers/musicxml::measure"};
  }
  for (const auto &subnode : node) {
    TRY("attributes") attributes(bar, subnode);
    else TRY("note") note(bar, subnode);
    else IGNORING("print");
    else TRY("sound") sound(bar, subnode);
    else MISSING("direction");
    else TRY("barline") measure_barline(bar, subnode);
    else PARSEFAIL("measure");
  }
}

void part(Movement *movement, cxnode &node) {
  auto &part = movement->add_part_at_bottom();
  for (const auto &subnode : node) {
    TRY("measure") measure(part, subnode);
    else PARSEFAIL("part")
  }
}

void score_partwise(Movement &movement, cxnode &node) {
  auto threads = std::vector<std::thread>{};

  for (const auto &subnode : node) {
    IGNORING("defaults");
    else MISSING("identification");
    else TRY("movement-title") movement_title(movement, subnode);
    else TRY("part") {
      auto thread = std::thread(part, &movement, subnode);
      threads.push_back(std::move(thread));
    }
    else MISSING("part-list");
    else PARSEFAIL("score-partwise")
  }

  for (auto &thread : threads)
    thread.join();
  util::debug("Joined ", threads.size(), " thread(s)");
}

Movement &root(cxnode &node, Movement &rval) {
  for (const auto &subnode : node) {
    TRY("score-partwise") score_partwise(rval, subnode);
    else PARSEFAIL("root")
  }
  return rval;
}

} // namespace

Movement &parse_music_xml(const std::string &file, notation::Book &book) {
  util::debug("Parsing MusicXml", file);
  xml_document doc;
  auto result = doc.load_file(file.c_str());
  if (!result) {
    util::error("result of loading ", file, " : ", result.description());
    throw std::runtime_error{"readers/musicxml::parse"};
  }

  util::info("result of loading ", file, " : ", result.description());

  auto &movement = book.add_movement();
  auto &rval = root(doc, movement);
  return rval;
}

} // namespace musicxml
