#pragma once

#include <memory>

namespace notation {
class Book;
class Movement;
} // namespace notation

/** \namespace musicxml \brief Functions for working with MusicXml files
 * <a href="http://">MusicXml</a> */
namespace musicxml {
notation::Movement &parse_music_xml(
    const std::string &file, notation::Book &book);
}
