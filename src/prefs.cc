#include "prefs.h"

#include "util/logging.h"

#include <thread>

namespace {
size_t get_machine_threads() {
  return std::thread::hardware_concurrency();
}
} // namespace

Prefs::Prefs() : mThreadCount{get_machine_threads() + 1} {
  util::debug("Initialised preferences"); // TODO load from disk
}

Prefs::~Prefs() {
  util::debug("Unloaded preferences"); // TODO save to disk
}

Prefs &Prefs::get_instance() {
  static Prefs instance{};
  return instance;
}

Pref<size_t> Prefs::thread_count() const {
  return mThreadCount;
}

Pref<size_t> Prefs::default_zoom() const {
  return mDefaultZoom;
}
