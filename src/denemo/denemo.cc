#include "denemo.h"

#include "dpostfix.h"
#include "notation/articulations.h"
#include "notation/bar.h"
#include "notation/barstyle.h"
#include "notation/book.h"
#include "notation/clef.h"
#include "notation/cleftype.h" // for ClefType
#include "notation/dynamic.h"
#include "notation/instrument.h"
#include "notation/key.h"
#include "notation/movement.h"
#include "notation/movementkey.h" // for MovementKey
#include "notation/note.h"
#include "notation/part.h"
#include "notation/rest.h"
#include "notation/span.h"
#include "notation/tempo.h"
#include "notation/timesignature.h"
#include "notation/tuplet.h"
#include "schemey.h"
#include "util/assert.h"
#include "util/hash.h"
#include "util/logging.h"
#include "util/string.h"
#include "xml/node.h"

#include <cstring>
#include <future>
#include <iostream>
#include <stdexcept>

namespace denemo {

using namespace notation;
using cxnode = const xml::Node;
using spPart = notation::Part *;

namespace {

void note_directives(cxnode &node, notation::TimedNotation *note);

void notes_note(cxnode &node, notation::Note *note) {
  auto sound = notation::Sound::C;
  auto accidental = notation::Accidental::Natural;
  auto octave = 4;
  node.once("middle-c-offset", [&](const xml::Node &subnode) {
    auto offset = subnode.get_value<int>();
    if (offset >= 0) {
      sound = enums::from_ordinal<notation::Sound>(offset % 7);
      octave = offset / 7 + 4;
    } else {
      sound = enums::from_ordinal<notation::Sound>(
          (offset + 7000) % 7); // hopefully enough
      octave = offset / 7 + 3;
    }
  });
  node.once("accidental", [&](const xml::Node &subnode) {
    auto name = subnode.get_attribute<std::string>("name");
    switch (util::lowercase_hash(name)) {
    case util::hash("sharp"):
      accidental = notation::Accidental::Sharp;
      break;
    case util::hash("flat"):
      accidental = notation::Accidental::Flat;
      break;
    case util::hash("natural"):
      accidental = notation::Accidental::Natural;
      break;
    case util::hash("double-sharp"):
      accidental = notation::Accidental::DoubleSharp;
      break;
    case util::hash("double-flat"): // TODO what is a flat-flat in Denemo`
      accidental = notation::Accidental::FlatFlat;
      break;
    default:
      util::error("Unknown accidental:", name);
    }
  });
  node.once("directives",
      [&](const xml::Node &subnode) { note_directives(subnode, note); });
  note->pitch(notation::Pitch(sound, accidental, octave));
}

void notes(cxnode &node, notation::Note *note) {
  // TODO a node.many("note") is a chord - need to implement
  // xml::Node::get_count(name)
  node.many(
      "note", [&](const xml::Node &subnode) { notes_note(subnode, note); });
}

notation::Duration note_duration(cxnode &node) {
  auto base = node.get_attribute<std::string>("base");
  auto length{4};
  auto dots{0};
  switch (util::lowercase_hash(base)) {
  case util::hash("sixty-fourth"):
    length = 64;
    break;
  case util::hash("thirty-second"):
    length = 32;
    break;
  case util::hash("sixteenth"):
    length = 16;
    break;
  case util::hash("eighth"):
    length = 8;
    break;
  case util::hash("quarter"):
    length = 4;
    break;
  case util::hash("half"):
    length = 2;
    break;
  case util::hash("whole"):
    length = 1;
    break;
  default:
    util::error("Unknown note length:", base);
  }
  node.once("dots",
      [&](const xml::Node &subnode) { dots = subnode.get_value<int>(); });
  return Duration{length, dots};
}

void note_directive_tag(cxnode &node, TimedNotation *tn) {
  auto tag = node.get_value<std::string>();
  switch (util::lowercase_hash(tag)) {
  case util::hash("togglestaccato"):
    tn->add_articulation(Articulation::Staccato);
    break;
  case util::hash("toggleaccent"):
    tn->add_articulation(Articulation::Accent);
    break;
  case util::hash("togglefermata"):
    tn->add_articulation(Articulation::Fermata);
    break;
  case util::hash("warnaccidental"): {
    if (tn->get_notation_type() == NotationType::Rest) {
      util::error("Tried to add an accidental to a rest?");
    } else {
      auto note = dynamic_cast<Note *>(tn);
      note->set_courtesy_accidental(true);
    }
  } break;
  case util::hash("cresc"): // nb. useless - doesn't tell us when they stop
  case util::hash("beam"):
  case util::hash("dim"):
    break;
  case util::hash("tremolo"): // nb. useless - doesn't include the frequency,
                              // which is in the postfix as :freq
    break;
  case util::hash(
      "wholemeasurerest"): // we check this properly when outputting lilypond]
    break;
  default:
    util::error("Denemo: unknown note->directive->tag: ", tag);
  }
}

void note_directive_postfix(cxnode &node, TimedNotation *note) {
  auto value = node.get_value<std::string>();
  auto postfix = util::strip(value);
  postfix::apply_note_postfix(*note, postfix);
}

void measure_directive_postfix(cxnode &node, Bar &bar) {
  auto value = node.get_value<std::string>();
  auto postfix = util::strip(value);
  postfix::apply_bar_postfix(bar, postfix);
}

void note_directive(cxnode &node, notation::TimedNotation *note) {
  node.once("tag",
      [&](const xml::Node &subnode) { note_directive_tag(subnode, note); });
  node.once("postfix",
      [&](const xml::Node &subnode) { note_directive_postfix(subnode, note); });
  node.ignore({"prefix", "graphic_name", "gx", "gy", "tx", "ty", "grob",
      "midibytes", "override", "display", "minpixels"});
}

void note_directives(cxnode &node, notation::TimedNotation *note) {
  node.many("directive",
      [&](const xml::Node &subnode) { note_directive(subnode, note); });
}

notation::Grace grace_for_name(const std::string &name) {
  switch (util::lowercase_hash(name)) {
  case util::hash("true"):
    return notation::Grace::Grace;
  case util::hash("acciaccatura"):
    return notation::Grace::Acciaccatura;
  case util::hash("appoggiatura"): // TODO presumably
    return notation::Grace::Appoggiatura;
  case util::hash(""):
  case util::hash("false"):
    return notation::Grace::None;
  }
  util::error("Denemo: unknown grace mark '", name, "'");
  return notation::Grace::None;
}

void measure_chord(cxnode &node, Bar &bar) {
  auto note = std::make_unique<notation::Note>();
  auto grace = node.get_attribute<std::string>("grace");

  node.once("duration", [&](const xml::Node &subnode) {
    auto duration = note_duration(subnode);
    if (!grace.empty()) {
      duration = notation::Duration(duration.get_base_duration(),
          duration.get_dots(), grace_for_name(grace));
    }
    note->set_duration(duration);
  });
  node.once("slur-begin",
      [&](const xml::Node &) { note->add_span_start(notation::Span::Slur); });
  node.once("slur-end",
      [&](const xml::Node &) { note->add_span_end(notation::Span::Slur); });
  node.once("dim-begin", [&](const xml::Node &) {
    note->add_span_start(notation::Span::Diminuendo);
  });
  node.once("dim-end", [&](const xml::Node &) {
    note->add_span_end(notation::Span::Diminuendo);
  });
  node.once("cresc-begin", [&](const xml::Node &) {
    note->add_span_start(notation::Span::Crescendo);
  });
  node.once("cresc-end", [&](const xml::Node &) {
    note->add_span_end(notation::Span::Crescendo);
  });
  node.once(
      "notes", [&](const xml::Node &subnode) { notes(subnode, note.get()); });
  node.once("directives",
      [&](const xml::Node &subnode) { note_directives(subnode, note.get()); });
  node.once("tie", [&](const xml::Node &) { note->tie_to_next(); });
  bar.append_notation(std::move(note));
}

void measure_rest(cxnode &node, Bar &bar) {
  auto note = std::make_unique<notation::Rest>();
  auto grace = node.get_attribute<std::string>("grace");

  node.once("duration", [&](const xml::Node &subnode) {
    auto duration = note_duration(subnode);
    if (!grace.empty()) {
      duration = notation::Duration(duration.get_base_duration(),
          duration.get_dots(), grace_for_name(grace));
    }
    note->set_duration(duration);
  });
  node.once("directives",
      [&](const xml::Node &subnode) { note_directives(subnode, note.get()); });
  node.ignore({"ticks"}); //  MIDI length?
  bar.append_notation(std::move(note));
}

void measure_directives(cxnode &node, Bar &bar) {
  auto barNumber = bar.get_bar_number();
  node.once("tag", [&](const xml::Node &subnode) {
    auto tagname = subnode.get_value<std::string>();
    switch (util::lowercase_hash(tagname)) {
    case util::hash("dynamictext"):
    case util::hash("textannotation"):
    case util::hash("metronomemark"):
      break; // TODO these barstyles are handled differently
    case util::hash("repeatend"):
      bar.get_part().get_movement().set_bar_end_style(
          barNumber, notation::BarStyle::Repeat);
      break;
    case util::hash("doublebarline"):
      bar.get_part().get_movement().set_bar_end_style(
          barNumber, notation::BarStyle::Double);
      break;
    case util::hash("multimeasurerests"):
      break; //  TODO presumably ignore?  We'll calculate these ourselves.
    case util::hash("rehearsalmark"):
      bar.get_part().get_movement().add_rehearsal_mark_at_bar(barNumber);
      break;
    case util::hash("opennthtimebar"): // TODO most important - calculate voltas
    case util::hash("endvolta"):       // TODO most important - calculate voltas
      util::debug("Denemo: ignoring volta");
      break;
    case util::hash("repeatstart"): // TODO implement more bar end styles
    case util::hash("upbeat"):
      break;
    default:
      util::error("Denemo: Unknown bar end:", tagname, " at bar ", barNumber);
    }
  });
  node.once("postfix", [&](const xml::Node &subnode) {
    measure_directive_postfix(subnode, bar);
  });
  node.ignore({"prefix", "display", "graphic_name", "minpixels", "midibytes",
      "grob", "gx", "gy", "tx", "ty", "override", "data"});
  //  TODO check if pre/postfix are ever interesting
}

std::unique_ptr<Key> key_signature(cxnode &node) {
  auto rval = std::unique_ptr<Key>{};
  node.once("modal-key-signature", [&](const xml::Node &subnode) {
    auto notename = subnode.get_attribute<std::string>("note-name");
    auto mode = subnode.get_attribute<std::string>("mode");
    auto accidentals{0};
    if (mode == "major") {
      accidentals = Key::get_accidentals_in_major_key(notename);
    } else if (mode == "minor") {
      accidentals = Key::get_accidentals_in_minor_key(notename);
    } else {
      util::error("Unknown mode for key:", mode);
    }
    rval = std::make_unique<Key>(accidentals);
  });
  node.ignore({"directives"});
  if (rval)
    return rval;
  util::error("Failed to parse key signature");
  throw std::runtime_error{"readers/denemo::key_signature"};
}

std::unique_ptr<TimeSignature> simple_time_signature(cxnode &node) {
  auto numerator{4};
  auto denominator{4};
  node.once("numerator",
      [&](const xml::Node &subnode) { numerator = subnode.get_value<int>(); });
  node.once("denominator", [&](const xml::Node &subnode) {
    denominator = subnode.get_value<int>();
  });
  return std::make_unique<TimeSignature>(numerator, denominator);
}

std::unique_ptr<TimeSignature> time_signature(cxnode &node) {
  auto rval = std::unique_ptr<TimeSignature>{};
  node.once("simple-time-signature",
      [&](const xml::Node &subnode) { rval = simple_time_signature(subnode); });
  node.ignore({"directives"});
  if (rval)
    return rval;
  throw std::runtime_error{"couldn't parse time signature"};
}

std::unique_ptr<Tuplet> tuplet_start(cxnode &node) {
  auto num{0};
  auto den{1};
  node.once("multiplier", [&](const xml::Node &n) {
    n.once(
        "numerator", [&](const xml::Node &sn) { num = sn.get_value<int>(); });
    n.once(
        "denominator", [&](const xml::Node &sn) { den = sn.get_value<int>(); });
  });
  return std::make_unique<Tuplet>(geom::Ratio{num, den});
}

std::unique_ptr<Clef> get_clef_for_name(const std::string &name) {
  switch (util::lowercase_hash(name)) {
  case util::hash("treble"):
    return Clef::Treble();
  case util::hash("alto"):
    return Clef::Alto();
  case util::hash("bass"):
    return Clef::Bass();
  case util::hash("tenor"):
    return Clef::Tenor();
  }
  util::error("Unknown clef:", name);
  throw std::runtime_error(util::concat("unknown clef: ", name));
}

void measure(cxnode &node, Bar &bar) {
  node.every([&](const xml::Node &subnode) {
    switch (subnode.get_hash()) {
    case util::hash("chord"):
      measure_chord(subnode, bar);
      break;
    case util::hash("rest"):
      measure_rest(subnode, bar);
      break;
    case util::hash("lily-directive"): // repeats...
      measure_directives(subnode, bar);
      break;
    case util::hash("key-signature"): {
      auto keySignature = key_signature(subnode);
      auto barNumber = bar.get_bar_number();
      bar.get_part().add_key_at_bar(barNumber, std::move(keySignature));
    } break;
    case util::hash("time-signature"): {
      auto timeSignature = time_signature(subnode);
      auto barNumber = bar.get_bar_number();
      bar.get_part().get_movement().add_bar_time_signature(
          barNumber, std::move(timeSignature));
    } break;
    case util::hash("tuplet-start"): {
      auto tuplet = tuplet_start(subnode);
      bar.append_notation(std::move(tuplet));
    } break;
    case util::hash("tuplet-end"): {
      auto endTuplet = std::make_unique<Tuplet>();
      bar.append_notation(std::move(endTuplet));
    } break;
    case util::hash("clef"): {
      auto clefname = subnode.get_attribute<std::string>("name");
      auto clef = get_clef_for_name(clefname);
      bar.append_notation(std::move(clef));
    } break;
    case util::hash("stem-directive"): {
      subnode.clear_unused();
    } break;
    default:
      util::error("Can't parse symbol in measure:", subnode.get_name());
    }
  });
}

void measures(cxnode &node, notation::Part &part) {
  node.many("measure", [&](const xml::Node &subnode) {
    auto &bar = part.add_bar_at_end();
    measure(subnode, bar);
  });
  for (auto &bar : part) {
    // make sure that all of the dynamic marks are up to date
    bar.apply_postfixes();
  }
}

void ivp_clef(cxnode &node, notation::Part &part) { //  TODO more clefs
  auto clefname = node.get_attribute<std::string>("name");
  node.ignore({"directives"});
  std::unique_ptr<Clef> clef = get_clef_for_name(clefname);
  part.add_clef_at_bar(0, std::move(clef));
}

void initial_voice_params(cxnode &node, notation::Part &part) {
  node.once("clef", [&](const xml::Node &subnode) { ivp_clef(subnode, part); });
  node.once("key-signature", [&](const xml::Node &subnode) {
    auto starting_key = key_signature(subnode);
    part.add_key_at_bar(0, std::move(starting_key));
  });
  node.once("time-signature", [&](const xml::Node &subnode) {
    auto timeSignature = time_signature(subnode);
    part.get_movement().add_bar_time_signature(0, std::move(timeSignature));
  });
  node.ignore({"staff-ref"});
}

void voice_info(cxnode &node, notation::Part &part) {
  node.once("voice-name", [&](const xml::Node &subnode) {
    auto partname = subnode.get_value<std::string>();
    part.set_part_name(partname);
  });
  node.once("first-measure-number",
      [&](const xml::Node &subnode) { // TODO implement first-measure-number?
        auto firstMeasureNumber = subnode.get_value<int>();
        if (firstMeasureNumber != 1)
          util::error("First measure number in a Denemo part is not 1");
      });
}

constexpr int middleCOffset = 60;

void voice_props(cxnode &node, notation::Part &part) {
  auto staffRangeLo{0};   // midi note
  auto staffRangeHi{127}; // midi note
  auto transpose{0};      // concert pitch
  auto name = std::string{"Denemo instrument"};
  node.once("staff-range-lo",
      [&](const xml::Node &subnode) { // relative to middle C
        staffRangeLo = subnode.get_value<int>() + middleCOffset;
      });
  node.once("staff-range-hi",
      [&](const xml::Node &subnode) { // relative to middle C
        staffRangeHi = subnode.get_value<int>() + middleCOffset;
      });
  node.once("transpose", [&](const xml::Node &subnode) { // in semitones
    transpose = subnode.get_value<int>();
  });
  node.once("instrument", [&](const xml::Node &subnode) { // generic name
    name = subnode.get_value<std::string>();
  });
  node.ignore({"staff-directives", "staff-range", "voice-control",
      "number-of-lines", "device-port", "volume", "override_volume", "mute",
      "midi_prognum", "midi_channel", "hasfigures", "hasfakechords",
      "voice-directives", "staff-color"});
  node.ignore({"clef-directives"}); // TODO has proper name
  auto instrument = notation::Instrument{name, InstrumentType::Piano,
      notation::Pitch{staffRangeLo}, notation::Pitch{staffRangeHi}, transpose};
  part.set_instrument(instrument);
};

void voice(cxnode &node, notation::Part &part) {
  node.once("initial-voice-params",
      [&](const xml::Node &subnode) { initial_voice_params(subnode, part); });
  node.once(
      "measures", [&](const xml::Node &subnode) { measures(subnode, part); });
  node.once("voice-info",
      [&](const xml::Node &subnode) { voice_info(subnode, part); });
  node.once("voice-props",
      [&](const xml::Node &subnode) { voice_props(subnode, part); });
}

void voices(cxnode &node, Movement &movement) {
  node.many("voice", [&](const xml::Node &subnode) {
    auto &part = movement.append_part();
    voice(subnode, part);
  });
}

void movementcontrol_directive(cxnode &node, Movement &movement) {
  auto tag = std::string{};
  node.once("tag", [&](const xml::Node &subnode) {
    tag = subnode.get_value<std::string>();
  });
  node.once("data", [&](const xml::Node &subnode) {
    auto value = subnode.get_value<std::string>();
    switch (util::hash(tag)) {
    case util::hash("TitledPiece"):
      movement.set(MovementKey::title, value);
      break;
    default:
      util::error("Unknown movementcontrol_directive: ", tag);
    }
  });
  node.ignore({"override", "display", "prefix"});
}

void movementcontrol_directives(cxnode &node, Movement &movement) {
  node.many("directive", [&](const xml::Node &subnode) {
    movementcontrol_directive(subnode, movement);
  });
}

void score_movement(cxnode *nodeptr, Movement *movement) {
  assert::is_not_null(movement);
  auto &node = *nodeptr;
  node.once("movementcontrol-directives", [&](const xml::Node &subnode) {
    movementcontrol_directives(subnode, *movement);
  });
  node.once(
      "voices", [&](const xml::Node &subnode) { voices(subnode, *movement); });
  node.ignore({"edit-info", "header-directives",
      "score-info", // TODO has midi tempo in it
      "staves",     // TODO part count, if we want to preallocate
      "layout-directives", "staves", "edit-info", "score-info"});
}

void scoreheader_directive_titles(cxnode &node, Book *book) {
  auto schemeEnvironment = schemey::get_standard_environment();
  schemeEnvironment->add_function(
      "cons", [&](const std::vector<schemey::Cell> &cells) -> std::string {
        auto key = cells.at(0).evaluate();
        auto value = cells.at(1).evaluate();
        if (value == "#f")
          return "EMPTY";
        try {
          auto bookkey = enums::from_string<notation::BookKey>(key);
          book->set(bookkey, value);
          return "OK";
        } catch (std::exception &) { //  no such key?
          return {key};
        }
      });
  auto nodeValue = node.get_value<std::string>();
  auto parsedScheme = parse_scheme_string(schemeEnvironment, nodeValue);
  //   util::info("Parsed some scheme: ", parsedScheme.get_display_string());
  auto rval = parsedScheme.evaluate();
  util::info("Scheme value equalled: ", rval);
}

void scoreheader_directive(cxnode &node, Book *piece) {
  auto tag = std::string{};
  node.once("tag", [&](const xml::Node &subnode) {
    tag = subnode.get_value<std::string>();
  });
  node.once("data", [&](const xml::Node &subnode) {
    switch (util::hash(tag)) {
    case util::hash("ScoreTitles"):
      scoreheader_directive_titles(subnode, piece);
      break;
    default:
      util::error("Denemo: Unhandled scoreheader_directive:", tag);
    }
  });
  node.ignore({"override", "display", "postfix"});
}

void scoreheader_directives(cxnode &node, Book *piece) {
  node.many("directive",
      [&](const xml::Node &subnode) { scoreheader_directive(subnode, piece); });
}

void score(cxnode &node, Book *piece) {
  auto threads = std::vector<std::thread>{};
  auto expectedMovementCount = size_t{0};

  node.once("movement-number",
      [&](const xml::Node &subnode) { // lets us know how many movements there
                                      // are - but that's not interesting
        expectedMovementCount = subnode.get_value<size_t>();
      });
  node.many("movement", [&](const xml::Node &subnode) {
    //     util::info("Parsing movement ",(piece->get_movement_count()+1));
    auto &movement = piece->add_movement();
    auto thread = std::thread(score_movement, &subnode, &movement);
    threads.push_back(std::move(thread));
  });
  node.once("scoreheader-directives", [&](const xml::Node &subnode) {
    scoreheader_directives(subnode, piece);
  });

  node.ignore(
      {"lilycontrol", "paper-directives", "thumbnail", "custom_scoreblock"});

  for (auto &thread : threads)
    thread.join();
  util::debug("Joined all threads");

  auto parsedMovementCount = piece->size();

  if (expectedMovementCount != parsedMovementCount)
    util::error("Expected ", expectedMovementCount, " movements and got ",
        parsedMovementCount);
}

} // namespace

std::unique_ptr<notation::Book> parse_denemo(const std::string &file) {
  auto doc = xml::Node::from_document(file);
  auto rval = std::make_unique<notation::Book>();
  score(*doc, rval.get());
  util::info("Document parsed");
  return rval;
}

} // namespace denemo
