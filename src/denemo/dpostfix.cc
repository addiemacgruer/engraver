#include "dpostfix.h"

#include "notation/articulations.h" // for Articulation
#include "notation/bar.h"
#include "notation/barstyle.h"
#include "notation/dynamic.h"
#include "notation/markup.h"
#include "notation/movement.h"
#include "notation/note.h"
#include "notation/part.h"
#include "notation/tempo.h"
#include "notation/timednotation.h"
#include "util/hash.h"
#include "util/logging.h"
#include "util/string.h"

#include <string_view>

namespace {

enum class Scope { Part, Movement };

notation::Note &cast_note(notation::TimedNotation &tn) {
  return dynamic_cast<notation::Note &>(tn);
}

template <class T>
bool is_scheme_comment(const T &string) {
  return util::starts_with(string, "%{") && util::ends_with(string, "%}");
}

void add_bar_information(notation::Bar &bar, const std::string &postfix) {
  auto barNumber = bar.get_bar_number();
  switch (util::hash(postfix)) {
  case util::hash("\".|:\""):
    bar.get_part().get_movement().set_bar_start_style(
        barNumber + 1, notation::BarStyle::Repeat);
    break;
  case util::hash("\":|.\""):
    bar.get_part().get_movement().set_bar_end_style(
        barNumber, notation::BarStyle::Repeat);
    break;
  default:
    util::error(
        "Unknown lilypond bar style:", postfix, " at bar number ", barNumber);
  }
}

void add_tempo_information(notation::Bar &bar, const std::string &postfix) {
  auto tempo = std::make_unique<notation::Tempo>(postfix);
  auto barNumber = bar.get_bar_number();
  bar.get_part().get_movement().add_bar_tempo(barNumber, std::move(tempo));
};

void add_markup_information(notation::Bar &bar, const std::string &postfix,
    Scope scope, notation::MarkupPosition position) {
  auto markup = std::make_unique<notation::Markup>(
      postfix); // TODO strip off { \italic "rit." } or etc from this
  markup->set_position(position);
  switch (scope) {
  case Scope::Movement: {
    auto barNumber = bar.get_bar_number();
    bar.get_part().get_movement().add_bar_markup(barNumber, std::move(markup));
  } break;
  case Scope::Part: {
    auto barNumber = bar.get_bar_number();
    bar.get_part().add_bar_markup(barNumber, std::move(markup));
  } break;
  default:
    util::error("Ballsed up a markup addition: ", postfix);
  }
  return;
};

geom::Ratio get_ratio_from_spec(const std::string &spec) {
  // TODO 256 * 8 in the spec means 8* 1/256 notes = 1/32.
  if (spec.find('*') == std::string::npos) {
    return geom::Ratio{1, std::atoi(spec.c_str())};
  }
  auto part = spec.substr(spec.find('*') + 1); // TODO wrong
  return geom::Ratio{1, std::atoi(part.c_str())};
};

void add_movement_anacrusis(
    notation::Movement &movement, const std::string &spec) {
  auto ratio = get_ratio_from_spec(spec);
  movement.set_anacrusis(ratio);
};

} // namespace

namespace {

Scope determine_scope(std::string_view &postfix) {
  if (util::starts_with(postfix, "s8*0")) { // applies to movement
    postfix.remove_prefix(4);
    return Scope::Movement;
  }
  return Scope::Part;
}

notation::MarkupPosition determine_position(std::string_view &postfix) {
  switch (postfix.at(0)) {
  case '^':
    postfix.remove_prefix(1);
    return notation::MarkupPosition::Above;
  case '-':
    postfix.remove_prefix(1);
    return notation::MarkupPosition::Either;
  case '_':
    postfix.remove_prefix(1);
    return notation::MarkupPosition::Below;
  }
  return notation::MarkupPosition::Either;
}

std::string determine_command(std::string_view &postfix) {
  auto space = postfix.find(' ');
  auto command = std::string{};

  if (space == std::string::npos) {
    command = std::string{postfix};
    postfix.remove_prefix(postfix.size());
  } else {
    command = std::string{postfix.substr(0, space)};
    postfix = postfix.substr(space + 1);
  }
  return command;
}

auto make_dynamic_postfix(const std::string &dynamicString, notation::Bar &bar,
    size_t notationlength) {
  return [dynamicString, &bar, notationlength]() {
    auto dynamic = std::make_unique<notation::Dynamic>(dynamicString);
    notation::Notation *note;
    if (notationlength >= bar.size()) {
      // actually applies to the first note in the next bar?
      auto nextBarNumber = bar.get_bar_number() + 1;
      if (nextBarNumber >= bar.get_part().get_bar_count()) {
        util::warning("Tried to add a dynamic to note ", notationlength,
            " in bar ", bar.get_bar_number(),
            ", but that's off the end of the piece");
        return;
      }
      auto &nextbar = bar.get_part().get_bar(nextBarNumber);
      if (nextbar.empty()) {
        util::warning("Tried to add a dynamic to note 0 in bar ", nextBarNumber,
            ", but that bar is empty");
        return;
      }
      try {
        note = &nextbar.at(0);
      } catch (std::exception &) {
        util::error("Failed to get the first note of bar ", nextBarNumber,
            ", dynamic not correctly applied (", dynamicString, ")");
        return;
      }
    } else {
      note = &bar.at(notationlength);
    }
    switch (note->get_notation_type()) {
    case notation::NotationType::Note: {
      auto *n = dynamic_cast<notation::Note *>(note);
      n->set_dynamic(std::move(dynamic));
    } break;
    default:
      util::error(
          "Tried to add a postfix dynamic to something which is not a note");
    }
  };
}
} // namespace
namespace denemo::postfix {

void apply_bar_postfix(notation::Bar &bar, const std::string &originalpostfix) {
  auto postfix = std::string_view{originalpostfix};
  if (postfix.empty() || is_scheme_comment(postfix))
    return;

  auto scope = determine_scope(postfix);
  auto position = determine_position(postfix);

  if (postfix.at(0) != '\\') { // oops, more prefixes
    util::error("Couldn't parse postfix: ", originalpostfix);
    return;
  }
  postfix.remove_prefix(1);

  auto command = determine_command(postfix);

  switch (util::lowercase_hash(command)) {
    // nb. dynamic marks all *precede* the notes they apply to.  How annoying.
  case util::hash("fff"):
  case util::hash("ff"):
  case util::hash("f"):
  case util::hash("p"):
  case util::hash("pp"):
  case util::hash("ppp"):
  case util::hash("mf"):
  case util::hash("mp"):
  case util::hash("fp"):
  case util::hash("sfz"): {
    auto notationlength = bar.size();
    auto commandstring = std::string{command};
    bar.add_postfix(make_dynamic_postfix(commandstring, bar, notationlength));
    return;
  }
  case util::hash("mark"):
    bar.get_part().get_movement().add_rehearsal_mark_at_bar(
        bar.get_bar_number());
    return;
  case util::hash("tempo"):
    add_tempo_information(bar, std::string{postfix});
    return;
  case util::hash("set"):
    // automatic concatenation of empty bars, which we calculate
    // on lilypond output
    if (util::starts_with(postfix, "Score.skipBars"))
      return;
    // calculates voltas, but we know this from the tag
    if (util::starts_with(postfix, "Score.repeatCommands"))
      return;
    break;
  case util::hash("markup"):
    add_markup_information(bar, std::string{postfix}, scope, position);
    return;
  case util::hash("bar"):
    add_bar_information(bar, std::string{postfix});
    return;
  case util::hash("partial"):
    add_movement_anacrusis(bar.get_part().get_movement(), std::string{postfix});
    return;
  }

  util::error("Denemo bar postfix: ignored command '", command,
      "' w/ postfix '", postfix, "'");
}

void apply_note_postfix(
    notation::TimedNotation &note, const std::string &postfix) {
  if (postfix.empty() || is_scheme_comment(postfix))
    return;
  switch (util::lowercase_hash(postfix)) {
  case util::hash("\\fff"):
  case util::hash("\\ff"):
  case util::hash("\\f"):
  case util::hash("\\p"):
  case util::hash("\\pp"):
  case util::hash("\\ppp"):
  case util::hash("\\mf"):
  case util::hash("\\mp"):
  case util::hash("\\fp"):
  case util::hash("\\sfz"): {
    auto dynamictext = postfix.substr(1);
    auto dynamic = std::make_unique<notation::Dynamic>(dynamictext);
    cast_note(note).set_dynamic(std::move(dynamic));
    return;
  }
  case util::hash("-\\staccato"): { // why this and ToggleStaccato?  a mystery
    note.add_articulation(notation::Articulation::Staccato);
    return;
  }
  case util::hash("-\\accent"): { // a similar duplication
    note.add_articulation(notation::Articulation::Accent);
    return;
  }
  case util::hash("-\\fermata"): { // a similar duplication
    note.add_articulation(notation::Articulation::Fermata);
    return;
  }
  case util::hash("!"): { // a similar duplication \\ TODO denemo import: is
                          // this end crescendo / diminuendo?
    cast_note(note).set_courtesy_accidental(true);
    return;
  }
  case util::hash(
      "-\\cresc"): // TODO confirm whether these are duplicates of the spans
  case util::hash("-\\dim"):
    util::debug("Denemo: ignoring a cresc / dim postfix");
    return;
  case util::hash("["): // TODO custom bars over the top of quavers etc
  case util::hash("]"):
    return;
  }

  if (postfix.at(0) == ':') { // tremolo
    auto tremolo = std::atoi(postfix.substr(1).c_str());
    cast_note(note).set_tremolo(tremolo);
    return;
  }

  if (util::starts_with(
          postfix, "R1*")) // calculation of whole bar rest - we do this
                           // ourselves when outputting lilypond
    return;

  util::info("Denemo: ignored a note postfix: ", postfix);
}

} // namespace denemo::postfix
