#pragma once

#include <memory>

namespace notation {
class Book;
}

/** @namespace denemo
@brief Functions for working with Denemo files <a
href="http://www.denemo.org">www.denemo.org</a>.
*/

namespace denemo {

/** Parse a Denemo file.  Denemo files encapsulate an entire book (one or more
 * movements). */
std::unique_ptr<notation::Book> parse_denemo(const std::string &file);

} // namespace denemo
