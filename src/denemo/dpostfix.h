#pragma once

#include <string>

namespace notation {
class Bar;
class TimedNotation;
} // namespace notation

namespace denemo {

/** \namespace denemo::postfix
\brief Sufficient code to handle to pre-/post-fix notation used in Denemo files
*/
namespace postfix {

void apply_bar_postfix(notation::Bar &, const std::string &);
void apply_note_postfix(notation::TimedNotation &, const std::string &);

} // namespace postfix
} // namespace denemo
