#include "schemey.h"

#include "util/logging.h"
#include "util/string.h"

#include <sstream>

namespace denemo::schemey {

//// Environment

Environment::Environment() : mParent{} {}

Environment::Environment(std::shared_ptr<Environment> &parent)
    : mParent{parent} {}

void Environment::add_function(
    const std::string &name, const EvalFunction &function) {
  mFunctions[name] = function;
}

std::optional<EvalFunction> Environment::get_function(const std::string &name) {
  auto rval = mFunctions.find(name);
  if (rval != mFunctions.end()) {
    EvalFunction ef = (*rval).second;
    return {ef};
  }
  if (mParent) {
    return mParent->get_function(name);
  }
  return std::nullopt;
}

//// Cell

Cell::Cell(std::shared_ptr<Environment> &environment)
    : mEnvironment{environment}, mCar{} {}

Cell::Cell(std::shared_ptr<Environment> &environment, const std::string &value)
    : mEnvironment{environment}, mCar{value} {}

std::string Cell::car() const {
  return mCar;
}

std::vector<Cell> Cell::cdr() const {
  return mCdr;
}

std::string Cell::evaluate() const {
  if (mCar.empty())
    return "";
  if (auto ef = mEnvironment->get_function(mCar); ef) {
    return ef->operator()(mCdr);
  }
  return mCar;
}

std::string Cell::get_display_string() const {
  if (mCar.empty())
    return "()";
  if (mCdr.empty()) {
    return mCar;
  }
  auto rval = std::stringstream{};
  rval << '(' << mCar;
  for (const auto &cell : mCdr) {
    rval << ' ' << cell.get_display_string();
  }
  rval << ')';
  return rval.str();
}

void Cell::append(const std::string &string) {
  if (mCar.empty()) {
    mCar = string;
    return;
  }
  mCdr.push_back({mEnvironment, string});
}

void Cell::append(const Cell &cell) {
  mCdr.push_back(cell);
}

//// Helpers

std::shared_ptr<Environment> get_standard_environment() {
  auto rval = std::make_shared<Environment>();
  rval->add_function("list", [](const std::vector<Cell> &cells) -> std::string {
    std::stringstream list{};
    for (const auto &cell : cells) {
      list << cell.evaluate() << ';';
    }
    return list.str();
  });
  rval->add_function("cons", [](const std::vector<Cell> &cells) -> std::string {
    std::stringstream cons{};
    size_t count{0};
    for (const auto &cell : cells) {
      if (count == 0) {
        cons << cell.evaluate() << "='";
      } else {
        if (count > 1)
          cons << ' ';
        cons << cell.evaluate();
      }
      ++count;
    }
    cons << '\'';
    return cons.str();
  });
  // TODO add some basic functionality
  return rval;
}

namespace { // parse_scheme_string

bool is_terminating_char(int c) {
  return (c == ' ' || c == ')');
}

std::string munch_word(std::istringstream &is) {
  auto rval = std::stringstream{};
  while (!is_terminating_char(is.peek())) {
    auto nextchar = is.get();
    // strings could start with an ', so munch that
    if (nextchar == '\'' && rval.str().empty())
      continue;
    if (nextchar == '"') { // open quote
      while (true) {
        auto next = is.get();
        if (next == '"')
          break;            // end quote
        if (next == '\\') { // literal, munch regardless
          rval << static_cast<char>(is.get());
          continue;
        }
        rval << static_cast<char>(next);
      }
      continue;
    }
    if (nextchar == '\\') { // literal, munch the next char regardless
      rval << static_cast<char>(is.get());
    }
    rval << static_cast<char>(nextchar);
  }
  // munch the terminating space between words.
  if (is.peek() == ' ')
    is.get();

  return util::remove_outer_quotes(rval.str());
}

Cell recursive_parse(
    std::shared_ptr<Environment> &parentEnvironment, std::istringstream &is) {
  if (is.get() != '(') {
    util::error("Parsing scheme went wrong: ", is.str());
    throw std::runtime_error{"Passed a string which is not Scheme"};
  }
  auto rval = Cell(parentEnvironment);
  while (true) {
    auto peek = is.peek();
    if (peek == ')')
      break;
    if (peek == '(') {
      rval.append(recursive_parse(parentEnvironment, is));
    } else
      rval.append(munch_word(is));
  };
  is.get(); // munch the last ')'
  return rval;
}

} // namespace

Cell parse_scheme_string(
    std::shared_ptr<Environment> &parentEnvironment, std::string &string) {
  util::info("Parsing scheme: ", string);
  std::istringstream is{string};
  return recursive_parse(parentEnvironment, is);
}

} // namespace denemo::schemey
