#pragma once

#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

namespace denemo {

/** @namespace denemo::schemey
@brief sufficient scheme handling to parse the code embedded in Denemo files.
*/
namespace schemey {

class Cell;
using EvalFunction = std::function<std::string(const std::vector<Cell> &)>;

class Environment {
public:
  Environment();
  explicit Environment(std::shared_ptr<Environment> &parent);

  void add_function(const std::string &name, const EvalFunction &function);
  std::optional<EvalFunction> get_function(const std::string &name);

private:
  std::shared_ptr<Environment> mParent;
  std::unordered_map<std::string, EvalFunction> mFunctions{};
};

class Cell {
public:
  explicit Cell(std::shared_ptr<Environment> &environment);
  Cell(std::shared_ptr<Environment> &environment, const std::string &value);
  // getters
  std::string car() const;
  std::vector<Cell> cdr() const;
  std::string evaluate() const;
  std::string get_display_string() const;

  // setters
  void append(const std::string &string);
  void append(const Cell &cell);

private:
  std::shared_ptr<Environment> mEnvironment;
  std::string mCar;
  std::vector<Cell> mCdr{};
};

std::shared_ptr<Environment> get_standard_environment();
Cell parse_scheme_string(
    std::shared_ptr<Environment> &parentEnvironment, std::string &string);

} // namespace schemey

} // namespace denemo
