#include "notation/pitch.h"

#include <catch.hpp>

namespace notation {

TEST_CASE("soundes", "[soundes]") {
  REQUIRE_THROWS(Pitch(-1));
  REQUIRE(Pitch(0) == Pitch(Sound::C, Accidental::Natural, -1));
  REQUIRE(Pitch(13) == Pitch(Sound::C, Accidental::Sharp, 0));
  REQUIRE(Pitch(26) == Pitch(Sound::D, Accidental::Natural, 1));
  REQUIRE(Pitch(39) == Pitch(Sound::D, Accidental::Sharp, 2));
  REQUIRE(Pitch(52) == Pitch(Sound::E, Accidental::Natural, 3));
  REQUIRE(Pitch(65) == Pitch(Sound::F, Accidental::Natural, 4));
  REQUIRE(Pitch(78) == Pitch(Sound::F, Accidental::Sharp, 5));
  REQUIRE(Pitch(91) == Pitch(Sound::G, Accidental::Natural, 6));
  REQUIRE(Pitch(104) == Pitch(Sound::G, Accidental::Sharp, 7));
  REQUIRE(Pitch(117) == Pitch(Sound::A, Accidental::Natural, 8));
  REQUIRE(Pitch(130) == Pitch(Sound::A, Accidental::Sharp, 9));
  REQUIRE(Pitch(131) == Pitch(Sound::B, Accidental::Natural, 9));
  for (int i = 0; i < 131; i++)
    REQUIRE(Pitch(i).get_midi_pitch_number() == i);

  REQUIRE(Pitch(Sound::A, Accidental::Natural, 4).get_midi_pitch_number() == 69);
  REQUIRE(Pitch(1).get_fundamental_frequency_in_hz() == Approx(8.662).epsilon(0.01));
  REQUIRE(Pitch(14).get_fundamental_frequency_in_hz() == Approx(18.354).epsilon(0.01));
  REQUIRE(Pitch(27).get_fundamental_frequency_in_hz() == Approx(38.891).epsilon(0.01));
  REQUIRE(Pitch(40).get_fundamental_frequency_in_hz() == Approx(82.407).epsilon(0.01));
  REQUIRE(Pitch(53).get_fundamental_frequency_in_hz() == Approx(174.61).epsilon(0.01));
  REQUIRE(Pitch(66).get_fundamental_frequency_in_hz() == Approx(369.99).epsilon(0.01));
  REQUIRE(Pitch(69).get_fundamental_frequency_in_hz() == Approx(440.0).epsilon(0.01));
  REQUIRE(Pitch(79).get_fundamental_frequency_in_hz() == Approx(783.99).epsilon(0.01));
  REQUIRE(Pitch(92).get_fundamental_frequency_in_hz() == Approx(1661.2).epsilon(0.01));
  REQUIRE(Pitch(105).get_fundamental_frequency_in_hz() == Approx(3520.0).epsilon(0.01));
  REQUIRE(Pitch(118).get_fundamental_frequency_in_hz() == Approx(7458.6).epsilon(0.01));
  REQUIRE(Pitch(131).get_fundamental_frequency_in_hz() == Approx(15804.3).epsilon(0.01));
};

} // namespace notation
