#include "notation/duration.h"
#include "util/maths.h"
#include "geom/ratio.h"

#include <catch.hpp>

namespace notation {

TEST_CASE("duration", "[duration]") {
  REQUIRE(util::is_power_of_two(1) == true);
  REQUIRE(util::is_power_of_two(2) == true);
  REQUIRE(util::is_power_of_two(3) == false);
  REQUIRE(util::is_power_of_two(4) == true);
  REQUIRE(util::is_power_of_two(8) == true);
  REQUIRE(util::is_power_of_two(10) == false);
  REQUIRE(util::is_power_of_two(16) == true);
  REQUIRE(Duration(4).get_length() == geom::Ratio(1, 4));
  REQUIRE(Duration(4, 1).get_length() == geom::Ratio(3, 8));
  REQUIRE(Duration(4, 2).get_length() == geom::Ratio(7, 16));
  REQUIRE(Duration(4, 3).get_length() == geom::Ratio(15, 32));
  REQUIRE_THROWS(Duration(4, -1));
  REQUIRE_THROWS(Duration(3, 0));
};

} // namespace notation
