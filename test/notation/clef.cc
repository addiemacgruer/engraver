#include "notation/clef.h"

#include <catch.hpp>

namespace notation {

TEST_CASE("clef", "[clef]") {
  REQUIRE(Clef::Treble()->get_sound_at_line(1) == Sound::F);
  REQUIRE(Clef::Treble()->get_sound_at_line(5) == Sound::B);
  REQUIRE(Clef::Treble()->get_sound_at_line(7) == Sound::G);
  REQUIRE(Clef::Treble()->get_sound_at_line(10) == Sound::D);
  REQUIRE(Clef::Treble()->get_sound_at_line(11) == Sound::C);
  REQUIRE(Clef::FrenchViolin()->get_sound_at_line(9) == Sound::G);
  REQUIRE(Clef::Bass()->get_sound_at_line(1) == Sound::A);
  REQUIRE(Clef::Baritone()->get_sound_at_line(1) == Sound::C);
  REQUIRE(Clef::Alto()->get_sound_at_line(5) == Sound::C);
}

} // namespace notation
