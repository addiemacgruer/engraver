#include "notation/sound.h"

#include <catch.hpp>

namespace notation {

TEST_CASE("sound", "[sound]") {
  auto s = Sound::C;
  REQUIRE(++s == Sound::D);
  REQUIRE(++s == Sound::E);
  REQUIRE(++s == Sound::F);
  REQUIRE(++s == Sound::G);
  REQUIRE(++s == Sound::A);
  REQUIRE(++s == Sound::B);
  REQUIRE(++s == Sound::C);
  REQUIRE(--s == Sound::B);
  REQUIRE(--s == Sound::A);
  REQUIRE(--s == Sound::G);
  REQUIRE(--s == Sound::F);
  REQUIRE(--s == Sound::E);
  REQUIRE(--s == Sound::D);
  REQUIRE(--s == Sound::C);
  REQUIRE(s++ == Sound::C);
  REQUIRE(s == Sound::D);
  REQUIRE(Sound::C + 1 == Sound::D);
  REQUIRE(Sound::B + 1 == Sound::C);
  REQUIRE(Sound::C - 1 == Sound::B);
  REQUIRE(Sound::B - 1 == Sound::A);
}

} // namespace notation
