#include "geom/rect.h"
#include "util/maths.h"
#include <catch.hpp>

namespace geom {

TEST_CASE("rect", "[rect]") {
  Rect<double> testRect = {{1.5, 1.5}, {2.5, 3.5}};
  REQUIRE(testRect.width() == 1.0);
  REQUIRE(testRect.height() == 2.0);
  Rect<double> otherRect = {{3, 4}, {5, 6}};
  auto overlap = testRect & otherRect;
  REQUIRE(util::approx_equal(overlap.lower_right().x(), 5.0));
  REQUIRE(overlap.lower_right().y() == 1.5);
  REQUIRE(overlap.upper_left().x() == 1.5);
  REQUIRE(util::approx_equal(overlap.upper_left().y(), 6.0));
}

} // namespace geom
