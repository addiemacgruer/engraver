#include "geom/ratio.h"

#include <catch.hpp>

namespace geom {

TEST_CASE("ratio", "[ratio]") {
  REQUIRE(Ratio::gcd(987, 1491) == 21);
  REQUIRE(Ratio::gcd(1491, 987) == 21);
  REQUIRE(Ratio(2, 4) == Ratio(1, 2));
  REQUIRE(Ratio(1, 2) + Ratio(1, 3) == Ratio(5, 6));
  REQUIRE(Ratio(1, 2) - Ratio(1, 3) == Ratio(1, 6));
  REQUIRE(Ratio(2, 3) * Ratio(1, 2) == Ratio(1, 3));
  REQUIRE(Ratio(2, 3) / Ratio(1, 2) == Ratio(4, 3));
  REQUIRE(Ratio(5, 6) * 2 == Ratio(10, 6));
  REQUIRE(Ratio(2, 1) == 2);
}

} // namespace geom
