#include "engraver/shell.h"

#include <catch.hpp>
#include <iostream>

TEST_CASE("Shell result", "[shell]") {
  auto ls = engraver::exec("ls");
  REQUIRE(ls.result == 0);
  // std::cout << "Result of ls command:\n" << ls.output << "\n";
}
