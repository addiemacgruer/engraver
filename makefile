all:
	@ninja

single:
	@clear && printf '\e[3J'
	@ninja -j1

format:
	@clang-format -i src/* src/*/* src/*/*/* || true

git:
	@git add . && git commit -a && git push

doc:
	@doxygen Doxyfile 
	@firefox doc/html/index.html &

run: all
	@LSAN_OPTIONS=print_suppressions=0:suppressions=./suppress.supp ./bin/engraver

debug: all
	@lldb ./bin/engraver

lizard:
	@lizard -t 5 --sort cyclomatic_complexity -m -Tcyclomatic_complexity=10 -Tparameter_count=5 -L100 -Ttoken_count=500 -w src || true

cppcheck:
	@cppcheck --quiet -v -I src/ src/ -U UNITTEST --library=wxwidgets.cfg --enable=warning,style,performance,portability,missingInclude --suppress=information:*:* -j5

unused:
	@cppcheck --quiet -v -I src/ src/ -U UNITTEST --library=wxwidgets.cfg --enable=unusedFunction --suppress=information:*:*

check:	lizard cppcheck unused


.PHONY: all single format git doc run debug lizard cppcheck check
